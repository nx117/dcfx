var ipS = document.getElementById('awesome'),
	subSq = document.getElementById('subsq');
ipS.onclick = function() {
	if(ipS.checked) {
		subSq.disabled = false;
		subSq.style.backgroundColor = "#4E90E6";
	} else {
		subSq.disabled = true;
		subSq.style.backgroundColor = "#b9b7b7";
	}
}

<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('CLTPHP', function () {
    return 'hello,CLTPHP!';
});

Route::get('hello/:name', 'index/hello');
Route::get('/banka', 'home/index/banka');
Route::get('/article/id/:id', 'home/article/detail');
Route::get('/help/id/:id', 'home/user/detail');
Route::get('/data/order/:id', 'home/data/order');
Route::get('/product/loan/:id', 'home/loan/product');//网贷详情
Route::get('/product/jiesuan/:id', 'home/loan/jiesuan');//网贷奖金结算
Route::get('/product/msg/:id', 'home/loan/msg');//网贷评价
Route::get('/product/addcomment/:id', 'home/loan/addcomment');//网贷评价
Route::get('loan/tgqr/:id','home/loan/tgqr');//网贷推广二维码
Route::get('loan/url/:id/:vid','home/loan/url');//网贷推广二维码
Route::get('loan/dkinfo','home/loan/dkinfo');//网贷推广二维码

Route::get('loan/url/:id/:vid','home/loan/url');//网贷推广二维码

Route::get('loan/aboutBank','home/loan/aboutBank');//网贷推广二维码

//Route::get('loan/customer/:id','home/loan/jfinfo');
//银行
Route::get('/product/bank/:id', 'home/bank/product');//银行详情
Route::get('/bank/jiesuan/:id', 'home/bank/jiesuan');//银行详情
Route::get('/bank/msg/:id', 'home/bank/msg');//银行详情
Route::get('/bank/addcomment/:id', 'home/bank/addcomment');//银行详情
Route::get('bank/tgqr/:id','home/bank/tgqr');//网贷推广二维码
Route::get('bank/url/:id/:vid','home/bank/url');//网贷推广二维码
Route::get('safe/url/:id/:vid','home/safe/url');//网贷推广二维码
Route::get('bank/dkinfo','home/bank/dkinfo');//网贷推广二维码
Route::get( 'data/myteam/:id','home/data/myteam');//我的团队
Route::get( 'user/id/:uvid','home/user/userinfo');


Route::get( '/order/fanyong/:id','admin/order/fanyong');//返佣数据


//我的有店
Route::get( '/shop/mydian','home/shop/mydian');//
// Route::get( '/shop/add','home/shop/add');//

//推广注册
Route::get('/user/appregister/:vid', 'home/user/appregister');
return [
    '/'=>'home/index/index',

    //OfficialAccount
    '/OfficialAccount'=>'home/index/officialaccount',
    '/customer/:jfimg/:type' =>'home/index/customer',
    '/commodity' =>'home/commodity/index',//展示商品 2019-07-01 16:30:36
    '/login'=>'home/login/login',
    '/dologin'=>'home/login/dologin',
    '/register'=>'home/login/register',
    '/forgetpwd'=>'home/login/forgetpwd',
    '/user/index'=>'home/user/index',

    //微信支付回调
    '/wechatpay'=>'home/pay/wechatNotify',
    '/wxcode'=>'home/user/getWechatCode',

    '/user/order'=>'home/user/order',
    '/user/tuiguang'=>'home/user/tuiguang',
    '/user/help'=>'home/user/help',
    '/user/helpcenter'=>'home/user/helpcenter',
    '/user/myinfo'=>'home/user/myinfo',
    '/user/createpro'=>'home/user/createpro',
    '/user/updatename'=>'home/user/updatename',//更新昵称
    '/user/updatewxname'=>'home/user/updatewxname',//更新微信号
    '/user/aliacc'=>'home/user/aliacc',//更新提现账户
    '/user/vipcpl'=>'home/user/vipcpl',//职务介绍
    '/user/levelrules'=>'home/user/levelrules',//职务介绍
    '/pay/payorder'=>'home/pay/payOrder',//会员购买
    '/logout'=>'home/login/logout',//退出
    //报表
    '/data/index'=>'home/data/index',
    '/data/withdraw'=>'home/data/withdraw',//提现
    '/data/earningsdetail'=>'home/data/earningsdetail',
    '/data/txrecord'=>'home/data/txrecord', //提现记录
    '/data/myteam'=>'home/data/myteam',
    //资讯
    '/article/index'=>'home/article/index',
    //支付
    '/pay/hddz'=>'home/pay/hddz',
    '/pay/yywg'=>'home/pay/yywg',
    'pay/payResult'=>'home/pay/payResult',
];

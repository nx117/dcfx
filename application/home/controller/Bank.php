<?php
namespace app\home\controller;
use think\Db;
use think\Controller;
use think\facade\Request;
use think\facade\Session;
class Bank extends Controller
{
	public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
	public function product($id)
	{
		$this->login();
		$vid = Session::get('vid');
		$userinfo=Db('userinfo')->where('uvid',$vid)->find();
     	 $this->assign('userinfo',$userinfo);
		$bank = Db('bank')->where('id',$id)->find();
		$this->assign('bank',$bank);
		Db::name('bank')->where('id',$id)->setInc('num');
		$sui = Db('bank')->order('num','desc')->limit(8)->select();
		$this->assign('sui',$sui);
		$list=db('msg')->alias('m')
            	->join(config('database.prefix').'users u','m.vid = u.vid','right')
            	->where('cid',1)
            	->where('pid',$id)
            	->limit(2)
                ->select();
        $count = Db('msg')->where('cid',1)->where('pid',$id)->count();
		$this->assign('msg',$list);
		return view('product',['count'=>$count,'vid'=>$vid]);
	}
	//奖金结算
	public function jiesuan($id)
	{
		$vid = Session::get('vid');
		$userinfo=Db('userinfo')->where('uvid',$vid)->find();
     	 $this->assign('userinfo',$userinfo);
		$bank = Db('bank')->where('id',$id)->find();
		$this->assign('bank',$bank);
		return view('jiesuan',['vid'=>$vid]);
	}
	//产品评价
	public function msg($id)
	{
		$vid = Session::get('vid');
		$userinfo=Db('userinfo')->where('uvid',$vid)->find();
     	 $this->assign('userinfo',$userinfo);
		$bank = Db('bank')->where('id',$id)->find();
		$this->assign('bank',$bank);
		$list=db('msg')->alias('m')
            	->join(config('database.prefix').'users u','m.vid = u.vid','right')
            	->where('cid',1)
            	->where('pid',$id)
                ->select();
		$this->assign('msg',$list);
		$vid = Session::get('vid');
		return view('msg',['vid'=>$vid]);
	}

	//评价页面
	public function addcomment($id)
	{
		$this->assign('id',$id);
		$vid = Session::get('vid');
		$userinfo=Db('userinfo')->where('uvid',$vid)->find();
     	 $this->assign('userinfo',$userinfo);
		return view('addcomment',['vid'=>$vid]);
	}
	//保存数据
	public function savemsg()
	{
		if (Request::isAjax()) {
			$data = Request::post();
			$vid = Session::get('vid');
			$data['vid']=$vid;
			$data['add_time']=time();
			$data['cid'] = 1;
			if (Db('msg')->insert($data)) {
				return json(['code'=>0,'msg'=>'评论成功']);
			}else{
				return json(['code'=>1,'msg'=>'评论失败']);
			}
		}
	}

	//网贷推广二维码
	public function tgqr($id)
	{
		$bank = Db('bank')->where('id',$id)->find();

        $vid = Session::get('vid');
        $user =Db::name('users')->where('vid',$vid)->find();
        $sys =Db::name('system')->where('id',1)->find();
        $arr_w =explode(',',$bank['w']);
        require_once dirname( $_SERVER['DOCUMENT_ROOT']).'/extend/zh/phpqrcode.php';
        $uploads_dir =($_SERVER['DOCUMENT_ROOT']).'/uploads/bank/';
        $pic = $uploads_dir.$vid.$id.'.qrcode.png';
        $qr_pic_tmp = $uploads_dir.$vid.'qrcode_de_tmp.png';
        $err_level = 'L';
        $matri_size = 3;//图片大小
        $arr = explode(',', $sys['feng']);
        $herf = $arr[array_rand($arr,1)];
        $url = $herf.'/bank/url/'.$id.'/'.$vid;
        $url = short_url($url);
        \QRcode::png($url,$qr_pic_tmp, $err_level, $matri_size, 2);
        // $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] .'/w.png';
        $backagroud_pic = $_SERVER['DOCUMENT_ROOT'].$bank['bgpic'];
        $bg = imagecreatefromstring(file_get_contents($backagroud_pic));
        $qr = imagecreatefromstring(file_get_contents($qr_pic_tmp));
        $qr_width = imagesx($qr);//logo图片宽度
        $qr_height = imagesy($qr);//logo图片高度
        //重新组合图片并调整大小
        imagecopyresampled($bg, $qr,$arr_w[0],$arr_w[1],$arr_w[2],$arr_w[3],$arr_w[4],$arr_w[5],$qr_width, $qr_height);
        imagepng($bg, $pic);//生成图片
        @unlink($qr_pic_tmp);
//        $url = $url;
//        $url = short_url($url);
        return view('tgqr',['vid'=>$vid,'id'=>$id,'url'=>$url]);
        die;
	}

	//网贷申请页面
	public function url($id,$vid)
	{
		if (!empty(input('code'))) {
			$code = input('code');
			$this->assign('code',$code);
		}
		$bank = Db('bank')->where('id',$id)->find();
		$this->assign('bank',$bank);
		return view('detail',['vid'=>$vid]);
	}


	//保存数据
	public function saveinfo()
	{
          if (Request::isAjax()) {

			$data =Request::post();

			$vid = Session::get('vid');
              $user = Db('userinfo')->where('uvid',$data['vid'])->find();


              //dump($data);dump($vid);die();
			$order = Db('order')
                ->where('cid',1)
                ->where('card',$data['card'])
                ->order('ctime desc')
                ->find();
			$bank = Db('bank')->where('id', $data['id'])->find();
			//dump($bank);die();

			if ($order) {

                //0通过  1申请中    2 驳回
			    if ($order['status']==0){
                return json(['code'=>2, 'msg'=>'您已申请过该产品']);
                }

                if ($order['status']==1 ){
                    return json(['code'=>1, 'msg'=>'身份验证通过,正在跳转页面...','url'=>$bank['href']]);
                }
            }

                if ($data['vid']!='') {
              $bank_data['name'] = $data['ac_name'];
              $bank_data['tel'] = $data['ac_mobile'];
              $bank_data['card'] = $data['card'];
              $bank_data['code'] = $data['code'];
              $bank_data['sn'] =  time();
              $bank_data['status'] = 1;
              $bank_data['ctime'] = time();

              $bank_data['cid'] = 1;
              if ($bank['type'] ==1){
                  $bank_data['cid'] = 4;
              }

              $bank_data['pid'] = $data['id'];
              $bank_data['fan_id1']=$data['vid'];
              //获取上级代理ID
                $bank_data['fan_id2']=$user['lev1'];
                $bank_data['fan_id3']=$user['lev2'];
                $bank_data['total']=$bank['total'];
                $bank_data['title']=$bank['title'];
                $bank_data['jfname']=$bank['jfname'];
                //判断是否VIP  2为VIP
              if ($user['lev_id']==2){
                  $bank_data['lev1_money'] = $bank['vipmout'];
              }else{
                  $bank_data['lev1_money'] = $bank['mout'];
              }

                 $bank_data['lev2_money'] = $bank_data['lev1_money']*0.1;

                $bank_data['lev3_money'] = $bank_data['lev1_money']*0.03;

                if (Db::name('order')->insert($bank_data)) {

                    $sys = Db('system')->where('id', 1)->find();
//                    if (!captcha_check($data['yzm'])) {
//                        return json(['code' => 2, 'msg' => '图形验证码错误']);
//                    };

                    $smsapi = "http://api.smsbao.com/";
                    $user = $sys['smsaccount']; //短信平台帐号
                    $pass = md5($sys['smspassword']); //短信平台密码
                    $phone = $bank['jftel']; //要发送短信的手机号码
//            $code    = rand(1111, 9999);
//            【提花科技】尊敬的用户，您的本次验证码为：#code#   【提花科技订单】客户XXX手机号XXXXXXXXX身份证号XXXXXXXXX申请XXX产品 请尽快处理


//            $messsage = '【提花科技订单】客户XXX手机号XXXXXXXXX身份证号XXXXXXXXX申请XXX产品 请尽快处理';

                    $code = $bank_data['tel'];


                    $mes1 = '【提花科技订单】客户：';
                    $mes2 = ',手机号：#code# ';
                    $mes3 = '。已申请更多订单';
                    $mes4 = '请尽快处理。';

                    $messsage = $mes1 . $bank_data['name'] . $mes2 . $mes3 . $mes4;

                    $content = str_replace("#code#", $code, $messsage); //要发送的短信内容
                    $sendurl = $smsapi . "sms?u=" . $user . "&p=" . $pass . "&m=" . $phone . "&c=" . urlencode($content);
                    $result = file_get_contents($sendurl);


                    return json(['code' => 1, 'msg' => '身份验证通过,正在跳转页面...', 'url' => $bank['href']]);

                }else{
                    return json(['code'=>0, 'msg'=>'申请失败']);
                }
			}else{
                return json(['code'=>0, 'msg'=>'申请失败,缺少推广者ID']);
            }
            
			$bank = Db('bank')->where('id',$data['id'])->find();
			
			//$price = explode("|", $bank['mout']);
			if (!empty($data['vid'])) {
				$bank_data['lev1_money'] = $bank['sjyj'];
				$bank_data['fan_id1'] = $data['vid'];
			}
			if (!empty($user['lev1'])) {
				$bank_data['lev2_money'] = $bank['ssjyj'];
				$bank_data['fan_id2'] = $user['lev1'];
			}
			/*if (!empty($user['lev2'])) {
				$bank_data['lev3_money'] = $price[2];
				$bank_data['fan_id3'] = $user['lev2'];
			}*/
            $bank_data['name'] = $data['ac_name'];
            $bank_data['tel'] = $data['ac_mobile'];
            $bank_data['card'] = $data['card'];
            $bank_data['code'] = $data['code'];
            $bank_data['sn'] = date('YmdHis') . mt_rand(1, 10);
            $bank_data['status'] = 1;
            $bank_data['ctime'] = time();
            $bank_data['cid'] = 1;
            $bank_data['pid'] = $data['id'];
			if (Db::name('order')->insert($bank_data)) {
          		return json(['code'=>1, 'msg'=>'身份验证成功']);
        	}else{
          		return json(['code'=>0, 'msg'=>'申请失败']);
        	}
			
		}
	}




    //短信验证码（短信宝版）
    public function sendCode($bank_data,$jftel,$bank_name)
    {


            $data = Request::post();
            $sys  = Db('system')->where('id', 1)->find();
            if (!captcha_check($data['yzm'])) {
                return json(['code' => 2, 'msg' => '图形验证码错误']);
            };
            $statusStr = array(
                "0"  => "短信发送成功",
                "-1" => "参数不全",
                "-2" => "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！",
                "30" => "密码错误",
                "40" => "账号不存在",
                "41" => "余额不足",
                "42" => "帐户已过期",
                "43" => "IP地址限制",
                "50" => "内容含有敏感词",
            );
            $smsapi  = "http://api.smsbao.com/";
            $user    = $sys['smsaccount']; //短信平台帐号
            $pass    = md5($sys['smspassword']); //短信平台密码
            $phone   = $jftel; //要发送短信的手机号码
//            $code    = rand(1111, 9999);
//            【提花科技】尊敬的用户，您的本次验证码为：#code#   【提花科技订单】客户XXX手机号XXXXXXXXX身份证号XXXXXXXXX申请XXX产品 请尽快处理


//            $messsage = '【提花科技订单】客户XXX手机号XXXXXXXXX身份证号XXXXXXXXX申请XXX产品 请尽快处理';

            $code = $bank_data['tel'];

            $messsage = '【提花科技订单】客户：'+$bank_data['name']+',手机号：#code# ,身份证：'+$bank_data['card']+
                        '。已申请'+$bank_name+'请尽快处理。';

            $content = str_replace("#code#", $code, $messsage); //要发送的短信内容
            $sendurl = $smsapi . "sms?u=" . $user . "&p=" . $pass . "&m=" . $phone . "&c=" . urlencode($content);
            $result  = file_get_contents($sendurl);
//            if ($result == 0) {
//                session('code', $code);
//                return json(['code' => 1, 'msg' => '发送成功']);
//            } else {
//                $msg = $statusStr[$result];
//                return json(['code' => 0, 'msg' => $msg]);
//            }

    }


}
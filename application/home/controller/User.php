<?php
namespace app\home\controller;

use think\Db;
use think\facade\Request;
use think\facade\Session;
use app\admin\model\Users as UsersModel;
class User extends Common
{
    public function initialize(){
        parent::initialize();
    }
   public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
    public function index()
    {
        $this->login();
        $vid = Session::get('vid');



        $kefu=db('system')->where('id',1)->find();
        $this->assign('kefu',$kefu);
        $user = Db::name('users')->alias('u')
                ->join('userinfo i', 'u.vid = i.uvid', 'left')
                ->join('group g', 'i.lev_id = g.id', 'left')
                ->where('vid',$vid)
                ->find();

        $alipay='未实名';

        if($user['alipay']){
            $alipay='已实名';
        }

        $vip=0;

        if ($user['lev_id']==2){
            $vip=1;
        }

        $this->assign('vip',$vip);
        $this->assign('alipay',$alipay);


//
//        if (is_null($user)){
//            $this->redirect('/login');
//        }

        if (is_null($user['open_id'])){
            $url='https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx04a74f9942c11e01&redirect_uri=http://tihuakeji.com/wxcode&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect';
            echo "<script>window.location =\"$url\";</script>";
            return view('user/index2');
        }



        $user['appurl']=Db::name('system')->where('id',1)->value('appurl');
        $team1 = Db('pay')->where('fan_id1',$vid)->where('status',0)->select();
        $team2 = Db('pay')->where('fan_id2',$vid)->where('status',0)->select();
//        $team3 = Db('pay')->where('fan_id3',$vid)->where('status',0)->select();
        $sum1 = 0;
        foreach ($team1 as $k => $v) {
            $sum1 += $v['lev1_money'];
        }

        $sum2 = 0;
        foreach ($team2 as $k => $v) {
            $sum2 += $v['lev2_money'];
        }

        $lev1 = Db('recard')->where('vid',$vid)
                ->where('return_cash_lev',1)
                ->where('operating_type',1)
                ->sum('money');

        $lev2 = Db('recard')->where('vid',$vid)
            ->where('return_cash_lev',2)
            ->where('operating_type',1)
            ->sum('money');


//        $lev3 = Db('recard')->where('vid',$vid)
//            ->where('return_cash_lev',3)
//            ->where('operating_type',1)
//            ->sum('money');


        $total_money=0.00;

        $team = 0.00;

        if ($lev2 ) {
            $team = $lev2;
        }
//        if ($lev1) {
//            $total_money = $lev1+$team;
//        }



//        $sum3 = 0;
//        foreach ($team3 as $k => $v) {
//            $sum3 += $v['lev3_money'];
//        }
        $user['myteam'] = $sum1+$sum2;
        $user['me'] = $sum1;
        $user['dayprice']=Db('pay')->where('fan_id1',$vid)->where('status',0)->whereTime('ctime','today')->sum('lev1_money');
        //dump($user);die();

        $this->assign('total_money',$lev1+$team);

        $this->assign('team_total_money',$team);
        $this->assign('user',$user);
        return view('user/index2');
    }
    //快捷工具
    public function tools(){

        return $this->fetch();
    }
    //推广
    public function tuiguang()
    {
        $this->login();
        $vid = Session::get('vid');
        $tot = Db('userinfo')->where('lev1',$vid)->count();
        $this->assign('tot',$tot);
        $shouyi1 = Db('pay')->where('fan_id1',$vid)->where('status',0)->select();
        $shouyi2 = Db('pay')->where('fan_id2',$vid)->where('status',0)->select();
        $shouyi3 = Db('pay')->where('fan_id3',$vid)->where('status',0)->select();
        $sum1 = 0;
        foreach ($shouyi1 as $k => $v) {
            $sum1 += $v['lev1_money'];
        }
        $sum2 = 0;
        foreach ($shouyi2 as $k => $v) {
            $sum2 += $v['lev2_money'];
        }
        $sum3 = 0;
        foreach ($shouyi3 as $k => $v) {
            $sum3 += $v['lev3_money'];
        }
        $sum = $sum1+$sum2+$sum3;
        $this->assign('sum',$sum);
    	return view('user/tuiguang');
    }
    //海报
    public function createpro()
    {
        $this->login();
        $vid = Session::get('vid');
        $user =Db::name('users')->where('vid',$vid)->find();
        $sys =Db::name('system')->where('id',1)->find();
        $arr_w =explode(',',$sys['w']);
        require_once dirname( $_SERVER['DOCUMENT_ROOT']).'/extend/zh/phpqrcode.php';
        $uploads_dir =($_SERVER['DOCUMENT_ROOT']).'/uploads/users/';
        $pic = $uploads_dir.$vid.'.qrcode.png';
        $qr_pic_tmp = $uploads_dir.$vid.'qrcode_de_tmp.png';
        $err_level = 'L';
        $matri_size = 3;//图片大小
        $arr = explode(',', $sys['feng']);
        $herf = $arr[array_rand($arr,1)];
        $herf =short_url($herf."/user/appregister/".$user['vid']);

        \QRcode::png($herf,$qr_pic_tmp, $err_level, $matri_size, 2);


        // $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] .'/w.png';
        $backagroud_pic = $_SERVER['DOCUMENT_ROOT'].$sys['applogo'];
        $bg = imagecreatefromstring(file_get_contents($backagroud_pic));
        $qr = imagecreatefromstring(file_get_contents($qr_pic_tmp));
        $qr_width = imagesx($qr);//logo图片宽度
        $qr_height = imagesy($qr);//logo图片高度
        //重新组合图片并调整大小
        imagecopyresampled($bg, $qr,$arr_w[0],$arr_w[1],$arr_w[2],$arr_w[3],$arr_w[4],$arr_w[5],$qr_width, $qr_height);
        imagepng($bg, $pic);//生成图片
        @unlink($qr_pic_tmp);
        $this->assign('cdkey',$user['cdkey']);
        return view('user/createpro',['user'=>$user,'sys'=>$sys]);
        die;
    }
    //购买会员
    public function level()
    {
        $this->login();
        $vid = Session::get('vid');

        $this->assign('vid',$vid);

        $lev = Db('group')->where('id',2)->find();
        $this->assign('lev',$lev);
        $gou = Db('service')->where('id',3)->find();
        $this->assign('gou',$gou); 
        return view('user/level');
    }
    //新手帮助
    public function help()
    {
        $this->login();
    	$help = Db('service')->where('id',1)->find();
    	$this->assign('help',$help);
    	return view('user/help');
    }
    //会员设置
    public function myinfo()
    {
        $this->login();
        $vid = Session::get('vid');
        $user = Db::name('users')->alias('u')
                ->join('userinfo i', 'u.vid = i.uvid', 'left')
                ->join('group g', 'i.lev_id = g.id', 'left')
                ->where('vid',$vid)
                ->find();
        $this->assign('user',$user);
    	return view('user/myinfo');
    }

    //更新头像
    public function updateavatar()
    {
        $this->login();
        if (Request::isAjax()) {
            $vid = Session::get('vid');
            $data = Request::post();
            $res = Db::name('users')->where('vid',$vid)->update(['headpic'=>$data['user_avatar']]);
            if ($res) {
                return json(['code'=>0,'msg'=>'头像更新成功']);
            }else{
                return json(['code'=>1,'msg'=>'头像更新失败']);
            }
        }
    }
    //更新昵称
    public function updatename()
    {
        $this->login();
         $vid = Session::get('vid');
         $user = Db('users')->where('vid',$vid)->find();
         $this->assign('user',$user);
        return $this->fetch('user/updatename');
    }
    //保存数据
    public function savename()
    {
        if (Request::isAjax()) {
            $vid = Session::get('vid');
            $data = Request::post();
            $res = Db::name('users')->where('vid',$vid)->update(['nickname'=>$data['name']]);
            if ($res) {
                return json(['code'=>0,'msg'=>'昵称更新成功']);
            }else{
                return json(['code'=>1,'msg'=>'昵称更新失败']);
            }
        }
    }

     //更新微信号
    public function updatewxname()
    {
        $this->login();
         $vid = Session::get('vid');
         $user = Db('userinfo')->where('uvid',$vid)->find();
         $this->assign('user',$user);
        return $this->fetch('user/updatewxname');
    }
    //保存微信号
    public function savewxname()
    {
        if (Request::isAjax()) {
            $vid = Session::get('vid');
            $data = Request::post();


            $res = Db::name('userinfo')->where('uvid',$vid)->update(['wxname'=>$data['wxname'],'card'=>$data['card'],'name'=>$data['name']]);
            if ($res) {
                return json(['code'=>0,'msg'=>'信息更新成功']);
            }else{
                return json(['code'=>1,'msg'=>'信息更新失败']);
            }
        }
    }
    //保存手机号
    public function editphone()
    {
        if (Request::isAjax()) {
            $vid = Session::get('vid');
            $data = Request::post();
            // if ($data['yzm'] != Session::get('code')) {
            //     return json(['code'=>2,'msg'=>'验证码错误']);
            // }
            $res = Db::name('users')->where('vid',$vid)->update(['tel'=>$data['tel']]);
            if ($res) {
                return json(['code'=>0,'msg'=>'手机号更新成功']);
            }else{
                return json(['code'=>1,'msg'=>'手机号号更新失败']);
            }
        }
    }

     //更新提现账户
    public function aliacc()
    {
        $this->login();
         $vid = Session::get('vid');
         $user = Db('userinfo')->where('uvid',$vid)->find();
         if(strpos($user['name'],'用户') !== false){
            $user['name'] = "";
         }
         $this->assign('user',$user);
        return $this->fetch('user/aliacc');
    }
    //保存提现账户
    public function savealiacc()
    {
        if (Request::isAjax()) {
            $vid = Session::get('vid');
            $data = Request::post();
            $res = Db::name('userinfo')->where('uvid',$vid)->update($data);
            if ($res) {
                return json(['code'=>0,'msg'=>'提现账号更新成功']);
            }else{
                return json(['code'=>1,'msg'=>'提现账号更新失败']);
            }
        }
    }
    //职务介绍
    public function vipcpl()
    {
        $this->login();
        $vid = Session::get('vid');
        $user = Db::name('userinfo')->alias('u')
                ->join('users s', 'u.uvid = s.vid', 'left')
                ->join('group g', 'u.lev_id = g.id', 'left')
                ->where('uvid',$vid)
                ->find();
        $this->assign('user',$user);
        return view('user/vipcpl');
    }

    public function helpcenter()
    {
        $this->login();
        $cate = Db('helpcate')->select();
        $this->assign('cate',$cate);
        $art = Db('help')->where('is_tui',1)->select();
        $this->assign('art',$art);
        return view('helpcenter');
    }
    //帮助中心分类
    public function detail($id)
    {
        $this->login();
        $cate = Db('helpcate')->select();
        $this->assign('cate',$cate);
        $art = Db('help')->where('c_id',$id)->select();
        $this->assign('art',$art);
        return view('detail');
    }

    //分销注册
    public function appregister($vid)
    {
        $user = Db('users')->where('vid',$vid)->find();
        $this->assign('user',$user);
        return view('appregister');
    }
    //详细规则
    public function levelrules()
    {
        $this->login();
        $rule = Db('service')->where('id',4)->find();
        $this->assign('rule',$rule);
        return view('levelrules');
    }
    //用户详情
    public function userinfo($uvid)
    {
        $user = Db::name('userinfo')->alias('u')
                ->join('group g', 'u.lev_id = g.id', 'left')
                ->join('users s', 'u.uvid = s.vid', 'left')
                ->where('uvid',$uvid)
                ->find();
        $this->assign('user',$user);
        return view('userinfo');
    }

    //修改密码
    public function uppwd()
    {
        return view('uppwd');
    }
    //保存密码
    public function updatepwd()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $vid = Session::get('vid');
            if (Db('users')->where('vid',$vid)->update(['pwd'=>md5($data['password'])])) {
                return json(['code'=>0,'msg'=>'密码更新成功']);
            }else{
                return json(['code'=>1,'msg'=>'密码更新失败']);
            }
        }
    }


    //订单页面
    public function order()
    {
        $this->login();
        $vid = Session::get('vid');

        //保险订单查询
        $safes = Db::name('order')->alias('o')
            ->join('safe s', 's.id = o.pid', 'left')
            ->where('fan_id1',$vid)
            ->where('cid',3)
            ->order('ctime desc')
            ->field('o.status, o.name,o.sn,o.tel,o.ctime,s.title,s.logo')
            ->select();


        //贷款订单查询
        $loans = Db::name('order')->alias('o')
            ->join('loan s', 's.id = o.pid', 'left')
            ->where('fan_id1',$vid)
            ->where('cid',2)
            ->order('ctime desc')
            ->field('o.status,o.name,o.sn,o.tel,o.ctime,s.title,s.logo')
            ->select();




        //信用卡订单查询
        $banks = Db::name('order')->alias('o')
            ->join('bank s', 's.id = o.pid', 'left')
            ->where('fan_id1',$vid)
            ->where('cid',1)
            ->order('ctime desc')
            ->field('o.status,o.name,o.sn,o.tel,o.ctime,s.title,s.logo')
            ->select();


        //信用卡订单查询
        $recommend = Db::name('order')->alias('o')
            ->join('bank s', 's.id = o.pid', 'left')
            ->where('fan_id1',$vid)
            ->where('cid',4)
            ->order('ctime desc')
            ->field('o.status,o.name,o.sn,o.tel,o.ctime,s.title,s.logo')
            ->select();



        //-------贷款
        //通过
        $loanPass= array();
        //进行中
        $loanProcessing = array();
        //失败
        $loanFailure = array();


        //-------信用卡
        //通过
        $bankPass= array();
        //进行中
        $bankProcessing = array();
        //失败
        $bankFailure = array();



        //-------保险
        //通过
        $safePass= array();
        //进行中
        $safeProcessing = array();
        //失败
        $safeFailure = array();


        //-------商城
        //通过
        $rePass= array();
        //进行中
        $reProcessing = array();
        //失败
        $reFailure = array();



        foreach ($banks as $b) {

            //  cid = 1 信用卡 2 网贷 3 保险 4推荐

            //status 0 通过 1  待审核 2 未通过
            if ($b['status'] == 0) {
                $bankPass[] = $b;
            }

            if ($b['status'] == 1) {
                $bankProcessing[] = $b;
            }

            if ($b['status'] == 2) {

                $bankFailure[] = $b;
            }




        }


        foreach ($recommend as $r) {

            //  cid = 1 信用卡 2 网贷 3 保险 4推荐

                //status 0 通过 1  待审核 2 未通过
                if ($r['status'] == 0) {
                    $rePass[] = $r;
                }

                if ($r['status'] == 1) {
                    $reProcessing[] = $r;
                }

                if ($r['status'] == 2) {

                    $reFailure[] = $r;
                }




        }


        foreach ($loans as $l) {


            //status 0 通过 1  待审核 2 未通过
            if ($l['status'] == 0) {
                $loanPass[] = $l;
            }

            if ($l['status'] == 1) {

                $loanProcessing[] = $l;

            }

            if ($l['status'] == 2) {
                $loanFailure[] = $l;
            }

        }



        foreach ($safes as $s) {



                //status 0 通过 1  待审核 2 未通过
                if ($s['status'] == 0){
                    $safePass[]=$s;
                }

                if ($s['status'] == 1){
                $safeProcessing[]=$s;
                }

                if ($s['status'] == 2){
                    $safeFailure[]=$s;
                }


            }


//            if ($order['cid']==4){
//
//                //status 0 通过 1  待审核 2 未通过
//                if ($order['status'] == 0){
//                    $mallPass[]=$order;
//                }
//
//                if ($order['status'] == 1){
//                    $mallProcessing[]=$order;
//                }
//
//                if ($order['status'] == 2){
//                    $mallFailure[]=$order;
//                }
//
//            }



        $this->assign('empty','<div style="background-color: #F1F4F8;" class="no-ms-box"><img src="/static/home/images/nocp_icon.png" alt="" />
                    <span>暂无记录</span></div>');


        $this->assign('bankPass',$bankPass);
        $this->assign('bankProcessing',$bankProcessing);
        $this->assign('bankF',$bankFailure);

        $this->assign('rePass',$rePass);
        $this->assign('reProcessing',$reProcessing);
        $this->assign('reF',$reFailure);


        $this->assign('loanPass',$loanPass);
        $this->assign('loanProcessing',$loanProcessing);
        $this->assign('loanF',$loanFailure);

            $this->assign('safePass',$safePass);
        $this->assign('safeProcessing',$safeProcessing);
        $this->assign('safeF',$safeFailure);



        $this->assign('aaa',123);


        return view('user/order');
    }

    function getWechatCode(){

        $vid = Session::get('vid');
        $code =$_GET["code"];


        $openid =$this->GetOpenidFromMp($code);
        $data['open_id'] = $openid;

        $res = Db::name('userinfo')->where('uvid',$vid)->update(['open_id'=>$data['open_id']]);

        $url ='http://tihuakeji.com/user/index';
        echo "<script>window.location =\"$url\";</script>";
        return view('user/index');

    }



    /**
     *
     * 构造获取open和access_toke的url地址
     * @param string $code，微信跳转带回的code
     *
     * @return 请求的url
     */
    private function CreateOauthUrlForOpenid($code)
    {

        $urlObj["appid"] = 'wx04a74f9942c11e01';
        $urlObj["secret"] = '4e80955d61b18c1a93d6ec37714ad5fa';
        $urlObj["code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/sns/oauth2/access_token?".$bizString;
    }



    /**
     *
     * 拼接签名字符串
     * @param array $urlObj
     *
     * @return 返回已经拼接好的字符串
     */
    private function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v)
        {
            if($k != "sign"){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }



    /**
     *
     * 通过code从工作平台获取openid机器access_token
     * @param string $code 微信跳转回来带上的code
     *
     * @return openid
     */
    public function GetOpenidFromMp($code)
    {
        $url = $this->CreateOauthUrlForOpenid($code);

        //初始化curl
        $ch = curl_init();
        $curlVersion = curl_version();
//        $config = new WxPayConfig();




        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer

        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话

        //取出openid
        $data = json_decode($tmpInfo,true);
        $this->data = $data;
        $openid = $data['openid'];
        return $openid;
    }


}
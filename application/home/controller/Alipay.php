<?php
namespace app\home\controller;
use think\facade\Env;
use think\Controller;
class Alipay extends Controller
{
    var $aop;
    private $system;
    // 企业支付宝
    public function __construct (){
        //require(Env::get('EXTEND_PATH').'./Alipay/config.php');
        //支付宝支付
        require_once(Env::get('EXTEND_PATH').'Alipay/AopSdk.php');
        $table = db('system');
        $this->system = $table->field('appid,rsakey,alipaykey')->find(1);
        $this->aop    =    new \AopClient();
        $this->aop->gatewayUrl             = 'http://'.$_SERVER['SERVER_NAME'].'/index/pay/yywg';
        $this->aop->appId                 = $this->system['appid'];
        $this->aop->rsaPrivateKey         = $this->system['rsakey'];//私有密钥
        $this->aop->format                 = "json";
        $this->aop->charset                = "utf-8";
        $this->aop->signType            = "RSA2";
        $this->aop->alipayrsaPublicKey     = $this->system['alipaykey'];//共有密钥
        //echo "_initialize";
    }
    public function tradeWapPay($body, $subject, $out_trade_no, $total_amount)
    {
        require_once(Env::get('EXTEND_PATH').'Alipay/wappay/service/AlipayTradeService.php');
        require_once(Env::get('EXTEND_PATH').'Alipay/wappay/buildermodel/AlipayTradeWapPayContentBuilder.php');
        $config = array (   
                //应用ID,您的APPID。
                'app_id' => $this->system['appid'],

                //商户私钥，您的原始格式RSA私钥
                'merchant_private_key' => $this->system['rsakey'],
                //异步通知地址
                'notify_url' =>  'http://'.$_SERVER['SERVER_NAME'].'/pay/hddz',
                
                //同步跳转
                'return_url' => 'http://'.$_SERVER['SERVER_NAME'].'/pay/payResult',

                //编码格式
                'charset' => "UTF-8",

                //签名方式
                'sign_type'=>"RSA2",

                //支付宝网关
                'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

                //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
                'alipay_public_key' => $this->system['alipaykey'],
                
            
        );
        $timeout_express="1m";
        $payRequestBuilder = new \AlipayTradeWapPayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setOutTradeNo($out_trade_no);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setTimeExpress($timeout_express);
        $payResponse = new \AlipayTradeService($config);
        $result=$payResponse->wapPay($payRequestBuilder,$config['return_url'],$config['notify_url']);
    }
    public function rsaCheck($params, $signType)
    {
        return $this->aop->rsaCheckV1($params, NULL, $signType);
    }
}
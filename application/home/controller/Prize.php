<?php
namespace app\home\controller;
use think\Db;
use think\facade\Session;
class Prize extends Common
{
    public function initialize(){
        parent::initialize();
    }
    public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
    public function index()
    {
    	$this->login();

		//php获取今日开始时间戳和结束时间戳
		$beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
		$endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
		//php获取昨日起始时间戳和结束时间戳
		$beginYesterday=mktime(0,0,0,date('m'),date('d')-1,date('Y'));
		$endYesterday=mktime(0,0,0,date('m'),date('d'),date('Y'))-1;

        $vid = 8596;
        //$userinfo = Db('users')->where('vid',$vid)->find();
        $prizelevel = db('prizelevel')->order('min desc')->select();
        $ystmoney = db('order')
                ->where("fan_id1 = {$vid}")
                ->where("status = 1")
                ->where('ctime','between',[$beginYesterday,$endYesterday])
                ->sum('lev1_money');
        $prize = db('prizelevel')->where('min',"<",$ystmoney)->order('min desc')->find();
        var_dump($prize);

        return view('prize/index');
    }

}
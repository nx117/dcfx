<?php
namespace app\home\controller;

use think\Db;
use think\facade\Request;
use think\facade\Session;
use app\admin\model\Users as UsersModel;
class Commodity extends Common
{
    public function initialize(){
        parent::initialize();
    }
   public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
    //展示全部商品
    public function index()
    {
        $key=input('key');
        if($key){
            $where[] = ['title','like','%'.$key.'%'];
        }
        $vid = Session::get('vid');
        $stytem = Db('system')->find();
        $this->assign('system',$stytem);
        $userinfo = Db('userinfo')->where('uvid',$vid)->find();
        $this->assign('userinfo',$userinfo);
        $ad = Db('ad')->where('type_id',5)->select();
        $this->assign('ad',$ad);
        $cate = Db('loancate')->order('sort asc')->select();
        //var_dump($cate);
        foreach ($cate as $key => $value) {
            $pro = Db('loan')->where(array('loan_id'=>$value['id'],'status'=>1))->select();
            if($pro)
                $cate[$key]['products'] = $pro;
        }
        $list=db('article')->where('a_id',4)->where($where)->order('add_time desc')->limit('10')->select();
        $this->assign('list',$list);

        $this->assign('product',$cate);
        return view('index/index');
    }
    //快捷工具
    public function tools(){

        return $this->fetch();
    }


}
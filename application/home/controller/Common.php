<?php
namespace app\home\controller;
use think\Db;
use clt\Leftnav;
use think\Controller;
use think\facade\Session;
class Common extends Controller{

    protected $pagesize,$changyan;
    public function initialize()
    {
        $sys = cache('System');
        $this->assign('sys',$sys);
        //获取控制方法
        $action = request()->action();
        $controller = request()->controller();
        $this->assign('action',($action));
        $this->assign('controller',strtolower($controller));
        define('MODULE_NAME',strtolower($controller));
        define('ACTION_NAME',strtolower($action));
    }
    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }
    
    function is_weixin()
    { 
        if ( strpos($_SERVER['HTTP_USER_AGENT'],'MicroMessenger') !== false ) {
            return true;
        }
    }
}
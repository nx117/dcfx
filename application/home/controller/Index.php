<?php
namespace app\home\controller;
use think\Db;
use think\facade\Session;
class Index extends Common
{
    public function initialize(){
        parent::initialize();
    }
    public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
    public function index()
    {
        $this->login();
        //贷款列表
        $loanList = db('loan')
            ->where('status',1)
            ->order('sort')
            ->select();

        //保险列表
        $safeList = db('safe')
            ->where('status',1)
            ->order('sort')
            ->select();

        //信用卡列表
        $bankList = db('bank')
            ->where('status',1)
            ->where('type',0)
            ->order('sort')
            ->select();

        //推荐列表
        $reList = db('bank')
            ->where('status',1)
            ->where('type',1)
            ->order('sort')
            ->select();
        $this->assign('reL',$reList);
        $this->assign('loanL',$loanList);
        $this->assign('bankL',$bankList);
        $this->assign('safeL',$safeList);
        return view('/commodity/index');




    }

    public function officialaccount()
    {
        return view('index/OfficialAccount');
    }

    public function customer($jfimg)
    {
        $this->assign('jfimg',$jfimg);
        return view('index/customer');
    }


    public function banka()
    {
        $vid = Session::get('vid');
        $userinfo = Db('userinfo')->where('uvid',$vid)->find();
        $this->assign('userinfo',$userinfo);
        $userinfo=Db('userinfo')->where('uvid',$vid)->find();
         $this->assign('userinfo',$userinfo);
        $ad = Db('ad')->where('type_id',10)->select();
        $this->assign('ad',$ad);
        $bank = Db('bank')->where(array('status'=>1))->select();
        $this->assign('bank',$bank);
        return view('index/banka');
    }
}

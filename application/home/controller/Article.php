<?php
namespace app\home\controller;
use think\Db;
use think\facade\Request;
use think\facade\Session;
class Article extends Common
{
    public function initialize(){
        parent::initialize();
    }
    public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }

    //推广平台
    public function yongjin()
    {
        $this->login();
//        $article =Db('article')->where('a_id',2)->order('order','des')->select();
//        $this->assign('article',$article);
        return view('article/yongjin');
    }


    //展示客服信息
    public function kefu()
    {
        $this->login();
//        $sys = db('system');

//        $article =Db('article')->where('a_id',2)->order('order','des')->select();
//        $this->assign('article',$article);
        return view('article/kefu');
    }




    //文章列表
    public function index()
    {
        $this->login();
    	$article =Db('article')->where('a_id',2)->order('order','des')->select();
    	$this->assign('article',$article);
        return view('article/index');
    }
    //操作技巧
    public function skills(){
        $key=input('key');
        if($key){
            $where[] = ['title','like','%'.$key.'%'];
        }
        $list=db('article')->where($where)->where('a_id',3)->order('id desc')->select();
        $this->assign('list',$list);
        return $this->fetch();
    }
    //黑科技
    public function usth(){
        $key=input('key');
        if($key){
            $where[] = ['title','like','%'.$key.'%'];
        }
        $list=db('article')->where($where)->where('a_id',3)->order('add_time desc')->select();
        $this->assign('list',$list);

        $xblist=db('article')->where('a_id',7)->order('add_time desc')->select();
        $this->assign('xblist',$xblist);
        return $this->fetch();
    }






    //文章详情
    public function detail($id)
    {
    	Db::name('article')->where('id',$id)->setInc('hits');
    	$art = Db('article')->where('id',$id)->find();
    	$this->assign('art',$art);
    	return view('article/detail');
    }

    //新手指南
    public function noticle()
    {
    	$art = Db('article')->where('a_id',1)
            ->order('add_time desc')->select();
        $tot =Db('article')->where('a_id',1)->count();
    	$this->assign('art',$art);
    	return view('article/noticle',['tot'=>$tot]);
    }

    //培训课堂
    public function pxclass()
    {
    	$art = Db('article')->where('a_id',5)
            ->order('add_time asc')->select();
    	$this->assign('art',$art);
    	return view('article/class');
    }
    //今日口子
    public function todaycut()
    {
        $this->login();
    	$ad = Db('ad')->select();
        $this->assign('ad',$ad);
    	$art = Db('article')->where('a_id',2)->select();
    	$this->assign('art',$art);
    	return view('article/todaycut');
    }
    //提额技巧
    public function jiqiao()
    {
    	$ad = Db('ad')->select();
        $this->assign('ad',$ad);
    	$art = Db('article')->where('a_id',3)->select();
    	$this->assign('art',$art);
    	return view('article/jiqiao');
    }
    //搜索
    public function search()
    {
    	if (Request::isAjax()) {
    		$data = Request::post();
    	}
    }

    //检查用户权限
    public function checkUser()
    {

//        //判断管理员是否登录
//        if (!session('vid')) {
//            $this->redirect('/login');
//        }


        if (Request::isAjax()) {
            $data = Request::post();
            $art = Db('article')->where('id',$data['id'])->find();
            $vid = Session::get('vid');
                                            $user = Db::name('userinfo')->alias('u')
                ->where('uvid',$vid)
                ->join('group g', 'u.lev_id = g.id', 'left')
                ->find();
            if ($art['auth']>$user['weight']) {
                return json(['code'=>1,'msg'=>'老铁，您权限不够!']);
            }else{
                return json(['code'=>0]);
            }
        }
        
    }
}
<?php

namespace app\home\controller;

use think\captcha\Captcha;
use think\Controller;
use think\Db;
use think\facade\Request;
use think\facade\Session;

class Login extends Common
{
    public function login()
    {
        return $this->fetch('login/login');
    }
    public function dologin()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $res  = Db::name('users')->where('tel', $data['tel'])->find();
            if (!$res) {
                return json(['code' => 4, 'msg' => '账户不存在，请先注册']);
            }
            if ($res['open'] == 0) {
                return json(['code' => 4, 'msg' => '您的账户已被禁用']);
            }
            $res = Db::name('users')->where('tel', $data['tel'])->find();
            if ($res) {
                if (md5($data['pwd']) == $res['pwd']) {
                    Session::set('vid', $res['vid']);
                    $vid = Session::get('vid');
                    return json(['code' => 0, 'msg' => '登录成功']);
                } else {
                    return json(['code' => 1, 'msg' => '用户名或账户错误']);
                }
            } else {
                return json(['code' => 2, 'msg' => '用户名不存在']);
            }
        }
    }


    //注册
    public function register()
    {
        return view('login/register');
    }
    //注册保存数据
    public function doreg()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $code = Session::get('code');
            if ($data['yzm'] != $code) {
                print ("this");
                return json(['code' => 1, 'msg' => '验证码错误']);
            }


            $lev = Db::name('users')->where('tel', $data['tel'])->find();
            if ($lev) {
                return json(['code' => 3, 'msg' => '用户已存在！']);
            }

            $lev1=null;
            $lev2=null;

            $userinfo_data=null;


            if (!empty($data['cdkey'])){

            $key = Db::name('users')->where('cdkey', strtoupper($data['cdkey']))->find();
            if (!$key) {
                return json(['code' => 3, 'msg' => '邀请码无效已']);
            }else{
                //通过邀请码搜到的上级信息
                $user = Db::name('userinfo')->where('uvid', $key['vid'])->find();

                $userinfo_data['lev1'] = $user['uvid'];
                $userinfo_data['lev1_name'] = $user['name'];

                $inv = true;

            }
            }
            //项目试运行做活动 邀请好友默认VIP
            $userinfo_data['lev_id']      = 1;

            //生成6位邀请码
            $str = null;
            $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";//大小写字母以及数字
            $max = strlen($strPol)-1;

            for($i=0;$i<6;$i++){
                $str.=$strPol[rand(0,$max)];
            }


            $user_data = array(
                'tel'      => $data['tel'],
                'pwd'      => md5($data['pwd']),
                'headpic'  => '/uploads/logoNew.png',
                'reg_time' => time(),
                'vid' =>1,
                'cdkey' => $str

            );

            // 启动事务
            Db::startTrans();
            try {
                $userId   = Db::name('users')->insertGetId($user_data);
                $vid      = 8000 + $userId;
                $nickname = '用户' . $vid;
                $res      = Db::name('users')->where('id', $userId)->update(['vid' => $vid, 'nickname' => $nickname]);

                $userinfo_data['uvid'] = $vid;
                $userinfo_data['name'] =$nickname;
                $userinfo = Db::name('userinfo')->insert($userinfo_data);

                // 提交事务
              Db::commit();
              
            } catch (\Exception $e) {

                // 回滚事务
                Db::rollback();
                return json(['code' => 2, 'msg' => $e]);
            }

            if ($res || $userinfo) {
              	
                return json(['code' => 0, 'msg' => '注册成功']);
            } else {
                return json(['code' => 2, 'msg' => '注册失败，请重新注册']);
            }
        }
    }




    //忘记密码
    public function forgetpwd()
    {
        return view('login/forgetpwd');
    }
    //审核密码
    public function dopwd()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $user = Db('users')->where('tel', $data['tel'])->find();
            if ($user) {
                Session::set('tel', $data['tel']);
                return json(['code' => 0, 'msg' => '审核成功', 'url' => url('login/uppwd')]);
            } else {
                return json(['code' => 1, 'msg' => '用户不存在']);
            }
        }
    }

    //验证码
    public function verify()
    {
        $config = [
            // 验证码字体大小
            'fontSize' => 30,
            // 验证码位数
            'length'   => 2,
            // 验证码杂点是否开启
            'useNoise' => false,
        ];
        $captcha = new Captcha($config);
        return $captcha->entry();
    }

    //短信验证码（聚合数据版）
    /*
    public function sendCode()
    {
    if (Request::isAjax()) {
    $sys = Db::name('system')->where('id',1)->find();
    $data = Request::post();
    $sys = Db('system')->where('id',1)->find();
    if(!captcha_check($data['yzm'])){
    return json(['code'=>2,'msg'=>'图形验证码错误']);
    };
    $sendUrl = 'http://v.juhe.cn/sms/send'; //短信接口的URL
    $code = rand(1111,9999);
    $smsConf = array(
    'key'   => $sys['smsaccount'], //您申请的APPKEY
    'mobile'    => $data['tel'],
    'tpl_id'    => $sys['gwid'], //您申请的短信模板ID，根据实际情况修改
    'tpl_value' =>'#code#='.$code.'&#company#=聚合数据' //您设置的模板变量，根据实际情况修改
    );
    $content = $this->juhecurl($sendUrl,$smsConf,1); //请求发送短信

    if($content){
    $result = json_decode($content,true);
    $error_code = $result['error_code'];
    if($error_code == 0){
    session('code', $code);
    return json(['code'=>1,'msg'=>'发送成功']);
    }else{
    $msg = $result['reason'];
    return json(['code'=>0,'msg'=>$msg]);
    }
    }else{
    return json(['code'=>0,'msg'=>'发送失败']);
    }
    }
    }*/
    //短信验证码（短信宝版）
    public function sendCode()
    {
        if (Request::isAjax()) {
//            $sys  = Db::name('system')->where('id', 1)->find();
            $data = Request::post();
            $sys  = Db('system')->where('id', 1)->find();
            if (!captcha_check($data['yzm'])) {
                return json(['code' => 2, 'msg' => '图形验证码错误']);
            };
            $statusStr = array(
                "0"  => "短信发送成功",
                "-1" => "参数不全",
                "-2" => "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！",
                "30" => "密码错误",
                "40" => "账号不存在",
                "41" => "余额不足",
                "42" => "帐户已过期",
                "43" => "IP地址限制",
                "50" => "内容含有敏感词",
            );
            $smsapi  = "http://api.smsbao.com/";
            $user    = $sys['smsaccount']; //短信平台帐号
            $pass    = md5($sys['smspassword']); //短信平台密码
            $phone   = $data['tel']; //要发送短信的手机号码
            $code    = rand(1111, 9999);
            $content = str_replace("#code#", $code, $sys['smscontent']); //要发送的短信内容
            $sendurl = $smsapi . "sms?u=" . $user . "&p=" . $pass . "&m=" . $phone . "&c=" . urlencode($content);
            $result  = file_get_contents($sendurl);
            if ($result == 0) {
                session('code', $code);
                return json(['code' => 1, 'msg' => '发送成功']);
            } else {
                $msg = $statusStr[$result];
                return json(['code' => 0, 'msg' => $msg]);
            }
        }
    }
    public function juhecurl($url, $params = false, $ispost = 0)
    {
        $httpInfo = array();
        $ch       = curl_init();
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($ispost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_URL, $url);
        } else {
            if ($params) {
                curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
            }
        }
        $response = curl_exec($ch);
        if ($response === false) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
        curl_close($ch);
        return $response;
    }
    //修改密码
    public function uppwd()
    {
        return view('login/uppwd');
    }
    //修改密码保存数据
    public function updatepwd()
    {
        if (Request::isAjax()) {
            $tel  = Session::get('tel');
            $data = Request::post();
            $pwd  = md5($data['password']);
            $res  = Db::name('users')->where('tel', $tel)->update(['pwd' => $pwd]);
            if ($res) {
                return json(['code' => 0, 'msg' => '修改成功']);
            } else {
                return json(['code' => 1, 'msg' => '修改失败']);
            }
        }
    }
    //退出
    public function logout()
    {
        Session::set('vid', "");
        $this->redirect('/login');
    }

    //app注册
    public function appregister()
    {
       // print ("dsoifjsjdflfdddddddddddddd");

        if (Request::isAjax()) {
            $data = Request::post();
            $user = Db::name('users')->alias('u')
                ->join('userinfo i', 'u.vid = i.uvid', 'left')
                ->where('vid', $data['vid'])
                ->find();
            $code = Session::get('code');
            if ($data['yzma'] != $code) {
                return json(['code' => 1, 'msg' => '验证码错误']);
            }
            $lev = Db::name('users')->where('tel', $data['tel'])->find();
            if ($lev) {
                return json(['code' => 3, 'msg' => '用户已存在！']);
            }


            $key = Db::name('users')->where('cdkey', strtoupper($data['cdkey']))->find();
            if (!$key) {
                return json(['code' => 3, 'msg' => '邀请码无效']);
            }





            //生成6位邀请码
            $str = null;
            $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";//大小写字母以及数字
            $max = strlen($strPol)-1;
            for($i=0;$i<6;$i++){
                $str.=$strPol[rand(0,$max)];
            }




            $user_data = array(
                'tel'      => $data['tel'],
                'pwd'      => md5($data['mima']),
                'headpic'  => '/uploads/logoNew.png',
                'reg_time' => time(),
                'cdkey' => $str
            );
//            if (!empty($user['lev1'])) {
//                $userInfo                   = Db::name('userinfo')->where('uvid', $user['lev1'])->find();
//                $userinfo_data['lev2']      = $user['lev1'];
//                $userinfo_data['lev2_name'] = $userInfo['name'];
//            }

            $userinfo_data['lev1']      = $data['vid'];
            $userinfo_data['lev1_name'] = $user['name'];

            //项目试运行做活动 邀请好友默认VIP
            $userinfo_data['lev_id']      = 1;
            // 启动事务
           Db::startTrans();
           try {
                $userId                = Db::name('users')->insertGetId($user_data);
                $vid                   = 8000 + $userId;
                $nickname              = '用户' . $vid;
                $userinfo_data['name'] = $nickname;
                $userinfo_data['uvid'] = $vid;
                $res                   = Db::name('users')->where('id', $userId)->update(['vid' => $vid, 'nickname' => $nickname]);
                $userinfo              = Db::name('userinfo')->insert($userinfo_data);
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                //print ("出错啦");
                Db::rollback();
            }

            if ($res || $userinfo) {
              	
                return json(['code' => 0, 'msg' => '注册成功']);
            } else {
                return json(['code' => 2, 'msg' => '注册失败']);
            }
        }
    }

    //注册服务协议
    public function fuwuxieyi()
    {
        $xieyi = Db('service')->where('id', 2)->find();
        $this->assign('xieyi', $xieyi);
        return view('user/fuwuxieyi');
    }

    public function getRandChar($length){
        $str = null;
        $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";//大小写字母以及数字
        $max = strlen($strPol)-1;

        for($i=0;$i<$length;$i++){
            $str.=$strPol[rand(0,$max)];
        }
        return $str;
    }
}

<?php

namespace app\home\controller;

use think\Controller;
use think\Db;
use app\home\controller\Alipay;
use think\facade\Env;
use think\facade\Request;
use think\facade\Session;

/*
require "example/WxPay.JsApiPay.php";
require "example/WxPay.Config.php";
require "lib/WxPay.Api.php";
*/
include APP_PATH."../plugins/WeChatDeveloper-master/include.php";

class Pay extends Controller
{
	
	private $config = [
		'token'          => 'shield',
		'appid'          => 'wx04a74f9942c11e01',
		'appsecret'      => '4e80955d61b18c1a93d6ec37714ad5fa',
		'encodingaeskey' => 'BJIUzE0gqlWy0GxfPp4J1oPTBmOrNDIGPNav1YFH5Z5',
		// 配置商户支付参数（可选，在使用支付功能时需要）
		'mch_id'         => "1554887621",
		'mch_key'        => '5aa97629fb18530a2bdc2d8d5baddfd2',
		// 配置商户支付双向证书目录（可选，在使用退款|打款|红包时需要）
		'ssl_key'        => '',
		'ssl_cer'        => '',
		// 缓存目录配置（可选，需拥有读写权限）
		'cache_path'     => APP_PATH.'../runtime',
	];

  public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
	
	public function getJsapiPackage(){ 
		$wechat = new \WeChat\Script($this->config);
		return json(['code'=>1, 'data'=>$wechat->getJsSign(input('url'))]);
		
	}
	
	public function order(){
		if (Request::isAjax()) {
			$vid = Session::get('vid');
			
			$user = Db::name('userinfo')->where('uvid',$vid)->find();
			if ($user['lev_id'] ==2) {

			  return json(['code'=>1,'msg'=>'您已经购买过了！']);
			}else{
				$openId = $user['open_id'];
				
				 // 创建接口实例
				  $wechat = new \WeChat\Pay($this->config);
				  
				  // 组装参数，可以参考官方商户文档
				  $options = [
					  'body'             => '提花科技——VIP服务',
					  'out_trade_no'     => time(),
					  'total_fee'        => '19900',
					  'openid'           => $openId,
					  'trade_type'       => 'JSAPI',
					  'notify_url'       => 'http://tihuakeji.com/wechatpay',
					  'spbill_create_ip' => '127.0.0.1',
				  ];
					
				try {

					// 生成预支付码
					$result = $wechat->createOrder($options);
					
					// 创建JSAPI参数签名
					$options = $wechat->createParamsForJsApi($result['prepay_id']);

                   $this->createOrder();
					
					return json(['code'=>0, 'data'=>$options, 'info'=>'success']);
					
				} catch (Exception $e) {
					return json(['code'=>-1, 'data'=>[], 'info'=>$e->getMessage() . PHP_EOL]);					
				}
			}
		}
	}


    public function wechatNotify()
    {
        $xmlData = file_get_contents('php://input');
        libxml_disable_entity_loader(true);
        $data = json_decode(json_encode(simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        ksort($data);
        $buff = '';
        foreach ($data as $k => $v){
            if($k != 'sign'){
                $buff .= $k . '=' . $v . '&';
            }
        }
        $stringSignTemp = $buff . 'key=5aa97629fb18530a2bdc2d8d5baddfd2';//key为证书密钥
        $sign = strtoupper(md5($stringSignTemp));
//判断算出的签名和通知信息的签名是否一致
        if($sign == $data['sign']){

           $open_id =  $data['openid'];
            $user = Db::name('userinfo')->where('open_id',$open_id)->find();


            if ($user['lev_id']===2){
                echo '<xml>
              <return_code><![CDATA[SUCCESS]]></return_code>
              <return_msg><![CDATA[OK]]></return_msg>
          </xml>';
                exit();
            }


            $payOrder = Db::name('pay')->where('nickname',$user['uvid'])->find();



            $user_id1=Db::name('users')->where('vid',$payOrder['lefan_id1v1'])->find();

            if ($user_id1) {



                //用户uid
                $recard1 = ['vid'            => $user_id1['fan_id1'] ,
                    //操作类型 操作类型 1增加 0减少
                    'operating_type' => 1,
                    //商品类型 1信用卡 2贷款 3保险 4vip 0提现
                    'commodity_type' => 4,
                    //商品id -1为vip商品
                    'commodity_id'   => -1,
                    //操作的金额
                    'money'          => $payOrder['lev1_money'],
                    //操作开始前用户金额
                    'start_money'    => $user_id1['money'],
                    //操作结束后用户金额
                    'end_money'      => $payOrder['lev1_money'] + $user_id1['money'],
                    //订单ID
                    'order_id'       => $payOrder['id'],
                    'return_cash_lev'            => 1,
                    //商品logo
                    'img'            => 'vip商品',
                    'title'            => 'VIP订单返佣',
                    'update_time'    => time()];

                $inster_recard1 = Db::table('clt_recard')->insert($recard1);


                $user_data1 = array(
                    'money'=>$payOrder['lev1_money'] + $user_id1['money'],
                );
                Db::name('users')->where('vid',$user['fan_id1'])->update($user_data1);
            }

            $lev_id_update = array(
                'lev_id'=>2,
            );

            $payOrderUpdate = array(
                'status'=>0,
            );


            $payOrder = Db::name('pay')->where('nickname',$user['uvid'])->update($payOrderUpdate);
            Db::name('userinfo')->where('uvid',$user['uvid'])->update($lev_id_update);

            //处理完成之后，告诉微信成功结果
            echo '<xml>
              <return_code><![CDATA[SUCCESS]]></return_code>
              <return_msg><![CDATA[OK]]></return_msg>
          </xml>';
            exit();
        }
    }
/*
  public function order()
    {
		die('aaa');

      if (Request::isAjax()) {
        $vid = Session::get('vid');
		
        $user = Db::name('userinfo')->where('uvid',$vid)->find();
        if ($user['lev_id'] ==2) {



          return json(['code'=>1,'msg'=>'您已经购买过了！']);
        }else{


            $tools = new \JsApiPay();
//            $openId = $tools->GetOpenid();


            $openId = $user['open_id'];

            //②、统一下单
            $input = new \WxPayUnifiedOrder();
            $input->SetBody("提花科技——会员服务");
            $input->SetAttach("test");
            $input->SetOut_trade_no(date("YmdHis").$vid);
            $input->SetTotal_fee("1");
            $input->SetTime_start(date("YmdHis"));
            $input->SetTime_expire(date("YmdHis", time() + 600));
//	$input->SetGoods_tag("test");
            $input->SetNotify_url("http://paysdk.weixin.qq.com/notify.php");
            $input->SetTrade_type("JSAPI");
            $input->SetOpenid($openId);
            $config = new \WxPayConfig();
            $order = \WxPayApi::unifiedOrder($config, $input);
            echo '<font color="#f00"><b>统一下单支付单信息</b></font><br/>';
            printf_info($order);
            $jsApiParameters = $tools->GetJsApiParameters($order);

            printf_info($jsApiParameters);

            //获取共享收货地址js函数参数
            $editAddress = $tools->GetEditAddressParameters();

            printf_info($editAddress);

//③、在支持成功回调通知中处理成功之后的事宜，见 notify.php
 




            return json(['code'=>0,'msg'=>'正在跳转支付页面...']);
          Session::set('group_id',2);
        }      
      }
    }
*/
    public function payOrder()
    {
        $ali = new Alipay();
        $group = Db::name('group')->where('id',2)->find();
        $bianhao = $this->createOrder($group['price']);
        if($group['price'] == 0){
            $order = Db('pay')->where('sn', $bianhao)->where('status', '1')->find();
            $order['status'] = '0';
            $order['ptime'] = strtotime ("now");
            $r = Db('pay')->update($order);
            $lev_id = array(
                'lev_id' =>2,
            );
            $g = Db::name('users')->where('tel',$order['tel'])->find();
            $us = Db::name('userinfo')->where('uvid',$g['vid'])->update($lev_id);
            $p = Db::name('pay')->where('sn',$out_trade_no)->find();
            $user = Db::name('userinfo')->where('uvid',$g['vid'])->find();

            $user_id1=Db::name('users')->where('vid',$user['lev1'])->find();
            $user_id2=Db::name('users')->where('vid',$user['lev2'])->find();
            $user_id3=Db::name('users')->where('vid',$user['lev3'])->find();
            if ($user_id1) {
                $user_data1 = array(
                    'money'=>$p['lev1_money'] + $user_id1['money'],
                );
                Db::name('users')->where('vid',$user['lev1'])->update($user_data1);
            }
            if ($user_id2) {
                $user_data2 = array(
                    'money'=>$p['lev2_money'] + $user_id2['money'],
                );
                Db::name('users')->where('vid',$user['lev2'])->update($user_data2);
            }
            if ($user_id3) {
                $user_data3 = array(
                    'money'=>$p['lev3_money'] + $user_id3['money'],
                );
                Db::name('users')->where('vid',$user['lev3'])->update($user_data3);
            }
                header("Refresh:0,Url=/user/index");
                die;            
        }
        $ali->tradeWapPay('支付'.$group['price'],"购买".$group['title'], $bianhao, $group['price']);
    }
    public function yywg(){
        echo "应用网关";
    }
    public function payResult(){
        echo "支付成功";
        header("Refresh:0,Url=/user/index");
    }
    public function createOrder($money=100, $time = 1)
    {
      $vid = Session::get('vid');
      $group = Db::name('group')->where('id',2)->find();
      // $user = Db::name('users')->where('vid',$vid)->find();
      $user = Db::name('users')->alias('u')
                ->join('userinfo i', 'u.vid = i.uvid', 'left')
                ->where('vid',$vid)
                ->find();
      $order['sn'] = 'DDBH' . date('YmdHis') . mt_rand(1000, 9999);
      $order['money'] = $group['price'];
      $order['ctime'] = strtotime ("now");
      $order['ptime'] = $time;
      $order['status'] = 1;
      $order['title']=2;
      $order['name']=$user['name'];
      $order['nickname']=$user['vid'];
      $order['tel']=$user['tel'];

      //$pay = Db('group')->where('id',2)->find();
      $user = Db('userinfo')->where('uvid',$vid)->find();
      $price = explode("|", $group['mout']);

      if (!empty($user['lev1'])) {
        $order['lev1_money'] = $price[0];
        $order['fan_id1'] = $user['lev1'];
      }

    Db('pay')->insert($order);

     return $order['sn'];
    }

    public function hddz()
    {
        $request = input('post.');
        $log = "<br />\r\n\r\n".'==================='."\r\n".date("Y-m-d H:i:s")."\r\n".json_encode($request);
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
        if (isset($request['sign_type'])) {
            $log = "<br />\r\n\r\nsign_type";
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
            $signType = $request['sign_type'];
            $alipay = new Alipay();
            $flag = $alipay->rsaCheck($request, $signType);
            if ($flag) {
                $log = "<br />\r\n\r\签名验证成功";
                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                //支付成功:TRADE_SUCCESS   交易完成：TRADE_FINISHED   签名验证成功
                if ($request['trade_status'] == 'TRADE_SUCCESS' || $request['trade_status'] == 'TRADE_FINISHED') {
                    //这里根据项目需求来写你的操作 如更新订单状态等信息 更新成功返回'success'即可

                    $out_trade_no = $request['out_trade_no'];
                    $order = Db('pay')->where('sn', $out_trade_no)->where('status', '1')->find();
                    if (!$order) {//----上次已经付费成功
                        $log = "<br />\r\n\r\上次已经付费成功";
                        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                        exit('success');
                    }
                    $log = "<br />\r\n\r\----处理中----";
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                    $order['status'] = '0';
                    $order['ptime'] = strtotime ("now");
                    $r = Db('pay')->update($order);
                    $lev_id = array(
                        'lev_id' =>2,
                        );
                    $g = Db::name('users')->where('tel',$order['tel'])->find();

                    $us = Db::name('userinfo')->where('uvid',$g['vid'])->update($lev_id);
                    $p = Db::name('pay')->where('sn',$out_trade_no)->find();
                    $user = Db::name('userinfo')->where('uvid',$g['vid'])->find();

                    $user_id1=Db::name('users')->where('vid',$user['lev1'])->find();
                    $user_id2=Db::name('users')->where('vid',$user['lev2'])->find();
                    $user_id3=Db::name('users')->where('vid',$user['lev3'])->find();
                if ($user_id1) {
                    $startMoney1 = $user_id1['money'];
                    $endMoney1 = $p['lev1_money'] + $user_id1['money'];
                    $user_data1 = array(
                        'money'=>$p['lev1_money'] + $user_id1['money'],
                    );
                    $res1 = Db::name('users')->where('vid',$user['lev1'])->update($user_data1);
                    if ($res1){

                        //用户uid
                        $recard1 = ['vid'            => $user_id1['vid'] ,
                            //操作类型 操作类型 1增加 0减少
                            'operating_type' => 1,
                            //商品类型 1保险 2贷款 3保险 4vip 0提现
                            'commodity_type' => 4,

                            //操作的金额
                            'money'          => $p['lev1_money'],
                            //操作开始前用户金额
                            'start_money'    => $startMoney1,
                            //操作结束后用户金额
                            'end_money'      => $endMoney1,
                            //订单ID
                            'order_id'       => $out_trade_no,
                            'return_cash_lev'            => 2,
                            //商品logo
                            'img'            => '/uploads/20190711/f5eb727c15c32589ed5b8f1ceccab604.jpeg',
                            'title'            => '团队工资提成(购买会员返佣)',
                            'update_time'    => time()];

                        $inster_recard1 = Db::table('clt_recard')->insert($recard1);

                    }
                }
                if ($user_id2) {

                    $startMoney2 = $user_id2['money'];
                    $endMoney2 = $p['lev2_money'] + $user_id2['money'];

                    $user_data2 = array(
                        'money'=>$p['lev2_money'] + $user_id2['money'],
                    );
                   $res2= Db::name('users')->where('vid',$user['lev2'])->update($user_data2);


                    if ($res2){

                        //用户uid
                        $recard2 = ['vid'            => $user_id2['vid'] ,
                            //操作类型 操作类型 1增加 0减少
                            'operating_type' => 1,
                            //商品类型 1保险 2贷款 3保险 4vip 0提现
                            'commodity_type' => 4,

                            //操作的金额
                            'money'          => $p['lev2_money'],
                            //操作开始前用户金额
                            'start_money'    => $startMoney2,
                            //操作结束后用户金额
                            'end_money'      => $endMoney2,
                            //订单ID
                            'order_id'       => $out_trade_no,
                            'return_cash_lev'            => 3,
                            //商品logo
                            'img'            => '/uploads/20190711/f5eb727c15c32589ed5b8f1ceccab604.jpeg',
                            'title'            => '团队工资提成(购买会员返佣)',
                            'update_time'    => time()];

                        $inster_recard1 = Db::table('clt_recard')->insert($recard2);

                    }


                }
//                if ($user_id3) {
//
//
//                    $startMoney3 = $user_id3['money'];
//                    $endMoney3 = $p['lev3_money'] + $user_id3['money'];
//
//                    $user_data3 = array(
//                        'money'=>$p['lev3_money'] + $user_id3['money'],
//                    );
//                  $res3 =  Db::name('users')->where('vid',$user['lev3'])->update($user_data3);
//
//
//
//                    if ($res3){
//
//                        //用户uid
//                        $recard2 = ['vid'            => $user_id3['vid'] ,
//                            //操作类型 操作类型 1增加 0减少
//                            'operating_type' => 1,
//                            //商品类型 1保险 2贷款 3保险 4vip 0提现
//                            'commodity_type' => 4,
//
//                            //操作的金额
//                            'money'          => $p['lev2_money'],
//                            //操作开始前用户金额
//                            'start_money'    => $startMoney2,
//                            //操作结束后用户金额
//                            'end_money'      => $endMoney2,
//                            //订单ID
//                            'order_id'       => $out_trade_no,
//                            //商品logo
//                            'img'            => '/uploads/20190711/f5eb727c15c32589ed5b8f1ceccab604.jpeg',
//                            'update_time'    => time()];
//
//                        $inster_recard1 = Db::table('recard')->insert($recard2);
//
//                    }
//
//
//                }
                if ($r) {
                    $log = "<br />\r\n\r\----处理---成功----<br>".$data;
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                        exit('success'); //成功处理后必须输出这个字符串给支付宝
                    } else {
                        $log = "<br />\r\n\r\----处理---失败----";
                        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                        exit('fail');
                    }
                } else {
                    $log = "<br />\r\n\r\交易状态错误";
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                    exit('fail');
                }
            } else {
                $log = "<br />\r\n\r\签名参数--验证失败";
                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                exit('fail');
            }
        } else {
            $log = "<br />\r\n\r\签名参数错误";
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
            // echo "授权";
        }
    }











}

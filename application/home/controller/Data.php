<?php
namespace app\home\controller;
use think\Db;
use think\facade\Session;
use think\facade\Request;
class Data extends Common
{
    public function initialize(){
        parent::initialize();
    }
    public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
    public function index()
    {
        $this->login();
        $vid = Session::get('vid');
        $user = Db::name('users')->where('vid',$vid)->find();
        $this->assign('user',$user);
        $dai = DB::name('cash')->where('tel',$user['tel'])->where('status',1)->select();
        //待支付
        $daizhifu =0;
        foreach ($dai as $key => $value) {
            $daizhifu +=$value['money'];
        }
        $this->assign('daizhifu',$daizhifu);
        //已支付
        $yizhifu = 0;
        $yi = DB::name('cash')->where('tel',$user['tel'])->where('status',0)->select();
        foreach ($yi as $key => $value) {
            $yizhifu +=$value['money'];
        }
        $this->assign('yizhifu',$yizhifu);
        $s1=0;
        $s2=0;
        $s4=0;
        $s5=0;
        //总收益
        //邀请1收入
        $y1 = Db('pay')->where('fan_id1',$vid)->where('status',0)->select();
        foreach ($y1 as $k => $v) {
            $s1 +=$v['lev1_money'];
        }
        //邀请2收入
        $y2 = Db('pay')->where('fan_id2',$vid)->where('status',0)->select();
        foreach ($y2 as $k => $v) {
            $s2 +=$v['lev2_money'];
        }
//        //邀请33收入
//        $y3 = Db('pay')->where('fan_id3',$vid)->where('status',0)->select();
//        foreach ($y3 as $k => $v) {
//            $s3 +=$v['lev3_money'];
//        }
        //订单1收入
        $d1 = Db('order')->where('fan_id1',$vid)->where('status',0)->select();
        foreach ($d1 as $k => $v) {
            $s4 +=$v['lev1_money'];
        }
        //订单2收入
        $d2 = Db('order')->where('fan_id2',$vid)->where('status',0)->select();
        foreach ($d2 as $k => $v) {
            $s5 +=$v['lev2_money'];
        }
        //订单3收入
//        $d3 = Db('order')->where('fan_id3',$vid)->where('status',0)->select();
//        foreach ($d3 as $k => $v) {
//            $s6 +=$v['lev3_money'];
//        }
        $zong = $s1+$s2+$s4+$s5;

        $this->assign('zong',$zong);
        return view('data/index');
    }

    //提现记录
    public function txrecord()
    {
        $this->login();
        $vid = Session::get('vid');
        $cash = Db('cash')->where('vid',$vid)->select();
        $this->assign('cash',$cash);
        $this->assign('empty','<span class="empty"><div style="background-color: #F1F4F8" class="no-ms-box" v-if="record.length < 1">
                    <img src="/static/home/images/nocp_icon.png"/><span>暂无记录</span></div></span>');
        return view('data/txrecord');
    }
    //
    public function order($id)
    {
        $this->login();
        $vid = Session::get('vid');
        if ($id ==1) {
            $list = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
                    ->where('fan_id1',$vid)
                    ->where('cid',1)
                    ->select();
            $this->assign('data',$list);
            $bank = Db('bank')->select();
            $this->assign('bank',$bank);
        }
        if ($id ==2) {
            $list = Db('order')->field('o.id,o.ctime,o.tel,o.name,o.status,l.title,l.logo')->alias('o')
                    ->join(config('database.prefix') . 'loan l', 'o.pid = l.id', 'left')
                    ->where('fan_id1',$vid)
                    ->where('cid',2)
                    ->select();
            $this->assign('data',$list);
            $bank = Db('loan')->select();
            $this->assign('bank',$bank);
        }
        return view('data/order',['id'=>$id]);
    }

    //我的团队
    public function myteam($id)
    {
        $this->login();
        $vid = Session::get('vid');

        $levs1 = Db('userinfo')->alias('i')
                    ->join('users u','i.uvid = u.vid','left')
                    ->where('lev1',$vid)
                    ->order('uid acs')
                ->field('u.reg_time,i.uvid,i.name,i.lev_id,u.tel,u.headpic')
                    ->select();

        $levs2 =  Db('userinfo')->alias('i')
            ->join('users u','i.uvid = u.vid','left')
                ->where('lev2',$vid)
                ->order('uid acs')
                ->field('u.reg_time,i.uvid,i.name,i.lev_id,u.tel,u.headpic')
                ->select();
//        if ($id == 1) {
//            $list = Db('userinfo')->alias('i')
//                    ->join(config('database.prefix') . 'users u', 'i.uvid = u.vid', 'left')
//                    ->where('lev1',$vid)
//                    ->where('lev_id','>',1)
//                    ->select();
//            //var_dump(DB::name('userinfo')->getlastsql());
//        }else{
//            $list = Db('userinfo')->alias('i')
//                    ->join(config('database.prefix') . 'users u', 'i.uvid = u.vid', 'left')
//                    ->where('lev1',$vid)
//                    ->where('lev_id','<=',1)
//                    ->select();
//        }

//        $tot = Db('pay')->where('fan_id1',$vid)->where('status',0)->count();
//
//        $team1 = Db('pay')->where('fan_id1',$vid)->where('status',0)->select();
//        $team2 = Db('pay')->where('fan_id2',$vid)->where('status',0)->select();
//        $team3 = Db('pay')->where('fan_id3',$vid)->where('status',0)->select();
//        $sum1 = 0;
//        foreach ($team1 as $k => $v) {
//            $sum1 += $v['lev1_money'];
//        }
//
//        $sum2 = 0;
//        foreach ($team2 as $k => $v) {
//            $sum2 += $v['lev2_money'];
//        }
//
//        $sum3 = 0;
//        foreach ($team3 as $k => $v) {
//            $sum3 += $v['lev3_money'];
//        }
        $this->assign('levs1',$levs1);

        $this->assign('levs2',$levs2);
        $this->assign('id',$id);
        $this->assign('empty','<span class="empty"><div style="background-color: #F1F4F8" class="no-ms-box" v-if="record.length < 1">
                    <img src="/static/home/images/nocp_icon.png"/><span>暂无团队</span></div></span>');
//        return view('data/myteam',['tot'=>$tot,'sum'=>$sum]);
        return view('user/agent_info');
    }
        //检查支付宝
    public function checkPay()
    {
        if (Request::isAjax()) {
            $vid = Session::get('vid');
            $user = Db::name('userinfo')->where('uvid',$vid)->find();

            if(empty($user['alipay']) || strpos($user['name'], '用户') !== false){
                return json(['code'=>0,'msg'=>'支付宝账户未完善']);
            }else{
                return json(['code'=>1]);
            }
        }
    }
    //提现
    public function withdraw()
    {
        $this->login();
        $vid = Session::get('vid');
        $user = Db::name('users')->where('vid',$vid)->find();
        $this->assign('user',$user);
        return view('data/withdraw');
    }
    //提现订单
    public function txsave()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $vid = Session::get('vid');
            $user = Db::name('users')->where('vid',$vid)->find(); 
            $userinfo = Db::name('userinfo')->where('uvid',$vid)->find();
            if($userinfo['alipay']=='' || $userinfo['name']==''){
                return json(['code'=>2,'msg'=>'请先设置支付宝账号']);
            }
            if ($userinfo['card'=='']){
                return json(['code'=>1,'msg'=>'请先完善提现信息，填写身份证号']);
            }
            if ($user['money'] < $data['money']) {
                return json(['code'=>1,'msg'=>'账户余额不足']);
            }
            $cash_data = array(
                    'nickname'=>$user['nickname'],
                    'alipay'=>$userinfo['alipay'],
                    'ctime'=>time(),
                    'tel'=>$user['tel'],
                    'vid'=>$vid,
                    'money'=>$data['money'],
                    'name'=>$userinfo['name']
                ); 
            if (Db::name('cash')->insert($cash_data)) {
                $money = $user['money']-$data['money'];
                Db::name('users')->where('vid',$vid)->update(['money'=>$money]);
                return json(['code'=>0,'msg'=>'提现成功，请等待审核']);
            }else{
                return json(['code'=>2,'msg'=>'提现失败']);
            } 
        }
    }

    //查询代理详情数据
    public function customerdetail()
    {
        $type = input('type');
        $id = input('id');
        $ids = input('ids');
        $this->assign('ids',$ids);
        if ($type == 1) {
            if ($ids == 1) {
                $order = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
                    ->where('pid',$id)->where('cid',1)->select();
            }
            if($ids ==2) {
                $order = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
                    ->where('pid',$id)->where('cid',1)->where('status',2)->select();
                    $this->assign('ids',$ids);
            }
            if($ids ==0){
                $order = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
                    ->where('pid',$id)->where('cid',1)->where('status',0)->select();
                    $this->assign('ids',$ids);
            }
            
            $tot = Db('order')->where('pid',$id)->where('cid',1)->count();
            $tot1 = Db('order')->where('pid',$id)->where('cid',1)->where('status',0)->count();
            $suc = Db('order')->where('pid',$id)->where('cid',1)->where('status',0)->select();
            $sum = 0;
            foreach ($suc as $key => $value) {
                $sum +=$v['lev1_money'];
            }
            $this->assign('order',$order);
        }
        if ($type == 2) {
            if ($ids == 1) {
                $order = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'loan l', 'o.pid = l.id', 'left')
                    ->where('pid',$id)->where('cid',2)->select();
            }
            if($ids ==2) {
                $order = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'loan l', 'o.pid = l.id', 'left')
                    ->where('pid',$id)->where('cid',2)->where('status',2)->select();
                    $this->assign('ids',$ids);
            }
            if($ids ==0){
                $order = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'loan l', 'o.pid = l.id', 'left')
                    ->where('pid',$id)->where('cid',2)->where('status',0)->select();
                    $this->assign('ids',$ids);
            }
            $tot = Db('order')->where('pid',$id)->where('cid',2)->count();
            $tot1 = Db('order')->where('pid',$id)->where('cid',2)->where('status',0)->count();
            $suc = Db('order')->where('pid',$id)->where('cid',2)->where('status',0)->select();
            $sum = 0;
            foreach ($suc as $key => $value) {
                $sum +=$v['lev1_money'];
            }
            $this->assign('order',$order);
        }
         $this->assign('empty','<div style="background-color: #F1F4F8;" class="no-ms-box"><img src="/static/home/images/nocp_icon.png" alt="" />
                    <span>暂无记录</span></div>');
        return view('customerdetail',['tot'=>$tot,'tot1'=>$tot1,'sum'=>$sum,'id'=>$id,'type'=>$type]);
    }

    //邀请记录
    public function yaoqing()
    {
        $vid = Session::get('vid');


        $recard = Db('recard')
            ->where('vid',$vid)
            ->where('status',1)
            ->order('update_time desc')
            ->select();



        $cash = Db('cash')
            ->where('vid',$vid)
            ->order('ctime desc')
            ->select();
//
//
//        $y1 = Db('pay')->alias('p')
//                    ->join(config('database.prefix') . 'users u', 'p.fan_id1 = u.vid', 'left')
//                    ->field('p.*,u.headpic')
//                    ->where('fan_id1',$vid)
//                    ->where('status',0)
//                    ->select();
//            $a1 =array('fid'=>1);
//            $p1 = array();
//            foreach ($y1 as $k => $v) {
//               $p1[] = array_merge($v,$a1);
//            }
//        $y2 = Db('pay')->alias('p')
//                    ->join(config('database.prefix') . 'users u', 'p.fan_id2 = u.vid', 'left')
//                    ->field('p.*,u.headpic')
//                    ->where('fan_id2',$vid)
//                    ->where('status',0)
//                    ->select();
//            $a2 =array('fid'=>2);
//            $p2 = array();
//            foreach ($y2 as $k => $v) {
//               $p2[] = array_merge($v,$a2);
//            }
//        $y3 = Db('pay')->alias('p')
//                    ->join(config('database.prefix') . 'users u', 'p.fan_id3 = u.vid', 'left')
//                    ->field('p.*,u.headpic')
//                    ->where('fan_id3',$vid)
//                    ->where('status',0)
//                    ->select();
//            $a3 =array('fid'=>3);
//            $p3 = array();
//            foreach ($y3 as $k => $v) {
//               $p3[] = array_merge($v,$a3);
//            }
//        $order = array_merge($p1,$p2,$p3);
//        $this->assign('empty','<div style="background-color: #F1F4F8;" class="no-ms-box"><img src="/static/home/images/nocp_icon.png" alt="" />
//                    <span>暂无记录</span></div>');
        $this->assign('recard',$recard);
        $this->assign('cash',$cash);

        return view('user/recard');

//        return view('yaoqing');
    }

    //网贷记录
    public function loanjilu()
    {
       $vid = Session::get('vid');
       //一级返佣
       $y1 = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'users u', 'o.fan_id1 = u.vid', 'left')
                    ->join(config('database.prefix') . 'loan l', 'o.pid = l.id', 'left')
                    ->field('o.*,u.headpic,nickname,l.title')
                    ->where('o.fan_id1',$vid)
                    ->where('o.cid',2)
                    ->where('o.status',0)
                    ->select();
        $a1 =array('fid'=>1);
        $p1 = array();
        foreach ($y1 as $k => $v) {
            $p1[] = array_merge($v,$a1);
        }
        //二级返佣
        $y2 = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'users u', 'o.fan_id2 = u.vid', 'left')
                    ->join(config('database.prefix') . 'loan l', 'o.pid = l.id', 'left')
                    ->field('o.*,u.headpic,nickname,l.title')
                    ->where('o.fan_id2',$vid)
                    ->where('o.cid',2)
                    ->where('o.status',0)
                    ->select();
        $a2 =array('fid'=>2);
        $p2 = array();
        foreach ($y2 as $k => $v) {
            $p2[] = array_merge($v,$a2);
        }
        //三级返佣
        $y3 = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'users u', 'o.fan_id3 = u.vid', 'left')
                    ->join(config('database.prefix') . 'loan l', 'o.pid = l.id', 'left')
                    ->field('o.*,u.headpic,nickname,l.title')
                    ->where('o.fan_id3',$vid)
                    ->where('o.cid',2)
                    ->where('o.status',0)
                    ->select();
        $a3 =array('fid'=>3);
        $p3 = array();
        foreach ($y3 as $k => $v) {
            $p3[] = array_merge($v,$a3);
        }
        $this->assign('empty','<div style="background-color: #F1F4F8;" class="no-ms-box"><img src="/static/home/images/nocp_icon.png" alt="" />
                    <span>暂无记录</span></div>');
        $order = array_merge($p1,$p2,$p3);
        $this->assign('order',$order);
       
       return view('loanjilu'); 
    }

     //网贷记录
    public function bankjilu()
    {
       $vid = Session::get('vid');
       //一级返佣
       $y1 = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'users u', 'o.fan_id1 = u.vid', 'left')
                    ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
                    ->field('o.*,u.headpic,nickname,b.title')
                    ->where('o.fan_id1',$vid)
                    ->where('o.cid',1)
                    ->where('o.status',0)
                    ->select();
        $a1 =array('fid'=>1);
        $p1 = array();
        foreach ($y1 as $k => $v) {
            $p1[] = array_merge($v,$a1);
        }
        //二级返佣
        $y2 = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'users u', 'o.fan_id2 = u.vid', 'left')
                    ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
                    ->field('o.*,u.headpic,nickname,b.title')
                    ->where('o.fan_id2',$vid)
                    ->where('o.cid',1)
                    ->where('o.status',0)
                    ->select();
        $a2 =array('fid'=>2);
        $p2 = array();
        foreach ($y2 as $k => $v) {
            $p2[] = array_merge($v,$a2);
        }
        //三级返佣
        $y3 = Db('order')->alias('o')
                    ->join(config('database.prefix') . 'users u', 'o.fan_id3 = u.vid', 'left')
                    ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
                    ->field('o.*,u.headpic,nickname,b.title')
                    ->where('o.fan_id3',$vid)
                    ->where('o.cid',1)
                    ->where('o.status',0)
                    ->select();
        $a3 =array('fid'=>3);
        $p3 = array();
        foreach ($y3 as $k => $v) {
            $p3[] = array_merge($v,$a3);
        }
        $this->assign('empty','<div style="background-color: #F1F4F8;" class="no-ms-box"><img src="/static/home/images/nocp_icon.png" alt="" />
                    <span>暂无记录</span></div>');
        $order = array_merge($p1,$p2,$p3);
        $this->assign('order',$order);
       
       return view('bankjilu'); 
    }

    //保险记录
    public function safejilu()
    {
        $vid = Session::get('vid');
        //一级返佣
        $y1 = Db('order')->alias('o')
            ->join(config('database.prefix') . 'users u', 'o.fan_id1 = u.vid', 'left')
            ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
            ->field('o.*,u.headpic,nickname,b.title')
            ->where('o.fan_id1',$vid)
            ->where('o.cid',1)
            ->where('o.status',0)
            ->select();
        $a1 =array('fid'=>1);
        $p1 = array();
        foreach ($y1 as $k => $v) {
            $p1[] = array_merge($v,$a1);
        }
        //二级返佣
        $y2 = Db('order')->alias('o')
            ->join(config('database.prefix') . 'users u', 'o.fan_id2 = u.vid', 'left')
            ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
            ->field('o.*,u.headpic,nickname,b.title')
            ->where('o.fan_id2',$vid)
            ->where('o.cid',1)
            ->where('o.status',0)
            ->select();
        $a2 =array('fid'=>2);
        $p2 = array();
        foreach ($y2 as $k => $v) {
            $p2[] = array_merge($v,$a2);
        }
        //三级返佣
        $y3 = Db('order')->alias('o')
            ->join(config('database.prefix') . 'users u', 'o.fan_id3 = u.vid', 'left')
            ->join(config('database.prefix') . 'bank b', 'o.pid = b.id', 'left')
            ->field('o.*,u.headpic,nickname,b.title')
            ->where('o.fan_id3',$vid)
            ->where('o.cid',1)
            ->where('o.status',0)
            ->select();
        $a3 =array('fid'=>3);
        $p3 = array();
        foreach ($y3 as $k => $v) {
            $p3[] = array_merge($v,$a3);
        }
        $this->assign('empty','<div style="background-color: #F1F4F8;" class="no-ms-box"><img src="/static/home/images/nocp_icon.png" alt="" />
                    <span>暂无记录</span></div>');
        $order = array_merge($p1,$p2,$p3);
        $this->assign('order',$order);

        return view('bankjilu');
    }
}
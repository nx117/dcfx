<?php
namespace app\home\controller;
use think\Db;
use think\Controller;
use think\facade\Request;
use think\facade\Session;
class safe extends Controller
{
    public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }

    //保险申请页面
    public function url($id,$vid)
    {

        if (!empty(input('code'))) {
            $code = input('code');
            $this->assign('code',$code);
        }
        $safe = Db('safe')->where('id',$id)->find();
        $this->assign('safe',$safe);
        return view('detail',['vid'=>$vid]);
    }


    //保存数据
    public function saveinfo()
    {
        if (Request::isAjax()) {

            $data =Request::post();

            $vid = Session::get('vid');

            $user = Db('userinfo')->where('uvid',$data['vid'])->find();

            //dump($data);dump($vid);die();
            $order = Db('order')->where('cid',3)->where('tel',$data['ac_mobile'])->find();
            $safe = Db('safe')->where('id', $data['id'])->find();
            if ($order) {
                if ($order['status']==0){
                    return json(['code'=>1, 'msg'=>'身份验证通过','url'=>$safe['jfimg']]);
                }

                return json(['code'=>2, 'msg'=>'您已申请过该产品','url'=>$safe['href']]);
            }
            if ($data['vid']!='') {
                $safe_data['name'] = $data['ac_name'];
                $safe_data['tel'] = $data['ac_mobile'];
                $safe_data['card'] = $data['card'];
                $safe_data['code'] = $data['code'];
                $safe_data['sn'] = time();
                $safe_data['status'] = 1;
                $safe_data['ctime'] = time();
                $safe_data['cid'] = 3;
                $safe_data['pid'] = $data['id'];
                $safe_data['fan_id1']=$data['vid'];


//                $safe_data['fan_id3']=$user['lev2'];
//                $safe_data['total']=$safe['total'];
                $safe_data['title']=$safe['title'];
                $safe_data['jfname']=$safe['jfname'];
                //判断是否VIP  2为VIP
                if ($user['lev_id']==2){
                    $safe_data['lev1_money'] = $safe['vipmout'];
                }else{
                    $safe_data['lev1_money'] = $safe['mout'];
                }

                if ($user['lev1']){
                    //获取上级代理ID
                    $safe_data['fan_id2']=$user['lev1'];
                    $safe_data['lev2_money'] =  $safe_data['lev1_money']*0.1;

                }

//                $safe_data['lev2_money'] = $safe_data['lev1_money']*0.1;
//
//                $safe_data['lev3_money'] = $safe_data['lev1_money']*0.03;


                if (Db::name('order')->insert($safe_data)) {


                    $sys  = Db('system')->where('id', 1)->find();
//                    if (!captcha_check($data['yzm'])) {
//                        return json(['code' => 2, 'msg' => '图形验证码错误']);
//                    };

                    $smsapi  = "http://api.smsbao.com/";
                    $user    = $sys['smsaccount']; //短信平台帐号
                    $pass    = md5($sys['smspassword']); //短信平台密码
                    $phone   = $safe['jftel']; //要发送短信的手机号码
//            $code    = rand(1111, 9999);
//            【提花科技】尊敬的用户，您的本次验证码为：#code#   【提花科技订单】客户XXX手机号XXXXXXXXX身份证号XXXXXXXXX申请XXX产品 请尽快处理


//            $messsage = '【提花科技订单】客户XXX手机号XXXXXXXXX身份证号XXXXXXXXX申请XXX产品 请尽快处理';

                    $code = $safe_data['tel'];

                    $mes1 ='【提花科技订单】客户：';
                    $mes2 = ',手机号：#code# ';
                    $mes3 = '。已申请更多订单';
                    $mes4 = '请尽快处理。';

                    $messsage = $mes1.$safe_data['name'].$mes2. $mes3.$mes4;

                    $content = str_replace("#code#", $code, $messsage); //要发送的短信内容
                    $sendurl = $smsapi . "sms?u=" . $user . "&p=" . $pass . "&m=" . $phone . "&c=" . urlencode($content);
                    $result  = file_get_contents($sendurl);


                    return json(['code'=>1, 'msg'=>'身份验证通过','url'=>$safe['jfimg']]);
                }else{
                    return json(['code'=>0, 'msg'=>'申请失败']);
                }



            }else{
                return json(['code'=>0, 'msg'=>'申请失败,缺少推广者ID']);
            }

            $safe = Db('safe')->where('id',$data['id'])->find();


            if (!empty($data['vid'])) {
                $bank_data['lev1_money'] = $safe['sjyj'];
                $bank_data['fan_id1'] = $data['vid'];
            }
            if (!empty($user['lev1'])) {
                $bank_data['lev2_money'] = $safe['ssjyj'];
                $bank_data['fan_id2'] = $user['lev1'];
            }
            /*$price = explode("|", $safe['mout']);
            if (!empty($data['vid'])) {
                $safe_data['lev1_money'] = $price[0];
                $safe_data['fan_id1'] = $data['vid'];
            }
            if (!empty($user['lev1'])) {
                $safe_data['lev2_money'] = $price[1];
                $safe_data['fan_id2'] = $user['lev1'];
            }
            if (!empty($user['lev2'])) {
                $safe_data['lev3_money'] = $price[2];
                $safe_data['fan_id3'] = $user['lev2'];
            }*/
            $safe_data['name'] = $data['ac_name'];
            $safe_data['tel'] = $data['ac_mobile'];
            $safe_data['card'] = $data['card'];
            $safe_data['code'] = $data['code'];
//            $safe_data['sn'] = date('YmdHis') . mt_rand(1, 10);
            $safe_data['status'] = 1;
            $safe_data['ctime'] = time();
            $safe_data['cid'] = 3;
            $safe_data['pid'] = $data['id'];


            $sn= date('yy-MM-dd',time());


            if ($orderId = Db::name('order')->insert($safe_data)->getLastInsID()) {


            if ($orderId) {
                if ($orderId < 99) {
                    $snId = 100 + $orderId;
                    $sn= $sn.$snId;
                } else {
                    $sn = $sn.$orderId;
                }
            }

            if ( Db::name('order')->where('id', $orderId)->update('sn', $sn)){
                return json(['code'=>1, 'msg'=>'身份验证成功']);
            }else{
                return json(['code'=>0, 'msg'=>'申请失败']);
            }




            }else{
                return json(['code'=>0, 'msg'=>'申请失败']);
            }

        }
    }
}
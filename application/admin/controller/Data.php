<?php
namespace app\admin\controller;
use think\Db;
use think\facade\Request;
use think\facade\Env;
use think\Controller;
use think\facade\Session;
class Data extends Common
{




    public function initialize()
    {
        parent::initialize();
    }
    //网贷下款百分比额度页面
    public function edu()
    {
        $id = input('id');
        return view('loan/edu',['id'=>$id]);
    }


    public function cardEdit()
    {
        $id=input('param.id');
        $card = Db::name('card')->where('id',$id)->find();
        return view('bank/card',['cardList'=>$card]);
    }


    //计算下款额度数据
    public function saveloan()
    {
        if (Request::isAjax()) {
            $data = Request::Post();
            $order = Db('order')->where('id',$data['id'])->find();
            $loan = Db('loan')->where('id',$order['pid'])->find();
            $price = explode("|", $loan['percent_price']);
            $lev1_money = $data['edu'] * $order['lev1_money']/100;
            $lev2_money = $data['edu'] * $order['lev2_money']/100;
            $lev3_money = $data['edu'] * $order['lev3_money']/100;

            $fan_id1 = Db('users')->where('vid',$order['fan_id1'])->find();
            $fanid1_money = $fan_id1['money'];
            $fan_id2 = Db('users')->where('vid',$order['fan_id2'])->find();
            $fanid2_money = $fan_id2['money'];
            $fan_id3 = Db('users')->where('vid',$order['fan_id3'])->find();
            $fanid3_money = $fan_id3['money'];

            $money1 = $lev1_money +$fanid1_money;
            $money2 = $lev2_money +$fanid2_money;
            $money3 = $lev3_money +$fanid3_money;
            $res1 = Db('users')->where('vid',$order['fan_id1'])->update(['money'=>$money1]);
            $res2 = Db('users')->where('vid',$order['fan_id2'])->update(['money'=>$money2]);
            $res3 = Db('users')->where('vid',$order['fan_id3'])->update(['money'=>$money3]);
            $res4 = Db('order')->where('id',$data['id'])->update(['status'=>0]);
            if ($res1 ||$res2||$res3 ||$res4) {
                return json(['code'=>1,'msg'=>'审核成功','url'=>url('order/loan')]);
            }else{
                return json(['code'=>0,'msg'=>'审核失败','url'=>url('order/loan')]);
            }
        }
    }
    //网贷通过
    public function loantg()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $order = Db('order')->where('id',$data['id'])->find();

            if ($order){
                if (!$data['total']){
                    return json(['code'=>0,'msg'=>'审核失败,未设置贷款总额','url'=>url('order/loan')]);
                }
            }else{
                return json(['code'=>0,'msg'=>'审核失败,未找到贷款订单记录','url'=>url('order/loan')]);
            }

            $loan = Db('loan')->where('id',$order['pid'])->find();
            if(!$loan){
                return json(['code'=>0,'msg'=>'审核失败,未找到贷款商品信息','url'=>url('order/loan')]);
            }

            $lev1_money =0;
            $lev2_money =0;
            $lev3_money =0;

//
//                 $user = Db::name('users')->alias('u')
//                     ->join('userinfo i', 'u.vid = i.uvid', 'left')
//                     ->join('group g', 'i.lev_id = g.id', 'left')
//                     ->where('vid',$vid)
//                     ->find();



            if ($order['fan_id1']){

                $fan_id1 = Db('users')->alias('u')
                    ->where('vid',$order['fan_id1'])
//                    ->join('userinfo i', 'u.vid = i.uvid', 'left')
//                    ->field('u.money,u.update_time,i.lev_id')
                    ->find();

                if ($fan_id1){
//                    if ($fan_id1['lev_id']==2) {
//                        $lev1_money = $order['total'] * $loan['vip_commisson']*0.01;
//                    }else{
//                        $lev1_money = $order['total'] * $loan['commisson']*0.01;
//                    }

                    $lev1_money = $data['total'] * $order['lev1_money']*0.01;

                    $money1 = $lev1_money + $fan_id1['money'];

                    $res1 = Db('users')
                        ->where('vid',$order['fan_id1'])
                        ->where('update_time',$fan_id1['update_time'])
                        ->update(['money'=>$money1,'update_time'=>time()]);

                    if($res1 ==0){
                        return json(['code'=>0,'msg'=>'审核失败,更新推广者佣金失败','url'=>url('order/loan')]);
                    }

                    //用户uid
                    $recard1 = ['vid'            => $order['fan_id1'] ,
                        //操作类型 1增加 0减少
                        'operating_type' => 1,
                        //商品类型 1信用卡 2贷款 3保险 4vip 0提现
                        'commodity_type' => $order['cid'],
                        //商品id -1为vip商品
                        'commodity_id'   => $order['pid'],
                        //操作的金额
                        'money'          => $lev1_money,
                        //操作开始前用户金额
                        'start_money'    => $fan_id1['money'],
                        //操作结束后用户金额
                        'end_money'      => $money1,
                        //订单ID
                        'order_id'       => $order['id'],
                        //商品logo
                        'img'            => $loan['logo'],
                        'return_cash_lev'            => 1,
                        'title'            => '工资提成('.$loan['title'].')',
                        'update_time'    => time()];

                    $inster_recard1 = Db::table('clt_recard')->insert($recard1);

                    if ($inster_recard1 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,更新推广者佣金失败','url'=>url('order/loan')]);
                    }

                }else {
                    return json(['code' => 0, 'msg' => '审核失败,未找到推广者信息', 'url' => url('order/loan')]);
                }
            }else{
                return json(['code'=>0,'msg'=>'审核失败,缺少推广者信息','url'=>url('order/loan')]);
            }



            if ($order['fan_id2']){
                $fan_id2 = Db('users')->where('vid',$order['fan_id2'])->find();
                if ($fan_id2){

                    $lev2_money = $lev1_money * 0.1 ;

                    $money2 = $lev2_money + $fan_id2['money'];

                    $res2 = Db('users')
                        ->where('vid',$order['fan_id2'])
                        ->where('update_time',$fan_id2['update_time'])
                        ->update(['money'=>$money2,'update_time'=>time()]);

                    if($res2 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,更新直属代理佣金失败','url'=>url('order/loan')]);
                    }

                    //用户uid
                    $recard2 = ['vid'            => $order['fan_id2'] ,
                        //操作类型 1增加 0减少
                        'operating_type' => 1,
                        //商品类型 1信用卡 2贷款 3保险 4vip 0提现
                        'commodity_type' => $order['cid'],
                        //商品id -1为vip商品
                        'commodity_id'   => $order['pid'],
                        //操作的金额
                        'money'          => $lev2_money,
                        //操作开始前用户金额
                        'start_money'    => $fan_id2['money'],
                        //操作结束后用户金额
                        'end_money'      => $money2,
                        //订单ID
                        'order_id'       => $order['id'],
                        //商品logo
                        'img'            => $loan['logo'],
                        'return_cash_lev'            => 2,
                        'title'            => '团队工资提成('.$loan['title'].')',
                        'update_time'    => time()];

                    $inster_recard2 = Db::table('clt_recard')->insert($recard2);

                    if ($inster_recard2 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,更新直属代理佣金失败','url'=>url('order/loan')]);
                    }
                }else {
                    return json(['code' => 0, 'msg' => '审核失败,未找到直属代理信息', 'url' => url('order/loan')]);
                }
            }


            if ($order['fan_id3']) {
                $fan_id3 = Db('users')->where('vid', $order['fan_id3'])->find();
                if ($fan_id3) {

                    $lev3_money = $lev1_money * 0.03;

                    $money3 = $lev3_money + $fan_id3['money'];

                    $res3 = Db('users')
                        ->where('vid', $order['fan_id3'])
                        ->where('update_time', $fan_id3['update_time'])
                        ->update(['money' => $money3, 'update_time' => time()]);

                    if ($res3 == 0) {
                        return json(['code' => 0, 'msg' => '审核失败,更新越级代理佣金失败', 'url' => url('order/loan')]);
                    }

                    //用户uid
                    $recard3 = ['vid' => $order['fan_id3'],
                        //操作类型 1增加 0减少
                        'operating_type' => 1,
                        //商品类型 1信用卡 2贷款 3保险 4vip 0提现
                        'commodity_type' => $order['cid'],
                        //商品id -1为vip商品
                        'commodity_id' => $order['pid'],
                        //操作的金额
                        'money'         => $lev3_money,
                        //操作开始前用户金额
                        'start_money' => $fan_id3['money'],
                        //操作结束后用户金额
                        'end_money' => $money3,
                        //订单ID
                        'order_id' => $order['id'],
                        //商品logo
                        'img' => $loan['logo'],
                        'return_cash_lev'            => 3,
                        'title'            => '团队工资提成('.$loan['title'].')',
                        'update_time' => time()];

                    $inster_recard3 = Db::table('clt_recard')->insert($recard3);

                    if ($inster_recard3 == 0) {
                        return json(['code' => 0, 'msg' => '审核失败,更新越级代理佣金失败', 'url' => url('order/loan')]);
                    }


                }else {
                    return json(['code' => 0, 'msg' => '审核失败,未找到越级代理信息', 'url' => url('order/loan')]);
                }

            }

//                $fanid1_money = $fan_id1['money'];
//            $fan_id2 = Db('users')->where('vid',$order['fan_id2'])->find();
//            $fanid2_money = $fan_id2['money'];
//            $fan_id3 = Db('users')->where('vid',$order['fan_id3'])->find();
//            $fanid3_money = $fan_id3['money'];
//
//            $money1 = $order['lev1_money'] +$fanid1_money;
//            $money2 = $order['lev2_money'] +$fanid2_money;
//            $money3 = $order['lev3_money'] +$fanid3_money;
//
//            $res1 = Db('users')->where('vid',$order['fan_id1'])->update(['money'=>$money1]);
//            $res2 = Db('users')->where('vid',$order['fan_id2'])->update(['money'=>$money2]);
//            $res3 = Db('users')->where('vid',$order['fan_id3'])->update(['money'=>$money3]);
            $total_mont= $data['total'] * $loan['total_mout']*0.01;

            $return_cash = $lev1_money+$lev3_money+$lev3_money;

            $total_income = $total_mont - $return_cash;

            $res4 = Db('order')->where('id',$data['id'])->update(['status'=>0,'lev1_money' =>$lev1_money,
                                                'lev2_money' => $lev2_money, 'lev3_money' =>$lev3_money,
                                                'total_mont'=>$total_mont,'return_cash'=>$return_cash,'total_income'=>$total_income,'total'=>$data['total']]);
            if ($res4) {
                return json(['code'=>1,'msg'=>'审核成功','url'=>url('order/loan')]);
            }else{
                return json(['code'=>0,'msg'=>'审核失败','url'=>url('order/loan')]);
            }
        }
    }

    public function bohui()
    {
        if (Request::isAjax()) {
            $data = Request::post();

            if (Db('order')->where('id',$data['id'])->update(['status'=>2])) {
                return json(['code'=>1,'msg'=>'驳回成功']);
            }else{
                return json(['code'=>0,'msg'=>'驳回失败']);
            }
        }
    }
    public function delete()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            
            if (Db('order')->where('id',$data['id'])->delete()) {
                return json(['code'=>1,'msg'=>'删除成功']);
            }else{
                return json(['code'=>0,'msg'=>'删除失败']);
            }
        }
    }
    //信用卡通过
    public function banktg()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $order = Db('order')->where('id',$data['id'])->find();
            if (!$order){
                return json(['code'=>0,'msg'=>'审核失败,未找到信用卡订单记录','url'=>url('order/bank')]);
            }

            $bank = Db('bank')->where('id',$order['pid'])->find();
            if(!$bank){
                return json(['code'=>0,'msg'=>'审核失败,未找到信用卡商品信息','url'=>url('order/bank')]);
            }

            $lev1_money=0;
            $lev2_money=0;


            //先判断订单是否存在推广者用户id
            if($order['fan_id1']){
                //如果存在就去数据库查询用户信息
                $fan_id1 = Db('users')->where('vid',$order['fan_id1'])->find();
                //判断是否查询到用户信息
                if($fan_id1){

                    //如果查询到 就用用户当前的 钱 + order 里 一级代理的佣金
                        $money1 = $order['lev1_money'] + $fan_id1['money'];

                        //把计算过的金额 根据用户ID 和 updatetime字段 更新到相应的信息里
                    //update_tiem 为乐观锁
                    $res1 = Db('users')
                        ->where('vid',$order['fan_id1'])
                        ->where('update_time',$fan_id1['update_time'])
                        ->update(['money'=>$money1,'update_time'=>time()]);

                        if ($res1 == 0){
                            return json(['code'=>0,'msg'=>'审核失败,更新推广者佣金失败','url'=>url('order/bank')]);
                        }

                    $lev1_money=$fan_id1['money'];

                                //用户uid
                    $recard1 = ['vid'            => $order['fan_id1'] ,
                                //操作类型 操作类型 1增加 0减少
                                'operating_type' => 1,
                                //商品类型 1信用卡 2贷款 3保险 4vip 0提现
                                'commodity_type' => $order['cid'],
                                //商品id -1为vip商品
                                'commodity_id'   => $order['pid'],
                                //操作的金额
                                'money'          => $order['lev1_money'],
                                //操作开始前用户金额
                                'start_money'    => $fan_id1['money'],
                                //操作结束后用户金额
                                'end_money'      => $money1,
                                //订单ID
                                'order_id'       => $order['id'],
                        'return_cash_lev'            => 1,
                                //商品logo
                                'img'            => $bank['logo'],
                        'title'            => '工资提成('.$bank['title'].')',
                                'update_time'    => time()];

                   $inster_recard1 = Db::table('clt_recard')->insert($recard1);

                    if ($inster_recard1 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,添加推广者交易记录失败','url'=>url('order/bank')]);
                    }
                }else {
                    return json(['code' => 0, 'msg' => '审核失败,未找到推广者信息', 'url' => url('order/bank')]);
                }
            }else{
                return json(['code'=>0,'msg'=>'审核失败,缺少推广者信息','url'=>url('order/bank')]);
            }

            if($order['fan_id2']) {
                $fan_id2 = Db('users')->where('vid',$order['fan_id2'])->find();
                if ($fan_id2) {
                    $money2 = $order['lev2_money'] +$fan_id2['money'];
                    $res2 = Db('users')
                        ->where('vid', $order['fan_id2'])
                        ->where('update_time', $fan_id2['update_time'])
                        ->update(['money' => $money2, 'update_time' => time()]);
                        if($res2 == 0){
                          return json(['code'=>0,'msg'=>'审核失败,更新直属上级佣金失败','url'=>url('order/bank')]);
                        }

                        $lev2_money=$fan_id2['money'];

                    $recard2 = ['vid'            => $order['fan_id2'] ,
                                'operating_type' => 1,
                                'commodity_type' => $order['cid'],
                                'commodity_id'   => $order['pid'],
                                'money'          => $order['lev2_money'],
                                'start_money'    => $fan_id2['money'],
                                'end_money'      => $money2,
                                'order_id'       => $order['id'],
                                'img'            => $bank['logo'],
                        'return_cash_lev'            => 2,
                               'title'            => '团队工资提成('.$bank['title'].')',
                                'update_time'    => time()];

                    $inster_recard2 = Db::table('clt_recard')->insert($recard2);

                    if ($inster_recard2 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,添加直属代理交易记录失败','url'=>url('order/bank')]);
                    }

                }else {
                    return json(['code' => 0, 'msg' => '审核失败,未找到直属代理信息', 'url' => url('order/bank')]);
                }
            }

//            if($order['fan_id3']) {
//                $fan_id3 = Db('users')->where('vid',$order['fan_id3'])->find();
//                if ($fan_id3) {
//
//                    $money3 = $order['lev3_money'] +$fan_id3['money'];
//
//                    $res3 = Db('users')
//                        ->where('vid', $order['fan_id3'])
//                        ->where('update_time', $fan_id3['update_time'])
//                        ->update(['money' => $money3, 'update_time' => time()]);
//                    if($res3== 0){
//                        return json(['code'=>0,'msg'=>'审核失败,更新越级上级佣金失败','url'=>url('order/bank')]);
//                    }
//
//                    $recard3 = ['vid'            => $order['fan_id3'] ,
//                                'operating_type' => 1,
//                                'commodity_type' => $order['cid'],
//                                'commodity_id'   => $order['pid'],
//                                'money'          => $order['lev3_money'],
//                                'start_money'    => $fan_id3['money'],
//                                'end_money'      => $money3,
//                                'order_id'       => $order['id'],
//                                'img'            => $bank['logo'],
//                        'return_cash_lev'            => 3,
//                        'title'            => '团队工资提成('.$bank['title'].')',
//                                'update_time'    => time()];
//
//                    $inster_recard3 = Db::table('clt_recard')->insert($recard3);
//
//                    if ($inster_recard3== 0){
//                        return json(['code'=>0,'msg'=>'审核失败,添加越级代理交易记录失败','url'=>url('order/bank')]);
//                    }
//                }else {
//                    return json(['code' => 0, 'msg' => '审核失败,未找到越级代理信息', 'url' => url('order/bank')]);
//                }
//            }

            $total_mont= $bank['total'];

            $return_cash = $lev1_money+$lev2_money;

            $total_income = $total_mont - $return_cash;


            $res4 = Db('order')->where('id',$data['id'])->update(['status'=>0,
                'total_mont'=>$total_mont,'return_cash'=>$return_cash,'total_income'=>$total_income]);

            if ($res4) {
                return json(['code'=>1,'msg'=>'审核成功','url'=>url('order/bank')]);
            }else{
                return json(['code'=>0,'msg'=>'审核失败，更改信用卡订单状态失败','url'=>url('order/bank')]);
            }
        }
    }

    //保险订单通过
    public function safetg()
    {
        if (Request::isAjax()) {
            $data = Request::post();

            if ($data['total']==''){
                return json(['code'=>0,'msg'=>'审核失败,请设置投保总额','url'=>url('order/safe')]);
            }

            $order = Db('order')->where('id',$data['id'])->find();
            if (!$order){
                return json(['code'=>0,'msg'=>'审核失败,未找到保险订单记录','url'=>url('order/safe')]);
            }

            $safe = Db('safe')->where('id',$order['pid'])->find();
            if(!$safe){
                return json(['code'=>0,'msg'=>'审核失败,未找到保险商品信息','url'=>url('order/safe')]);
            }

            $lev1_money =0;
            $lev2_money =0;
            $lev3_money =0;

            //先判断订单是否存在推广者用户id
            if($order['fan_id1']){
                //如果存在就去数据库查询用户信息
                $fan_id1 = Db('users')->where('vid',$order['fan_id1'])->find();
                //判断是否查询到用户信息
                if($fan_id1){

                    $lev1_money = $data['total'] * $order['lev1_money']*0.01;

                    //如果查询到 就用用户当前的 钱 + order 里 一级代理的佣金
                    $money1 = $lev1_money + $fan_id1['money'];

                    //把计算过的金额 根据用户ID 和 updatetime字段 更新到相应的信息里
                    //update_tiem 为乐观锁
                    $res1 = Db('users')
                        ->where('vid',$order['fan_id1'])
                        ->where('update_time',$fan_id1['update_time'])
                        ->update(['money'=>$money1,'update_time'=>time()]);

                    if ($res1 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,更新推广者佣金失败','data' => $res1,'url'=>url('order/safe')]);
                    }


                    //用户uid
                    $recard1 = ['vid'    => $order['fan_id1'] ,
                        //操作类型 操作类型 1增加 0减少
                        'operating_type' => 1,
                        //商品类型 1保险 2贷款 3保险 4vip 0提现
                        'commodity_type' => $order['cid'],
                        //商品id -1为vip商品
                        'commodity_id'   => $order['pid'],
                        //操作的金额
                        'money'          => $lev1_money,
                        //操作开始前用户金额
                        'start_money'    => $fan_id1['money'],
                        //操作结束后用户金额
                        'end_money'      => $money1,
                        //订单ID
                        'order_id'       => $order['id'],
                        //商品logo
                        'img'            => $safe['logo'],
                        'return_cash_lev'            => 1,
                        'title'            => '工资提成('.$safe['title'].')',
                        'update_time'    => time()];

                    $inster_recard1 = Db::table('clt_recard')->insert($recard1);

                    if ($inster_recard1 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,添加推广者交易记录失败','url'=>url('order/safe')]);
                    }
                }else {
                    return json(['code' => 0, 'msg' => '审核失败,未找到推广者信息', 'url' => url('order/safe')]);
                }
            }else{
                return json(['code'=>0,'msg'=>'审核失败,缺少推广者信息','url'=>url('order/safe')]);
            }

            if($order['fan_id2']) {
                $fan_id2 = Db('users')->where('vid',$order['fan_id2'])->find();
                if ($fan_id2) {

                    $lev2_money = $lev1_money*0.1;

                    $money2 = $lev2_money +$fan_id2['money'];
                    $res2 = Db('users')
                        ->where('vid', $order['fan_id2'])
                        ->where('update_time', $fan_id2['update_time'])
                        ->update(['money' => $money2, 'update_time' => time()]);
                    if($res2 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,更新直属上级佣金失败','url'=>url('order/safe')]);
                    }

                    $recard2 = ['vid'            => $order['fan_id2'] ,
                        'operating_type' => 1,
                        'commodity_type' => $order['cid'],
                        'commodity_id'   => $order['pid'],
                        'money'          => $lev2_money,
                        'start_money'    => $fan_id2['money'],
                        'end_money'      => $money2,
                        'order_id'       => $order['id'],
                        'img'            => $safe['logo'],
                        'return_cash_lev'            => 2,
                        'title'            => '团队工资提成('.$safe['title'].')',
                        'update_time'    => time()];

                    $inster_recard2 = Db::table('clt_recard')->insert($recard2);

                    if ($inster_recard2 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,添加直属代理交易记录失败','url'=>url('order/safe')]);
                    }

                }else {
                    return json(['code' => 0, 'msg' => '审核失败,未找到直属代理信息', 'url' => url('order/safe')]);
                }
            }

            if($order['fan_id3']) {
                $fan_id3 = Db('users')->where('vid',$order['fan_id3'])->find();
                if ($fan_id3) {

                    $lev3_money = $lev1_money*0.03;

                    $money3 = $lev3_money +$fan_id3['money'];

                    $res3 = Db('users')
                        ->where('vid', $order['fan_id3'])
                        ->where('update_time', $fan_id3['update_time'])
                        ->update(['money' => $money3, 'update_time' => time()]);
                    if($res3 == 0){
                        return json(['code'=>0,'msg'=>'审核失败,更新越级上级佣金失败','url'=>url('order/safe')]);
                    }

                    $recard3 = ['vid'            => $order['fan_id3'] ,
                        'operating_type' => 1,
                        'commodity_type' => $order['cid'],
                        'commodity_id'   => $order['pid'],
                        'money'          => $lev3_money,
                        'start_money'    => $fan_id3['money'],
                        'end_money'      => $money3,
                        'order_id'       => $order['id'],
                        'img'            => $safe['logo'],
                        'return_cash_lev'            => 3,
                        'title'            => '团队工资提成('.$safe['title'].')',
                        'update_time'    => time()];

                    $inster_recard3 = Db::table('clt_recard')->insert($recard3);

                    if ($inster_recard3 == 0 ){
                        return json(['code'=>0,'msg'=>'审核失败,添加越级代理交易记录失败','url'=>url('order/safe')]);
                    }
                }else {
                    return json(['code' => 0, 'msg' => '审核失败,未找到越级代理信息', 'url' => url('order/safe')]);
                    }
            }

            $total_mont= $data['total'] * $safe['total_mout']*0.01;

            $return_cash = $lev1_money+$lev3_money+$lev3_money;

            $total_income = $total_mont - $return_cash;


            $res4 = Db('order')->where('id',$data['id'])->update(['status'=>0,'total'=>$data['total'],
                                        'total_mont'=>$total_mont,'return_cash' =>$return_cash,'total_income'=>$total_income,]);

            if ($res4) {
                return json(['code'=>1,'msg'=>'审核成功','url'=>url('order/safe')]);
            }else{
                return json(['code'=>0,'msg'=>'审核失败，更改保险订单状态失败','url'=>url('order/safe')]);
            }
        }
    }

    //提现打款
    public function dakuan()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            if (Db('cash')->where('id',$data['id'])->update(['status'=>0,'remark'=>$data['remark']])) {
                return json(['code'=>1,'msg'=>'打款成功']);
            }else{
                return json(['code'=>0,'msg'=>'打款失败']);
            }
        }
        /*
        $table = db('system');
        $system = $table->field('name,appid,rsakey,alipaykey')->find(1);
        if (Request::isAjax()) {
            $data = Request::post();
            require_once Env::get('EXTEND_PATH') . 'Alipay/AopSdk.php';    
            $aop = new \AopClient();
            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $system['appid'];
            $aop->rsaPrivateKey = $system['rsakey'];
            $aop->apiVersion = '1.0';
            $aop->format = 'json';
            $aop->charset = 'utf-8';
            $aop->signType = 'RSA2';
            $aop->alipayrsaPublicKey = $system['alipaykey'];
            $request = new \AlipayFundTransToaccountTransferRequest ();
            $id = $data['id'];
            if(!$id){
                return json(['code'=>0,'msg'=>'请选择打款项目']);exit(0);
            }
            $where['c.id'] = $id;
            $order = db('cash')->alias('c')->where($where)->join('userinfo u','c.vid=u.uvid')->find();
            if($order['status'] == 0){
                return json(['code'=>0,'msg'=>'此项已经打款']);exit(0);
            }
            if($order){
                $id = $order['id'];
                $out_biz_no = 'TX'.time().$order['vid'].$order['id']; 
                $payee_account = $order['alipay'];
                $amount = $order['money'];
                $payee_real_name = $order['name'];
                $payer_show_name = $system['name'].'提现打款';
                $remark = '提现打款订单号ID:'.$order['id'];
                $request->setBizContent("{" .
                "\"out_biz_no\":\"{$out_biz_no}\"," .
                "\"payee_type\":\"ALIPAY_LOGONID\"," .
                "\"payee_account\":\"{$payee_account}\"," .
                "\"amount\":\"{$amount}\"," .
                "\"payer_show_name\":\"{$payer_show_name}\"," .
                "\"payee_real_name\":\"{$payee_real_name}\"," .
                "\"remark\":\"{$remark}\"" .
                "}");
                $result = $aop->execute($request);                     
                $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
                $resultCode = $result->$responseNode->code;
                $oid = $result->$responseNode->order_id;
                if(!empty($resultCode)&&$resultCode == 10000){
                    Db('cash')->where('id',$id)->update(array('status'=>0,'remark'=>$remark."成功,支付宝订单号:".$oid));
                    return json(['code'=>1,'msg'=>' 打款完成']); 
                } else {
                    Db('cash')->where('id',$id)->update(array('remark'=>"失败"));
                    return json(['code'=>0,'msg'=>'打款失败'.$resultCode]); 
                }
            }
        }*/                
                   
    }
    //提现驳回
    public function cashbohui()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $cash = Db('cash')->where('id',$data['id'])->find();

            $user = Db('users')->where('vid',$cash['vid'])->find();

            $money = $cash['money']+$user['money'];
            $users = Db('users')->where('vid',$cash['vid'])->update(['money'=>$money]);
            $cash = Db('cash')->where('id',$data['id'])->update(['status'=>2]);
            if ($users || $cash) {
                return json(['code'=>1,'msg'=>'驳回成功']);
            }else{
                return json(['code'=>0,'msg'=>'驳回失败']);
            }
        }
    }
    //批量驳回
    public function bohuiAll()
    {
        $map[] =array('id','IN',input('param.ids/a'));
        db('order')->where($map)->update(['status'=>2]);
        $result['msg'] = '驳回成功！';
        $result['code'] = 1;
        return $result;
    }
    //批量审核
    public function shenheAll(){
        if (Request::isAjax()) {
            //$data = Request::post();
            $ids = input('param.ids/a');
            if(empty($ids)){
                return json(['code'=>0,'msg'=>'参数错误','url'=>url('order/loan')]);
            }
            foreach ($ids as $key => $value) {
                $order = Db('order')->where('id',$value)->find();
                //var_dump($order);die;
                $loan = Db('loan')->where('id',$order['pid'])->find();

                $fan_id1 = Db('users')->where('vid',$order['fan_id1'])->find();
                $fanid1_money = $fan_id1['money'];
                $fan_id2 = Db('users')->where('vid',$order['fan_id2'])->find();
                $fanid2_money = $fan_id2['money'];
                $fan_id3 = Db('users')->where('vid',$order['fan_id3'])->find();
                $fanid3_money = $fan_id3['money'];

                $money1 = $order['lev1_money'] +$fanid1_money;
                $money2 = $order['lev2_money'] +$fanid2_money;
                $money3 = $order['lev3_money'] +$fanid3_money;

                $res1 = Db('users')->where('vid',$order['fan_id1'])->update(['money'=>$money1]);
                $res2 = Db('users')->where('vid',$order['fan_id2'])->update(['money'=>$money2]);
                $res3 = Db('users')->where('vid',$order['fan_id3'])->update(['money'=>$money3]);
                $res4 = Db('order')->where('id',$order['id'])->update(['status'=>0]);
                if ($res1 ||$res2||$res3 ||$res4) {
                    //return json(['code'=>1,'msg'=>'审核成功','url'=>url('order/loan')]);
                    continue;
                }else{
                    return json(['code'=>0,'msg'=>'审核失败订单ID:'.$value,'url'=>url('order/loan')]);
                    exit(0);
                }
            }
            return json(['code'=>1,'msg'=>'审核成功','url'=>url('order/loan')]);
        }
    }
    //提现批量打款
    public function payAll(){
        $table = db('system');
        $system = $table->field('name,appid,rsakey,alipaykey')->find(1);
        if (Request::isAjax()) {
            $data = Request::post();
            require_once Env::get('EXTEND_PATH') . 'Alipay/AopSdk.php';    
            $aop = new \AopClient();
            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $system['appid'];
            $aop->rsaPrivateKey = $system['rsakey'];
            $aop->apiVersion = '1.0';
            $aop->format = 'json';
            $aop->charset = 'utf-8';
            $aop->signType = 'RSA2';
            $aop->alipayrsaPublicKey = $system['alipaykey'];
            $request = new \AlipayFundTransToaccountTransferRequest ();
           // var_dump($data);die;
            $ids = $data['ids'];
            if(!$ids){
                return json(['code'=>0,'msg'=>'请选择打款项目']);exit(0);
            }
            foreach ($ids as $key => $value) {
                # code...
                $id = $value;
                $where['c.id'] = $id;
                $order = db('cash')->alias('c')->where($where)->join('userinfo u','c.vid=u.uvid')->find();
                if($order['status'] == 0){
                    continue;
                }
                //var_dump($system);die;
                if($order){
                    $out_biz_no = 'TX'.time().$order['vid'].$order['id']; 
                    $payee_account = $order['alipay'];
                    $amount = $order['money'];
                    $payee_real_name = $order['name'];
                    $payer_show_name = $system['name'].'提现打款';
                    $remark = '提现打款订单号ID:'.$order['id'];
                    $request->setBizContent("{" .
                    "\"out_biz_no\":\"{$out_biz_no}\"," .
                    "\"payee_type\":\"ALIPAY_LOGONID\"," .
                    "\"payee_account\":\"{$payee_account}\"," .
                    "\"amount\":\"{$amount}\"," .
                    "\"payer_show_name\":\"{$payer_show_name}\"," .
                    "\"payee_real_name\":\"{$payee_real_name}\"," .
                    "\"remark\":\"{$remark}\"" .
                    "}");
                    $result = $aop->execute($request);                     
                    $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
                    $resultCode = $result->$responseNode->code;
                    $oid = $result->$responseNode->order_id;
                    //var_dump($result);die;
                    if(!empty($resultCode)&&$resultCode == 10000){
                        Db('cash')->where('id',$id)->update(array('status'=>0,'remark'=>$remark."成功,支付宝订单号:".$oid));
                    } else {
                        Db('cash')->where('id',$id)->update(array('remark'=>"失败"));
                    }
                }
            }
            return json(['code'=>1,'msg'=>' 打款完成']);
            //return json(['code'=>0,'msg'=>'驳回失败']);
        }      
    }

}
<?php
namespace app\admin\controller;
use think\Db;
use clt\Tree;
use think\facade\Request;
use think\facade\Env;
class Category extends Common
{
    public function loanList()
    {
        if(request()->isPost()) {
            $val=input('post.val');
            $list =Db::name('loancate')->order('sort')->select();
            foreach ($list as $k =>$v){
                $data=db('loancate')->where('id',$v['pid'])->value('title');
                if($data){
                    $list[$k]['sjtitle']=$data;
                }else{
                    $list[$k]['sjtitle']="无";
                }

            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return $this->fetch('loan/cateindex');
    }
    //网贷分类添加
    public function loanAdd()
    {
        if(request()->isPost()){
            $data = Request::except('file');
            db('loancate')->insert($data);
            $result['code'] = 1;
            $result['msg'] = '网贷分类添加成功!';
            $result['url'] = url('loanList');
            cache('loanList', NULL);
            return $result;
        }else{
            $loan =Db::table('clt_loancate')->select();
            $this->assign('title',lang('add').lang('网贷分类'));
            $this->assign('info','null');
            return $this->fetch('loan/form',['cate'=>$loan]);
        }
    }

    //修改网贷分类排序
    public function loanOrder()
    {
        $data = input('post.');
        if(db('loancate')->update($data)!==false){
            cache('linkList', NULL);
            return $result = ['msg' => '操作成功！','url'=>url('index'), 'code' => 1];
        }else{
            return $result = ['code'=>0,'msg'=>'操作失败！'];
        }
    }

    //网贷分类删除
    public function loanDel()
    {
        db('loancate')->where(array('id'=>input('id')))->delete();
        cache('loanList', NULL);
        return ['code'=>1,'msg'=>'删除成功！'];
    }

    //修改网贷分类
    public function loanedit()
    {
        if(request()->isPost()){
            $data = Request::except('file');
            db('loancate')->update($data);
            $result['code'] = 1;
            $result['msg'] = '网贷分类修改成功!';
            $result['url'] = url('loanList');
            cache('linkList', NULL);
            return $result;
        }else{
            //dump(111);die();
            $loan =Db::table('clt_loancate')->select();

            $id=input('param.id');
            $info=db('loancate')->where(array('id'=>$id))->find();
           // dump($info);die();
            $this->assign('info',json_encode($info,true));
            $this->assign('title',lang('edit').lang('网贷分类'));
            return $this->fetch('loan/form',['cate'=>$loan]);
        }
    }
    /**************************新闻资讯分类********************************/
    //资讯列表
    public function index()
    {
        if(request()->isPost()) {
            $val=input('post.val');
            $list =Db::name('category')->order('sort')->select();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return $this->fetch('category/index');

    }

    //资讯编辑
    public function edit()
    {
        $id = input('param.id');
        $cate = Db::name('category')->where('c_id',$id)->find();
        return view('category/edit',['cate'=>$cate]);
    }
    //保存数据
    public function update()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $id =$data['c_id'];
            unset($data['c_id']);
            if (Db::name('category')->where('c_id',$id)->update($data)) {
                return json(['code'=>1,'msg'=>'修改成功','url'=>url('index')]);
            }else{
                return json(['code'=>0,'msg'=>'修改失败']);  
            }
        }
    }

    //资讯排序
    public function order()
    {
        $data = input('post.');
        if(db('category')->update($data)!==false){
            cache('index', NULL);
            return $result = ['msg' => '操作成功！','url'=>url('index'), 'code' => 1];
        }else{
            return $result = ['code'=>0,'msg'=>'操作失败！'];
        }
    }
}
<?php
namespace app\admin\controller;
use think\Db;
use think\facade\Request;
class Help extends Common
{
    public function initialize(){
        parent::initialize();
    }
    //帮助中心分类列表
    public function index()
    {
        if(request()->isPost()) {
            $val=input('post.val');
            $list=db('helpcate')->order('sort')->select();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return $this->fetch('help/helpcate');
    }

     public function cateAdd()
    {
        if(request()->isPost()){
            $data = Request::except('file');
            db('helpcate')->insert($data);
            $result['code'] = 1;
            $result['msg'] = '添加成功!';
            $result['url'] = url('index');
            cache('index', NULL);
            return $result;
        }else{
            $this->assign('title',lang('add').lang('帮助中心分类'));
            $this->assign('info','null');
            return $this->fetch('form');
        }
    }

    //修改帮助中心
    public function edit()
    {
    	$id=input('id');
    	$cate = Db('helpcate')->where('id',$id)->find();
    	$this->assign('cate',$cate);
    	return view('helpedit');
    }
    //保存修改数据
    public function cateUpdate()
    {
    	if (Request::isAjax()) {
    		$data = Request::except('file');
    		 $id = $data['id'];
    		 unset($data['id']);
    		 if (Db('helpcate')->where('id',$id)->update($data)) {
    		 	return json(['code'=>1,'msg'=>'修改成功','url'=>url('index')]);
    		 }else{
    		 	return json(['code'=>0,'msg'=>'修改失败']);
    		 }
    	}
	}

	 //删除
    public function cateDel()
    {
    	db('helpcate')->where(array('id'=>input('id')))->delete();
        cache('index', NULL);
        return ['code'=>1,'msg'=>'删除成功！'];
    }

    //修改排序
    public function cateOrder()
    {
    	$data = input('post.');
        if(db('helpcate')->update($data)!==false){
            cache('index', NULL);
            return $result = ['msg' => '操作成功！','url'=>url('index'), 'code' => 1];
        }else{
            return $result = ['code'=>0,'msg'=>'操作失败！'];
        }
    }
	/************************************************/
	//帮助中心文章列表
    public function artindex()
    {
        if(Request::isAjax()){
           $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('help')->alias('h')
            	->join(config('database.prefix').'helpcate c','h.c_id = c.id','right')
                ->where('h.htitle','like',"%".$key."%")
                ->order('h.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch('artindex');
    }

    //文章中心添加
    public function artAdd()
    {
    	$cate = Db('helpcate')->select();
    	$this->assign('cate',$cate);
    	return view('artadd');
    }

    //保存护甲
    public function artSave()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $data['content'] = htmlspecialchars($data['content']);
            if (Db::name('help')->insert($data)) {
                return json(['code'=>1, 'msg'=>'添加成功','url'=> url('artindex')]);
            }else{
                return json(['code'=>0, 'msg'=>'添加失败']);
            }
        }
    }
    //编辑
    public function artEdit()
    {
		$id =	input('param.id');
		$cate = Db::name('helpcate')->select();
		$list = Db::name('help')->where('id',$id)->find();
		return view('artEdit',['cate'=>$cate,'art'=>$list]);
    }

    //保存
    public function artUpdate()
    {
		if (Request::isAjax()){
	        $data = Request::post();
	        $id = $data['id'];
	        unset($data['id']);
	        $data['content'] = htmlspecialchars($data['content']);
	        $res = Db('help')->where('id',$id)->update($data);
	        if ($res) {
	              return json(['code'=>1, 'msg'=>'更新成功','url'=>('artindex')]);
	        }else{
	            return json(['code'=>0, 'msg'=>'更新失败']);
	        }
    	}
    }
     //删除
    public function artDel()
    {
    	db('help')->where(array('id'=>input('id')))->delete();
        cache('artindex', NULL);
        return ['code'=>1,'msg'=>'删除成功！'];
    }
}
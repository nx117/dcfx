<?php
namespace app\home\controller;
use think\Db;
use think\facade\Request;
use think\facade\Session;
class Shop extends Common
{
    public function initialize(){
        parent::initialize();
    }
    public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
    //我的有店
    public function mydian()
    {
        $this->login();
        return view('mydian');
    }
    //我的有店首页
    public function index()
    {
        $this->login();
        $vid = Session::get('vid');
        $shop = Db('shop')->where('vid',$vid)->select();
        $this->assign('shop',$shop);
    	$this->assign('empty','<div class="no-message-box"><img src="/static/home/images/nocp_icon.png" alt="" /><span>暂无店铺</span></div>');
        return view('index');
    }

    //添加商品
    public function add()
    {
        $vid = Session::get('vid');
        $this->assign('vid',$vid);
    	return view('add');
    }
    //保存数据
    public function insert()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $data['code']= 'DDBH' . date('YmdHis') . mt_rand(1000, 9999);
            $data['ctime']=time();
            if (Db('shop')->insert($data)) {
                return json(['code'=>1,'msg'=>'保存成功']);
            }else{
                return json(['code'=>0,'msg'=>'保存失败']);
            }
        }
    }

    //编辑数据
    public function edit()
    {
        $id = input('id');
        $shop = Db('shop')->where('id',$id)->find();
        $this->assign('shop',$shop);
        return view('edit');
    }
    //保存编辑数据
    public function update()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $id = $data['id'];
            unset($data['id']);
            if (Db('shop')->where('id',$id)->update($data)) {
                return json(['code'=>1,'msg'=>'保存成功']);
            }else{
                return json(['code'=>0,'msg'=>'保存失败']);
            }
        }
    }
    //删除
    public function del()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            if (Db('shop')->where('id',$data['id'])->delete()) {
                return json(['code'=>1,'msg'=>'删除成功']);
            }else{
                return json(['code'=>0,'msg'=>'删除失败']);
            }
        }
    }

    //有店海报
    public function newmyposter()
    {
        $this->login();
        $vid = Session::get('vid');
        $code = input('code');
        $sys =Db::name('system')->where('id',1)->find();
        $arr_w =explode(',',$sys['tgw']);
        require_once dirname( $_SERVER['DOCUMENT_ROOT']).'/extend/zh/phpqrcode.php';
        $uploads_dir =($_SERVER['DOCUMENT_ROOT']).'/uploads/users/';
        $pic = $uploads_dir.$vid.'.tttg.png';
        $qr_pic_tmp = $uploads_dir.$vid.'qrcode_de_tmp.png';
        $err_level = 'L';
        $matri_size = 3;//图片大小
        
        $arr = explode(',', $sys['feng']);
        $herf = $arr[array_rand($arr,1)];
        $url = $herf. '/home/shop/supermarket.html?code='.$code.'&type=1&vid='.$vid;

        \QRcode::png($url,$qr_pic_tmp, $err_level, $matri_size, 2);
        // $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] .'/w.png';
        $backagroud_pic = $_SERVER['DOCUMENT_ROOT'].$sys['tglogo'];
        $bg = imagecreatefromstring(file_get_contents($backagroud_pic));
        $qr = imagecreatefromstring(file_get_contents($qr_pic_tmp));
        $qr_width = imagesx($qr);//logo图片宽度
        $qr_height = imagesy($qr);//logo图片高度
        //重新组合图片并调整大小
        imagecopyresampled($bg, $qr,$arr_w[0],$arr_w[1],$arr_w[2],$arr_w[3],$arr_w[4],$arr_w[5],$qr_width, $qr_height);
        imagepng($bg, $pic);//生成图片
        @unlink($qr_pic_tmp);
        $this->assign('code',$code);
        return view('newmyposter',['user'=>$user,'id'=>$id,'vid'=>$vid]);
        die;
    }
    //有店详情
    public function supermarket()
    {
        $code = input('code');
        $vid = input('vid');
        $shop = Db('shop')->where('vid',$vid)->find();
        $this->assign('shop',$shop);
        $this->assign('code',$code);
        $this->assign('vid',$vid);
        $type = input('type');
        if ($type ==1) {
            $r = Db('loan')->where('tuijian',2)->select();
            $this->assign('r',$r);
            $loan = Db('loan')->select();
            $this->assign('loan',$loan);
            return view('daikuan');
        }
        if ($type ==2) {
            $bank = Db('bank')->select();
            $this->assign('bank',$bank);
            return view('banka');
        }
        if ($type ==3) {
            $bank = Db('bank')->select();
            $this->assign('bank',$bank);
            return view('chaxun');
        }
    }

    //数据详情首页
    public function customer()
    {
        $code = input('code');
        $this->assign('code',$code);
        $shop = Db('shop')->where('code',$code)->find();
        $this->assign('shop',$shop);
        $bank = Db('bank')->select();
        $this->assign('bank',$bank);
        $loan = Db('loan')->select();
        $this->assign('loan',$loan);
        $tot = Db('order')->where('code',$code)->count();
        return view('custome',['tot'=>$tot]);
    }
}
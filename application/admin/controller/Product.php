<?php
namespace app\admin\controller;
use think\Db;
use clt\Tree;
use think\facade\Request;
use think\facade\Env;

class Product extends Common
{
    //网贷
	public function loanList()
	{
        if(Request::isAjax()){
            $val=input('val');
            $url['val'] = $val;
            $this->assign('testval',$val);
            $map='';
            if($val){
                $map['title']= array('like',"%".$val."%");
            }
            $list=Db::table(config('database.prefix').'loan')
                ->where($map)
                ->order('status desc')
                ->select();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return view('loan/index');
	}

	//网贷添加
	public function loanAdd()
	{
        if(Request::isAjax()){
            $data = input('post.');
            //判断是cps还是cpa
//            if($data['lilv_price']==2){
//                $percent_price=$data['mout'];
//                $mout ="";
//            }else if($data['lilv_price']==1){
//                $mout=$data['mout'];
//                $percent_price ="";
//            }

            $herf = 'http://tihuakeji.com/customer';
            if ($data['loan_type']==1){
                $herf=  $data['href'];
            }

            $loan_data = array(
                'title' =>$data['title'],

                'w'=>$data['w'],
                'loan_id' =>  $data['loan_type'],
                'add_time'=>time(),
                'logo' =>$data['logo'],
                'max_money' =>$data['max_money'],
                'href' =>$herf,
                'success' =>$data['success'],
                'sort' =>$data['sort'],
//                'mout' =>$mout,
                'bgpic' =>$data['bgpic'],
                'content' =>htmlspecialchars($data['content']),
                'num' =>$data['num'],
                'jiesuan' =>$data['jiesuan'],
                'jiangjin' =>$data['jiangjin'],
                'annual_interest_rate' =>$data['annual_interest_rate'],
//                 'month_term_star'=>$data['month_term_star'],
//                 'month_term_end'=>$data['month_term_end'],
//                'percent_price' =>$percent_price,
                'status'=>$data['status'],
                'expenses' => $data['expenses'],
                'equity'=> $data['equity'],
                'cptd'=> $data['cptd'],
                'commisson'=>$data['commisson'],
                'vip_commisson'=>$data['vip_commisson'],
                'img'=>$data['img'],
                  'biaoqian1'=>$data['biaoqian1'],
              'biaoqian2'=>$data['biaoqian2'],
//                'total'=>$data['total'],
                'total_mout'=>$data['total'],
                'jfimg'=>$data['jfimg'],
                'jfname'=>$data['jfname'],
                'jftel'=>$data['jftel']
                );
            //添加
            if (Db::name('loan')->insert($loan_data)) {
                return ['code'=>1,'msg'=>'网贷产品添加成功!','url'=>url('loanList')];
            } else {
                return ['code'=>0,'msg'=>'网贷产品添加失败!'];
            }
        }else{
            $this->assign('info','null');

            $loan =Db::table('clt_loancate')->select();
            return view('loan/add',['cate'=>$loan]);
        }
	}

	public function loanEdit()
	{
        $id=input('param.id');
		$loan = Db::name('loan')->where('id',$id)->find();
            $cate = Db::name('loancate')
                ->where('type','loan')
                ->select();

        return view('loan/edit',['loan'=>$loan,'cate'=>$cate,'lid'=>$loan['cid']]);
    }
    public function loanUpdate()
    {
        if (Request::isAjax()) {
            $data = Request::except('file');
            // var_dump($data);
            $id = $data['id'];
//            //判断是cps还是cpa
//                if($data['lilv_price']==2){
//                $percent_price=$data['mout'];
//                $mout ="";
//            }else if($data['lilv_price']==1){
//                $mout=$data['mout'];
//                $percent_price ="";
//            }
            //dump($data);die();


            $herf = 'http://tihuakeji.com/customer';
            if ($data['loan_type']==1){
                $herf=  $data['href'];
            }


            $loan_data = array(
                'title' =>$data['title'],
//                'loan_id' => implode(",", $data['cate']),
                'loan_id' => $data['loan_type'],
                'add_time'=>time(),
                'logo' =>$data['logo'],
                'href' =>$herf,
                'img' =>$data['img'],
                'jfimg'=>$data['jfimg'],
                'success' =>$data['success'],
                'sort' =>$data['sort'],
//                'mout' =>$mout,
                'bgpic' =>$data['bgpic'],
                'jiesuan' =>$data['jiesuan'],
//                'content' =>htmlspecialchars($data['content']),
                'max_money' =>$data['max_money'],
                'num' =>$data['num'],
                'jiangjin' =>$data['jiangjin'],
//                'percent_price' =>$percent_price,
                'annual_interest_rate' =>$data['annual_interest_rate'],
//                 'month_term_star'=>$data['month_term_star'],
//                 'month_term_end'=>$data['month_term_end'],
                'w'=>$data['w'],
                'status'=>$data['status'],
                'commisson'=>$data['commisson'],
                'vip_commisson'=>$data['vip_commisson'],
                'expenses' => $data['expenses'],
                'equity'=> $data['equity'],
                'cptd'=> $data['cptd'],
                'biaoqian1'=>$data['biaoqian1'],
                'biaoqian2'=>$data['biaoqian2'],
//                'total'=>$data['total'],
                'total_mout'=>$data['total'],
                'jfname'=>$data['jfname'],
                'jftel'=>$data['jftel']
                );
            if (Db::name('loan')->where('id',$id)->update($loan_data)) {
                  return json(['code'=>1, 'msg'=>'更新成功','url'=>url('loanList')]);
            }else{
                return json(['code'=>0, 'msg'=>'更新失败']);
            }
        }

    }


    //删除
    public function loanDel()
    {
    	db('loan')->where(array('id'=>input('id')))->delete();
        cache('loanList', NULL);
        return ['code'=>1,'msg'=>'删除成功！'];
    }

    //修改排序
    public function loanOrder()
    {
    	$data = input('post.');
        if(db('loan')->update($data)!==false){
            cache('loanList', NULL);
            return $result = ['msg' => '操作成功！','url'=>url('loanList'), 'code' => 1];
        }else{
            return $result = ['code'=>0,'msg'=>'操作失败！'];
        }
    }


    /*-----------------------推荐管理--------------------Recommend--*/


    public function recommendList()
    {

        if(Request::isAjax()){
            $val=input('val');
            $url['val'] = $val;
            $this->assign('testval',$val);
            $list=Db::table(config('database.prefix').'bank')
                ->where('type',1)
                ->order('sort')
                ->select();
            foreach ($list as $k=>$v){
                $list[$k]['cname']=db('loancate')->where('id',$v['loan_id'])->value('title');
            }
            $this->assign('title','推荐列表');
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        $this->assign('type','1');
        $this->assign('title','推荐列表');
        return view('bank/index');
    }

    public function recommendAdd()
    {
        if(request()->isPost()){
            $data = Request::except('file');
//            $data['loan_id']=implode(",", $data['loan_id']);
            //dump($data);die();
            db('bank')->insert($data);
            $result['code'] = 1;
            $result['msg'] = '推荐产品添加成功!';

                $result['url'] = url('recommendList');

            cache('bankList', NULL);

            return $result;
        }else{
            $cate = Db::name('loancate')->where('pid',8)->select();
            $this->assign('cate',$cate);
            $this->assign('title',lang('add').lang('推荐产品'));
            $this->assign('info','null');

            $this->assign('type','1');
            return $this->fetch('bank/add');
        }
    }
/*-----------------------银行管理----------------------*/
    //银行列表
    public function bankList()
    {

        if(Request::isAjax()){
            $val=input('val');
            $url['val'] = $val;
            $this->assign('testval',$val);
            $list=Db::table(config('database.prefix').'bank')
                ->where('type',0)
                ->order('sort')
                ->select();
            foreach ($list as $k=>$v){
                $list[$k]['cname']=db('loancate')->where('id',$v['loan_id'])->value('title');
            }
            $this->assign('title','银行列表');
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        $this->assign('title','银行列表');
        $this->assign('type','0');
        return view('bank/index');
    }

    //银行产品添加
    public function bankAdd()
    {
        if(request()->isPost()){
            $data = Request::except('file');
//            $data['loan_id']=implode(",", $data['loan_id']);
            //dump($data);die();
            db('bank')->insert($data);
            $result['code'] = 1;
            $result['msg'] = '银行产品添加成功!';

                $result['url'] = url('bankList');

            cache('bankList', NULL);
            return $result;
        }else{
            $cate = Db::name('loancate')->where('pid',8)->select();
            $this->assign('cate',$cate);
            $this->assign('title',lang('add').lang('银行产品'));
            $this->assign('info','null');
            return $this->fetch('bank/add');
        }
    }

    //修改银行产品
    public function bankEdit()
    {
        $id=input('param.id');
        $list = Db::name('bank')->where('id',$id)->find();
        $cate = Db::name('loancate')->where('pid',8)->select();
        $this->assign('cate',$cate);
        if ($list['type'==0]){
            $this->assign('title','银行产品修改');
        }else{
            $this->assign('title','推荐产品修改');
        }
        $this->assign('cate',$cate);
        return view('bank/edit',['bank'=>$list]);
    }


    //新增卡种
    public function cardTypeAdd(){

            $data = Request::except('file');
            db('card')->insert($data);
            $result['code'] = 1;
            $result['msg'] = '信用卡种类添加成功!';
            $result['url'] = url('safeList');
            cache('bankList', NULL);
            return $result;

    }

    //查看卡种
    public function queryCardByBankId(){
        $key=input('post.key');
//        $card = Db::name('card')->where('id',1)->find();
//        $list = $card->toArray();


        $list=db('card')->alias('o')

            ->where('id',1)

            ->toArray();


        $this->assign('card',$list['data']);
        return $result = ['code'=>0,'msg'=>'获取成功!','data'=>1,'rel'=>1];
    }

    //修改银行下面有哪些卡种
    public function card()
    {
        $id=input('param.id');
       $card = Db::name('card')->where('id',1)->find();
        $this->assign('id',$id);
        $this->assign('card',$card['data']);
        return view('bank/card');
    }


    //修改银行产品数据保存
    public function bankUpdate()
    {
        if (Request::isAjax()) {
            $data = Request::except('file');
            $id = $data['id'];
            unset($data['id']);
          //  $data['content'] = htmlspecialchars($data['content']);
            $data['expenses']=htmlspecialchars($data['expenses']);
            $data['equity']=htmlspecialchars($data['equity']);
            if (Db::name('bank')->where('id',$id)->update($data)) {
                if ($data['type']==1){
                    return json(['code'=>1, 'msg'=>'推荐产品修改成功','url'=>url('recommendList')]);
                }

                return json(['code'=>1, 'msg'=>'银行产品修改成功','url'=>url('bankList')]);
            }else{
                return json(['code'=>0, 'msg'=>'银行产品修改成失败']);
            }
        }
    }

    //银行产品删除
    public function bankDel()
    {
        db('bank')->where(array('id'=>input('id')))->delete();
        cache('bankList', NULL);
        return ['code'=>1,'msg'=>'删除成功！'];
    }

    //银行产品排序
    public function bankOrder()
    {
        $data = input('post.');
        if(db('bank')->update($data)!==false){
            cache('bankList', NULL);
            return $result = ['msg' => '操作成功！','url'=>url('bankList'), 'code' => 1];
        }else{
            return $result = ['code'=>0,'msg'=>'操作失败！'];
        }
    }


    /*-----------------------保险管理----------------------*/

    public function safeList()
    {

        if(Request::isAjax()){
            $val=input('val');
            $url['val'] = $val;
            $this->assign('testval',$val);
            $list=Db::table(config('database.prefix').'safe')
                ->order('sort')
                ->select();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return view('safe/index');
    }

    //保险产品添加
    public function safeAdd()
    {
        if(request()->isPost()){
            $data = Request::except('file');
            db('safe')->insert($data);
            $result['code'] = 1;
            $result['msg'] = '保险产品添加成功!';
            $result['url'] = url('safeList');
            cache('bankList', NULL);
            return $result;
        }else{
            $this->assign('title',lang('add').lang('保险产品'));
            $this->assign('info','null');
            return $this->fetch('safe/add');
        }
    }

    //修改保险产品
    public function safeEdit()
    {
        $id=input('param.id');
        $list = Db::name('safe')->where('id',$id)->find();
        return view('safe/edit',['bank'=>$list]);
    }

    //修改保险产品数据保存
    public function safeUpdate()
    {
        if (Request::isAjax()) {
            $data = Request::except('file');
            $id = $data['id'];
            unset($data['id']);
            $data['content'] = htmlspecialchars($data['content']);
            $data['expenses']=htmlspecialchars($data['expenses']);
            $data['equity']=htmlspecialchars($data['equity']);
            if (Db::name('safe')->where('id',$id)->update($data)) {
                return json(['code'=>1, 'msg'=>'保险产品修改成功','url'=>url('safeList')]);
            }else{
                return json(['code'=>0, 'msg'=>'保险产品修改成失败']);
            }
        }
    }


    //银行产品删除
    public function safeDel()
    {
        db('safe')->where(array('id'=>input('id')))->delete();
        cache('safeList', NULL);
        return ['code'=>1,'msg'=>'删除成功！'];
    }


    //新增银行 信用卡种
    public function addCreditCardType()
    {
        if (Request::isAjax()) {
            $data = Request::except('file');
            $id = $data['id'];
            unset($data['id']);
            $data['content'] = htmlspecialchars($data['content']);
            $data['expenses']=htmlspecialchars($data['expenses']);
            $data['equity']=htmlspecialchars($data['equity']);
            if (Db::name('safe')->where('id',$id)->update($data)) {
                return json(['code'=>1, 'msg'=>'保险产品修改成功','url'=>url('safeList')]);
            }else{
                return json(['code'=>0, 'msg'=>'保险产品修改成失败']);
            }
        }
        return view('bank/addCreditCardType');
    }


    //新增银行 信用卡种
    public function editCreditCardType()
    {
        if (Request::isAjax()) {
            $data = Request::except('file');
            $id = $data['id'];
            unset($data['id']);
            $data['content'] = htmlspecialchars($data['content']);
            $data['expenses']=htmlspecialchars($data['expenses']);
            $data['equity']=htmlspecialchars($data['equity']);
            if (Db::name('safe')->where('id',$id)->update($data)) {
                return json(['code'=>1, 'msg'=>'保险产品修改成功','url'=>url('safeList')]);
            }else{
                return json(['code'=>0, 'msg'=>'保险产品修改成失败']);
            }
        }
        return view('bank/editCreditCardType');
    }



}

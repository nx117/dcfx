<?php
namespace app\admin\controller;
use think\Db;
use clt\Tree;
use think\facade\Request;
use think\facade\Env;
class Service extends Common
{
	public function index()
	{
        if(request()->isPost()) {
            $val=input('post.val');
            $list=db('service')->order('id')->select();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return $this->fetch('system/serviceindex');
	}

	public function edit()
	{
        $map['id'] = input('param.id');
        $list = Db::name('service')->where($map)->find();
        return view('system/serviceedit',['service'=>$list]);
	}
	public function update()
	{
        if (Request::isAjax()) {
            $data = Request::post();
            $id = $data['id'];
            unset($data['id']);
            $data['content'] = htmlspecialchars($data['content']);
            if (Db::name('service')->where('id',$id)->update($data)) {
                  return json(['code'=>1, 'msg'=>'修改成功','url'=>url('index')]);
            }else{
                return json(['code'=>0, 'msg'=>'修改失败']);
            }
        }
	}
}
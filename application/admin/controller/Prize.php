<?php
namespace app\admin\controller;
use think\Db;
use clt\Tree;
use think\facade\Request;
use think\facade\Env;
class Prize extends Common
{
    //网贷
	public function prizeList()
	{
        if(Request::isAjax()){
            $list=Db::table(config('database.prefix').'prizelevel')
                ->order('id asc')
                ->select();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return view('prize/index');
	}

	//网贷添加
	public function prizeAdd()
	{
        if(Request::isAjax()){
            $data = input('post.');
            //判断是cps还是cpa
            $prize_data = array(
                'min' =>$data['min'],
                'prize'=>$data['prize'],
                'createtime'=>time()
                );
            //添加
            if (Db::name('prizelevel')->insert($prize_data)) {
                return ['code'=>1,'msg'=>'添加成功!','url'=>url('prizeList')];
            } else {
                return ['code'=>0,'msg'=>'添加失败!'];
            }
        }else{
            return view('prize/add');
        }
	}

	public function prizeEdit()
	{
        $id=input('param.id');
		$prize = Db::name('prizelevel')->where('id',$id)->find();
        return view('prize/edit',['prize'=>$prize]);
    }
    public function prizeUpdate()
    {
        if (Request::isAjax()) {
            $data = input('post.');
            // var_dump($data);
            $id = $data['id'];
            //判断是cps还是cpa
            $prize_data = array(
                'min' =>$data['min'],
                'prize'=>$data['prize']
                );
            if (Db::name('prizelevel')->where('id',$id)->update($prize_data)) {
                  return json(['code'=>1, 'msg'=>'更新成功','url'=>url('prizeList')]);
            }else{
                return json(['code'=>0, 'msg'=>'更新失败']);
            }
        }

    }

    //删除
    public function prizeDel()
    {
    	db('prizelevel')->where(array('id'=>input('id')))->delete();
        return ['code'=>1,'msg'=>'删除成功！'];
    }
}
<?php

namespace app\home\controller;

use think\Controller;
use think\Db;
use app\home\controller\Alipay;
use think\facade\Env;
use think\facade\Request;
use think\facade\Session;

class Pay extends Controller
{
  public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
  public function order()
    {
      if (Request::isAjax()) {
        $vid = Session::get('vid');
        $user = Db::name('userinfo')->where('uvid',$vid)->find();
        if ($user['lev_id'] ==2) {
          return json(['code'=>1,'msg'=>'您已经购买过了！']);
        }else{
          return json(['code'=>0,'msg'=>'正在跳转支付页面...']);
          Session::set('group_id',2);
        }      
      }
    }
    public function payOrder()
    {
        $ali = new Alipay();
        $group = Db::name('group')->where('id',2)->find();
        $bianhao = $this->createOrder($group['price']);
        $ali->tradeWapPay('支付'.$group['price'],"购买".$group['title'], $bianhao, $group['price']);
    }
    public function yywg(){
        echo "应用网关";
    }
    public function payResult(){
        echo "支付成功";
        header("Refresh:0,Url=/user/index");
    }
    public function createOrder($money=100, $time = 1)
    {
      $vid = Session::get('vid');
      $group = Db::name('group')->where('id',2)->find();
      // $user = Db::name('users')->where('vid',$vid)->find();
      $user = Db::name('users')->alias('u')
                ->join('userinfo i', 'u.vid = i.uvid', 'left')
                ->where('vid',$vid)
                ->find();
      $order['sn'] = 'DDBH' . date('YmdHis') . mt_rand(1000, 9999);
      $order['money'] = $money;
      $order['ctime'] = strtotime ("now");
      $order['ptime'] = $time;
      $order['status'] = 1;
      $order['title']=2;
      $order['name']=$user['name'];
      $order['nickname']=$user['nickname'];
      $order['tel']=$user['tel'];

      $pay = Db('group')->where('id',2)->find();
      $user = Db('userinfo')->where('uvid',$vid)->find();
      $price = explode("|", $group['mout']);

      if (!empty($user['lev1'])) {
        $order['lev1_money'] = $price[0];
        $order['fan_id1'] = $user['lev1'];
      }

      if (!empty($user['lev2'])) {
        $order['lev2_money'] = $price[1];
        $order['fan_id2'] = $user['lev2'];
      }
      if (!empty($user['lev3'])) {
        $order['lev3_money'] = $price[2];
        $order['fan_id3'] = $user['lev3'];
      }
    Db('pay')->insert($order);

     return $order['sn'];
    }

    public function hddz()
    {
        $request = input('post.');
        $log = "<br />\r\n\r\n".'==================='."\r\n".date("Y-m-d H:i:s")."\r\n".json_encode($request);
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
        if (isset($request['sign_type'])) {
            $log = "<br />\r\n\r\nsign_type";
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
            $signType = $request['sign_type'];
            $alipay = new Alipay();
            $flag = $alipay->rsaCheck($request, $signType);
            if ($flag) {
                $log = "<br />\r\n\r\签名验证成功";
                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                //支付成功:TRADE_SUCCESS   交易完成：TRADE_FINISHED   签名验证成功
                if ($request['trade_status'] == 'TRADE_SUCCESS' || $request['trade_status'] == 'TRADE_FINISHED') {
                    //这里根据项目需求来写你的操作 如更新订单状态等信息 更新成功返回'success'即可

                    $out_trade_no = $request['out_trade_no'];
                    $order = Db('pay')->where('sn', $out_trade_no)->where('status', '1')->find();
                    if (!$order) {//----上次已经付费成功
                        $log = "<br />\r\n\r\上次已经付费成功";
                        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                        exit('success');
                    }
                    $log = "<br />\r\n\r\----处理中----";
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                    $order['status'] = '0';
                    $order['ptime'] = strtotime ("now");
                    $r = Db('pay')->update($order);
                    $lev_id = array(
                        'lev_id' =>2,
                        );
                    $g = Db::name('users')->where('tel',$order['tel'])->find();

                    $us = Db::name('userinfo')->where('uvid',$g['vid'])->update($lev_id);
                    $p = Db::name('pay')->where('sn',$out_trade_no)->find();
                    $user = Db::name('userinfo')->where('uvid',$g['vid'])->find();

                    $user_id1=Db::name('users')->where('vid',$user['lev1'])->find();
                    $user_id2=Db::name('users')->where('vid',$user['lev2'])->find();
                    $user_id3=Db::name('users')->where('vid',$user['lev3'])->find();
                if ($user_id1) {
                    $user_data1 = array(
                        'money'=>$p['lev1_money'] + $user_id1['money'],
                    );
                    Db::name('users')->where('vid',$user['lev1'])->update($user_data1);
                }
                if ($user_id2) {
                    $user_data2 = array(
                        'money'=>$p['lev2_money'] + $user_id2['money'],
                    );
                    Db::name('users')->where('vid',$user['lev2'])->update($user_data2);
                }
                if ($user_id3) {
                    $user_data3 = array(
                        'money'=>$p['lev3_money'] + $user_id3['money'],
                    );
                    Db::name('users')->where('vid',$user['lev3'])->update($user_data3);
                }
                    if ($r) {
                    $log = "<br />\r\n\r\----处理---成功----<br>".$data;
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                        exit('success'); //成功处理后必须输出这个字符串给支付宝
                    } else {
                        $log = "<br />\r\n\r\----处理---失败----";
                        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                        exit('fail');
                    }
                } else {
                    $log = "<br />\r\n\r\交易状态错误";
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                    exit('fail');
                }
            } else {
                $log = "<br />\r\n\r\签名参数--验证失败";
                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
                exit('fail');
            }
        } else {
            $log = "<br />\r\n\r\签名参数错误";
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ali-pay.html', $log, FILE_APPEND);
            // echo "授权";
        }
    }
}

<?php
namespace app\admin\controller;
use app\admin\model\Users as UsersModel;
use think\Db;
use think\facade\Request;
class Users extends Common{
    //会员列表
    public function index(){
        $members = db("users")->count();
        $agents = db("users")->alias('u')
                ->join(config('database.prefix').'userinfo i','u.vid = i.uvid','left')
                ->where('i.lev_id = 2')
                ->count();
        if(request()->isPost()){
            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('users')->alias('u')
                ->join(config('database.prefix').'userinfo i','u.vid = i.uvid','left')
                ->join(config('database.prefix').'group g','i.lev_id = g.id','left')
                ->where('u.nickname|i.name|u.tel','like',"%".$key."%")
                ->order('u.vid desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['reg_time'] = date('Y-m-d H:s',$v['reg_time']);
            }
            $yue = Db('users')->select();
            $w = 0;
            foreach ($yue as $k => $v) {
                $w +=$v['money'];
            }
          
            $w = number_format($w, 2, '.', '');
            $yi = Db('cash')->select();
            $y = 0;
            foreach ($yi as $k => $v) {
                $y +=$v['money'];
            }
            $y = number_format($y, 2, '.', '');
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'w'=>$w,'y'=>$y,'rel'=>1];
        }
        $this->assign('members',$members);
        $this->assign('agents',$agents);
        return $this->fetch();
    }

    public function edit($id=''){
            $info = Db('group')->where('id','<',3)->select();
            $this->assign('info',$info);
            $vid = input('vid');
            $user = Db('userinfo')->where('uvid',$vid)->find();
            $this->assign('user',$user);
            $this->assign('title',lang('edit').lang('user'));
            return $this->fetch();
    }

    public function userupdate()
    {
        if (Request::isAjax()) {
            $data = Request::Post();
            $uvid = $data['uvid'];
            unset($data['uvid']);
            $res = Db('userinfo')->where('uvid',$uvid)->update($data);
            if ( $res ) {
                return json(['code'=>1,'msg'=>'修改成功','url'=>url('index')]);
            }else{
                return json(['code'=>0,'msg'=>'修改失败']);
            }
        }
    }
//账户禁用
    public function status()
    {
        $vid=input('post.vid');
        $open=input('post.open');
        if(db('users')->where('vid='.$vid)->update(['open'=>$open])!==false){
            return ['status'=>1,'msg'=>'禁用成功!'];
        }else{
            return ['status'=>0,'msg'=>'禁用失败!'];
        }
    }

    /***********************************会员组***********************************/
    public function userGroup()
    {
        if(request()->isPost()){
            $userLevel=db('group');
            $list=$userLevel->where('id',2)->select();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return $this->fetch();
    }
    //会员组别编辑
    public function groupEdit()
    {
        $map['id'] = input('param.id');
        $list = Db::name('group')->where($map)->find();
        return view('users/groupForm',['group'=>$list]);
    }

    public function update()
    {
        if (Request::isAjax()) {
            $data = Request::post();
            $id = $data['id'];
            unset($data['id']);
            if (Db::name('group')->where('id',$id)->update($data)) {
                return json(['code'=>1,'msg'=>'用户组修改成功','url'=>url('userGroup')]);
            }else{
                return json(['code'=>0,'msg'=>'用户组修改失败']); 
            }
        }
    }
}
<?php
namespace app\home\controller;

use think\Controller;
use think\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\Config;
class Loan extends Controller
{
    public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
    public function  index(){

        $this->login();

        $data=input('post.keyword');
        if($data){
            $where[] = ['title','like','%'.$data.'%'];
        }
        $list=$where?db('loancate')->where($where)->where('pid',8)->select() :db('loancate')->where('pid',8)->select() ;
        $this->assign('list',$list);

        return $this->fetch();
    }

    public function  banklist(){
        $id=input('id');

        $list=db('bank')->where('loan_id',$id)->select();
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function about(){
        $id=input('id');
//        $type=input('type');
        $data=db('bank')->where('id',$id)->find();
        $this->assign('data',$data);
        return $this->fetch();
    }
    //贷款
    public function aboutLoan(){
        $id=input('id');
        $data=db('loan')->where('id',$id)->find();
        $this->assign('data',$data);
        return $this->fetch();
    }
    //保险
    public function aboutSafe(){
        $id=input('id');
        $data=db('safe')->where('id',$id)->find();
        $this->assign('data',$data);
        return $this->fetch();
    }

    //信用卡
    public function aboutBank(){
        $id=input('id');
        $data=db('bank')->where('id',$id)->find();
        $this->assign('data',$data);

        if($id=40){
            return view('/');
        }

      return view('/user/index');

//        return $this->fetch();
    }



    public function  checklev(){
        if(db('userinfo')->where('uvid',session('vid'))->value('lev_id')!=2){
            $this->redirect("/home/user/level");
        }
    }

    public function  loanlist(){
        $this->login();
        $id=input('id');
        $list=db('loan')->alias('a')
            ->join('clt_loancate b','a.loan_id=b.id')
            ->where(['loan_id'=>$id,'status'=>1])->field('a.*,b.title as ctitle')->select();
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function lou(){
        $this->login();
        //$this->checklev();
        $list=db('loan')->where(['loan_id'=>3,'status'=>1])
                ->select();
        $this->assign('list',$list);
        return $this->fetch();
    }

    public function  bath(){
        $data=input('post.keyword');
        if($data){
            $where[] = ['a.title','like','%'.$data.'%'];
        }
        $list=$where?db('safe')->alias('a')
            ->where($where)->select()
            :db('safe')->alias('a')
            ->select();
        $this->assign('list',$list);

        return $this->fetch();
    }
  
  	public function tuiguang(){
    	$id=input('id');
      	$this->assign('id',$id);
      	return $this->fetch();
    }
  
  	public function tuiguang1(){
    	$id=input('id');
      	$this->assign('id',$id);
      	return $this->fetch();
    }
  
  	public function tuiguang2(){
    	$id=input('id');
      	$this->assign('id',$id);
      	return $this->fetch();
    }

    public function product($id)
    {
        $this->login();
        $vid      = Session::get('vid');
        $userinfo = Db('userinfo')->where('uvid', $vid)->find();
        $this->assign('userinfo', $userinfo);
        $loan = Db('loan')->where('id', $id)->find();
        $this->assign('loan', $loan);
        Db::name('loan')->where('id', $id)->setInc('num');
        $sui = Db('loan')->where('status',1)->order('num', 'desc')->limit(8)->select();
        $this->assign('sui', $sui);
        $list = db('msg')->alias('m')
            ->join(config('database.prefix') . 'users u', 'm.vid = u.vid', 'right')
            ->where('cid', 2)
            ->where('pid', $id)
            ->limit(2)
            ->select();
        $count = Db('msg')->where('cid', 2)->where('pid', $id)->count();
        $this->assign('msg', $list);
        return view('loan/product', ['count' => $count, 'vid' => $vid]);
    }

    public function ceshi()
    {
        if (Request::isAjax()) {
            $data    = Request::post();
            $lxTotal = $data['money'] * $data['month_money'] * $data['month'] / 100;
            $emTotal = $lxTotal / $data['month'];
            $hkTotal = $lxTotal + $data['money'];
            $arr     = array(
                'lxTotal' => $lxTotal,
                'emTotal' => $emTotal,
                'hkTotal' => $hkTotal,
            );
            return $arr;
        }
    }
    //奖金结算
    public function jiesuan($id)
    {
        $vid      = Session::get('vid');
        $userinfo = Db('userinfo')->where('uvid', $vid)->find();
        $this->assign('userinfo', $userinfo);
        $this->assign('vid', $vid);
        $loan = Db('loan')->where('id', $id)->find();
        $this->assign('loan', $loan);
        return view('loan/jiesuan');
    }
    //产品评价
    public function msg($id)
    {
        $vid      = Session::get('vid');
        $userinfo = Db('userinfo')->where('uvid', $vid)->find();
        $this->assign('userinfo', $userinfo);
        $loan = Db('loan')->where('id', $id)->find();
        $this->assign('loan', $loan);
        $list = db('msg')->alias('m')
            ->join(config('database.prefix') . 'users u', 'm.vid = u.vid', 'right')
            ->where('cid', 2)
            ->where('status', 0)
            ->where('pid', $id)
            ->select();
        $this->assign('msg', $list);
        $vid = Session::get('vid');
        $this->assign('vid', $vid);
        return view('loan/msg');
    }

    //评价页面
    public function addcomment($id)
    {
        $this->login();
        $vid      = Session::get('vid');
        $userinfo = Db('userinfo')->where('uvid', $vid)->find();
        $this->assign('userinfo', $userinfo);
        $this->assign('id', $id);
        return view('addcomment');
    }
    //保存数据
    public function savemsg()
    {
        if (Request::isAjax()) {
            $data             = Request::post();
            $vid              = Session::get('vid');
            $data['vid']      = $vid;
            $data['add_time'] = time();
            $data['cid']      = 2;
            $data['status']   = 1;
            if (Db('msg')->insert($data)) {
                return json(['code' => 0, 'msg' => '评论成功']);
            } else {
                return json(['code' => 1, 'msg' => '评论失败']);
            }
        }
    }

    public function  getloaninfo(){
        $id=input('id');
        if($id){
            $loan    = Db('loan')->where('id', $id)->find();
          
            $vid     = Session::get('vid');
            if(!$vid){
                $vid=0;
            }
            $user    = Db::name('users')->where('vid', $vid)->find();
            $userinfo = Db::name('userinfo')->where('uvid', $vid)->find();
            $this->assign('userinfo', $userinfo);
            $sys   = Db::name('system')->where('id', 1)->find();
            $arr_w = explode(',', $loan['w']);
            require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/zh/phpqrcode.php';
            $uploads_dir = ($_SERVER['DOCUMENT_ROOT']) . '/uploads/loan/';
            $pic         = $uploads_dir . $vid . $id . '.qrcode.png';
            $qr_pic_tmp  = $uploads_dir . $vid . 'qrcode_de_tmp.png';
            $err_level   = 'L';
            $matri_size  = 3; //图片大小
            $arr         = explode(',', $sys['feng']);
            $herf        = $arr[array_rand($arr, 1)];
            $url         = $herf . '/loan/url/' . $id . '/' . $vid;
            \QRcode::png($url, $qr_pic_tmp, $err_level, $matri_size, 2);
            // $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] .'/w.png';
            $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] . $loan['bgpic'];
          
            $bg             = imagecreatefromstring(file_get_contents($backagroud_pic));
            $qr             = imagecreatefromstring(file_get_contents($qr_pic_tmp));
            $qr_width       = imagesx($qr); //logo图片宽度
            $qr_height      = imagesy($qr); //logo图片高度
            //重新组合图片并调整大小
            imagecopyresampled($bg, $qr, $arr_w[0], $arr_w[1], $arr_w[2], $arr_w[3], $arr_w[4], $arr_w[5], $qr_width, $qr_height);
            imagepng($bg, $pic); //生成图片
            @unlink($qr_pic_tmp);
            $loan['url'] = short_url($url);
            $loan['vid']=$vid;
            $loan['ewm']='/uploads/loan/'.$vid.$id."." ."qrcode.png";
            return json(['code' => 0, 'msg' => '获取成功','data'=>$loan]);
        }

    }
    public function  getbankinfo(){
        $id=input('id');
        if($id){
            $bank    = Db('bank')->where('id', $id)->find();
            $vid     = Session::get('vid');
            if(!$vid){
                $vid=0;
            }
            $user    = Db::name('users')->where('vid', $vid)->find();
            $userinfo = Db::name('userinfo')->where('uvid', $vid)->find();
            $this->assign('userinfo', $userinfo);
            $sys   = Db::name('system')->where('id', 1)->find();
            $arr_w = explode(',', $bank['w']);
            require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/zh/phpqrcode.php';
            $uploads_dir = ($_SERVER['DOCUMENT_ROOT']) . '/uploads/loan/';
            $pic         = $uploads_dir . $vid . $id . '.qrcode.png';
            $qr_pic_tmp  = $uploads_dir . $vid . 'qrcode_de_tmp.png';
          	
            $err_level   = 'L';
            $matri_size  = 3; //图片大小
            $arr         = explode(',', $sys['feng']);
            $herf        = $arr[array_rand($arr, 1)];
            $url         = $herf . '/bank/url/' . $id . '/' . $vid;
            \QRcode::png($url, $qr_pic_tmp, $err_level, $matri_size, 2);
            // $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] .'/w.png';
            $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] . $bank['bgpic'];
          	
            $bg             = imagecreatefromstring(file_get_contents($backagroud_pic));
            $qr             = imagecreatefromstring(file_get_contents($qr_pic_tmp));
            $qr_width       = imagesx($qr); //logo图片宽度
            $qr_height      = imagesy($qr); //logo图片高度
            //重新组合图片并调整大小
            imagecopyresampled($bg, $qr, $arr_w[0], $arr_w[1], $arr_w[2], $arr_w[3], $arr_w[4], $arr_w[5], $qr_width, $qr_height);
          	//dump($bg);dump($pic);die();
            imagepng($bg, $pic); //生成图片
            @unlink($qr_pic_tmp);
            $bank['url'] = short_url($url);
            $bank['vid']=$vid;
            $bank['ewm']='/uploads/loan/'.$vid.$id."." ."qrcode.png";
         
            return json(['code' => 0, 'msg' => '获取成功','data'=>$bank]);
        }

    }

    public function  getsafeinfo(){
        $id=input('id');
        if($id){
            $safe    = Db('safe')->where('id', $id)->find();
            $vid     = Session::get('vid');
            if(!$vid){
                $vid=0;
            }
            $user    = Db::name('users')->where('vid', $vid)->find();
            $userinfo = Db::name('userinfo')->where('uvid', $vid)->find();
            $this->assign('userinfo', $userinfo);
            $sys   = Db::name('system')->where('id', 1)->find();
            $arr_w = explode(',', $safe['w']);
            require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/zh/phpqrcode.php';
            $uploads_dir = ($_SERVER['DOCUMENT_ROOT']) . '/uploads/loan/';
            $pic         = $uploads_dir . $vid . $id . '.qrcode.png';
            $qr_pic_tmp  = $uploads_dir . $vid . 'qrcode_de_tmp.png';
            $err_level   = 'L';
            $matri_size  = 3; //图片大小
            $arr         = explode(',', $sys['feng']);
            $herf        = $arr[array_rand($arr, 1)];
            $url         = $herf . '/safe/url/' . $id . '/' . $vid;
            \QRcode::png($url, $qr_pic_tmp, $err_level, $matri_size, 2);
            // $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] .'/w.png';
            $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] . $safe['bgpic'];
            $bg             = imagecreatefromstring(file_get_contents($backagroud_pic));
            $qr             = imagecreatefromstring(file_get_contents($qr_pic_tmp));
            $qr_width       = imagesx($qr); //logo图片宽度
            $qr_height      = imagesy($qr); //logo图片高度
            //重新组合图片并调整大小
            imagecopyresampled($bg, $qr, $arr_w[0], $arr_w[1], $arr_w[2], $arr_w[3], $arr_w[4], $arr_w[5], $qr_width, $qr_height);
            imagepng($bg, $pic); //生成图片
            @unlink($qr_pic_tmp);
            $safe['url'] = short_url($url);
            $safe['vid']=$vid;
            $safe['ewm']='/uploads/loan/'.$vid.$id."." ."qrcode.png";
            return json(['code' => 0, 'msg' => '获取成功','data'=>$safe]);
        }

    }

    //网贷推广二维码
    public function tgqr($id)
    {
        $loan    = Db('loan')->where('id', $id)->find();
        $vid     = Session::get('vid');
        $user    = Db::name('users')->where('vid', $vid)->find();
        $userinfo = Db::name('userinfo')->where('uvid', $vid)->find();
        $this->assign('userinfo', $userinfo);
        $sys   = Db::name('system')->where('id', 1)->find();
        $arr_w = explode(',', $loan['w']);
        require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/extend/zh/phpqrcode.php';
        $uploads_dir = ($_SERVER['DOCUMENT_ROOT']) . '/uploads/loan/';
        $pic         = $uploads_dir . $vid . $id . '.qrcode.png';
        $qr_pic_tmp  = $uploads_dir . $vid . 'qrcode_de_tmp.png';
        $err_level   = 'L';
        $matri_size  = 3; //图片大小
        $arr         = explode(',', $sys['feng']);
        $herf        = $arr[array_rand($arr, 1)];
        $url         = $herf . '/loan/url/' . $id . '/' . $vid;
        \QRcode::png($url, $qr_pic_tmp, $err_level, $matri_size, 2);
        // $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] .'/w.png';
        $backagroud_pic = $_SERVER['DOCUMENT_ROOT'] . $loan['bgpic'];
        $bg             = imagecreatefromstring(file_get_contents($backagroud_pic));
        $qr             = imagecreatefromstring(file_get_contents($qr_pic_tmp));
        $qr_width       = imagesx($qr); //logo图片宽度
        $qr_height      = imagesy($qr); //logo图片高度
        //重新组合图片并调整大小
        imagecopyresampled($bg, $qr, $arr_w[0], $arr_w[1], $arr_w[2], $arr_w[3], $arr_w[4], $arr_w[5], $qr_width, $qr_height);
        imagepng($bg, $pic); //生成图片
        @unlink($qr_pic_tmp);
        $url = short_url($url);
        return view('tgqr', ['vid' => $vid, 'id' => $id, 'url' => $url]);
        die;
    }

    //信用卡申请页面
    public function  bankurl($id,$vid){

        return view('detail');
    }

    //网贷申请页面
    public function url($id, $vid)
    {
        //dump(111);die();
        $this->login();
        if (!empty(input('code'))) {
            $code = input('code');
            $this->assign('code', $code);
        }
        $loan = Db('loan')->where('id', $id)->find();
        if($loan['status'] == 0){
            $this->success('产品已下架', '/');die;
        }
        $this->assign('loan', $loan);
        Db::name('loan')->where('id', $id)->setInc('num');
        $sui = Db('loan')->order('num', 'desc')->limit(8)->select();
        $this->assign('sui', $sui);
        $list = db('msg')->alias('m')
            ->join(config('database.prefix') . 'users u', 'm.vid = u.vid', 'right')
            ->where('cid', 2)
            ->where('pid', $id)
            ->limit(2)
            ->select();
        $count = Db('msg')->where('cid', 2)->where('pid', $id)->count();
        $this->assign('msg', $list);
        return view('detail', ['count' => $count, 'vid' => $vid]);
    }
    //网贷马上申请
    public function dkinfo()
    {
        $id  = input('id');
        $vid = input('vid');
        if (!empty(input('code'))) {
            $code = input('code');
            $this->assign('code', $code);
        }
        $loan = Db('loan')->where('id', $id)->find();
        if($loan['status'] == 0){
            $this->success('产品已下架', '/');die;
        }
        $this->assign('loan', $loan);
        return view('dkinfo', ['vid' => $vid, 'id' => $id]);
    }

    //保存数据
    public function saveinfo()
    {
        if (Request::isAjax()) {
            $data  = Request::post();
            $vid   = Session::get('vid');
            $order = Db('order')->where('cid', 2)->where('tel', $data['ac_mobile'])->where('pid', $data['id'])->find();

            $loan = Db('loan')->where('id', $data['id'])->find();


            if (empty($data['vid'])){
                return json(['code' => 0, 'msg' => '申请失败,缺少推广者ID']);
            }

            $user = Db('userinfo')->where('uvid',$data['vid'])->find();

            if (empty($user)){
                return json(['code' => 0, 'msg' => '申请失败,未查询到推广者信息']);
            }


            if ($order) {
                return json(['code' => 3, 'msg' => '您已申请过该产品', 'url' => $loan['href']]);
            }


            $type = $loan['loan_id'];

            $lev1_money= 0;
            if ($user['lev_id']==2){
                $lev1_money = $loan['vip_commisson'];
            }else{
                $lev1_money= $loan['commisson'];
            }

            if ($data['vid']!='') {
                $loan_data['card']   = $data['card'];
                $loan_data['name']   = $data['ac_name'];
                $loan_data['tel']    = $data['ac_mobile'];
                $loan_data['code']   = $data['code'];
                $loan_data['sn']     = time();
                $loan_data['status'] = 1;
                $loan_data['ctime']  = time();
                $loan_data['cid']    = 2;
                $loan_data['pid']    = $data['id'];
                $loan_data['title']    = $loan['title'];
                $loan_data['jfname']    = $loan['jfname'];
                $loan_data['fan_id1']=$data['vid'];
                $loan_data['fan_id2']=$user['lev1'];
                $loan_data['fan_id3']=$user['lev2'];
                $loan_data['lev1_money'] = $lev1_money;

//                $bank_data['lev1_money'] = $order['total']*Config::get('app.loan_sj');



                if (Db::name('order')->insert($loan_data)) {


                    $sys  = Db('system')->where('id', 1)->find();
//                    if (!captcha_check($data['yzm'])) {
//                        return json(['code' => 2, 'msg' => '图形验证码错误']);
//                    };

                    $smsapi  = "http://api.smsbao.com/";
                    $user    = $sys['smsaccount']; //短信平台帐号
                    $pass    = md5($sys['smspassword']); //短信平台密码
                    $phone   = $loan['jftel']; //要发送短信的手机号码
//            $code    = rand(1111, 9999);
//            【鑫时代】尊敬的用户，您的本次验证码为：#code#   【鑫时代订单】客户XXX手机号XXXXXXXXX身份证号XXXXXXXXX申请XXX产品 请尽快处理


//            $messsage = '【鑫时代订单】客户XXX手机号XXXXXXXXX身份证号XXXXXXXXX申请XXX产品 请尽快处理';

                    $code = $loan_data['tel'];

                    $mes1 ='【鑫时代订单】客户：';
                    $mes2 = ',手机号：#code# ';
                    $mes3 = '。已申请';
                    $mes4 = '请尽快处理。';

                    $messsage = $mes1.$loan_data['name'].$mes2. $mes3.$loan['title'].$mes4;

                    $content = str_replace("#code#", $code, $messsage); //要发送的短信内容
                    $sendurl = $smsapi . "sms?u=" . $user . "&p=" . $pass . "&m=" . $phone . "&c=" . urlencode($content);
                    $result  = file_get_contents($sendurl);

                    //如果是银行贷款需要向客服发送通知短信
                    if ($type==2){
//
                        return json(['code' => 4,'type'=>$type, 'msg' => '身份验证通过，请联系客服', 'url' => $loan['href']]);
                    }

                    return json(['code' => 1,'type'=>$type,  'loan'=>$loan,'type'=>1,'msg' => '身份验证通过', 'url' => $loan['href']]);
                } else {
                    return json(['code' => 0, 'msg' => '申请失败']);
                }
            }else{
                return json(['code'=>0, 'msg'=>'申请失败,缺少推广者ID']);
            }

//            $loan = Db('loan')->where('id', $data['id'])->find();
//            $user = Db('users')->where('vid', $data['vid'])->find();
//
//            $user = Db('userinfo')->where('uvid', $data['vid'])->find();

//            if (!empty($data['vid'])) {
//                $loan_data['lev1_money']  = $order['total']*Config::get('app.loan_sj');
//                $loan_data['fan_id1']     = $data['vid'];
//                $loan_data['loan_status'] = 1;
//            }
//            if (!empty($user['lev1'])) {
//                $loan_data['lev2_money'] = $order['total']*Config::get('app.loan_sj')*Config::get('app.loan_ssj');
//                $loan_data['fan_id2']    = $user['lev1'];
//            }
            
            $loan_data['card']   = $data['card'];
            $loan_data['name']   = $data['ac_name'];
            $loan_data['tel']    = $data['ac_mobile'];
            $loan_data['code']   = $data['code'];
            $loan_data['sn']     = date('YmdHis') . mt_rand(1, 10);
            $loan_data['status'] = 1;
            $loan_data['ctime']  = time();
            $loan_data['cid']    = 2;
            $loan_data['pid']    = $data['id'];
            if (Db::name('order')->insert($loan_data)) {

                if ($type==1){
                    return json(['code' => 4,  'loan'=>$loan,'type'=>1,'msg' => '身份验证通过，请联系客服', 'url' => 'http://39.97.170.249/customer']);
                }

                return json(['code' => 1,'type'=>1, 'msg' => '身份验证通过', 'loan'=>$loan, 'url' => $loan['href']]);
            } else {
                return json(['code' => 0, 'msg' => '申请失败']);
            }

        }
    }
    public function checkuser()
    {
        if (Request::isAjax()) {
            $data     = Request::post();
            $vid      = Session::get('vid');
            $userinfo = Db('userinfo')->where('uvid', $vid)->find();
            if ($userinfo['lev_id'] < 2) {
                return json(['code' => 1, 'msg' => '您还不是会员，不能代理']);
            }
        }
    }
}

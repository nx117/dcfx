<?php

namespace app\admin\controller;

use think\facade\Env;
use think\Controller;
use think\facade\Session;
class Alipay extends Controller
{
	public function payToUser($out_biz_no, $payee_account, $amount, $payee_real_name,$payer_show_name,$remark)
	{
		require_once Env::get('EXTEND_PATH') . 'Alipay/AopSdk.php';
		$table = db('system');
		$system = $table->field('appid,rsakey,alipaykey')->find(1);		
		$aop = new \AopClient();
		$aop->gatewayUrl = 'http://newdc.lsweixin.cn/pay/yywg';
		$aop->appId = $system['appid'];
		$aop->rsaPrivateKey = $system['rsakey'];
		$aop->format = 'JSON';
		$aop->charset = 'utf-8';
		$aop->signType = 'RSA2';
		$aop->alipayrsaPublicKey = $system['alipaykey'];
		$request = new \AlipayFundTransToaccountTransferRequest ();
		$request->setBizContent("{" .
		"\"out_biz_no\":\"{$out_biz_no}\"," .
		"\"payee_type\":\"ALIPAY_LOGONID\"," .
		"\"payee_account\":\"{$payee_account}\"," .
		"\"amount\":\"{$amount}\"," .
		"\"payer_show_name\":\"{$payer_show_name}\"," .
		"\"payee_real_name\":\"{$payee_real_name}\"," .
		"\"remark\":\"{$remark}\"" .
		"}");
		$result = $aop->execute($request); 
		var_dump($system);die;
		$responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
		$resultCode = $result->$responseNode->code;
		if(!empty($resultCode)&&$resultCode == 10000){
			return 1;
		} else {
			return 0;
		}
	}
	public function rsaCheck($_var_4, $_var_5)
	{
		return $this->aop->rsaCheckV1($_var_4, NULL, $_var_5);
	}
}
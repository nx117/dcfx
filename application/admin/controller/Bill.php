<?php
namespace app\admin\controller;
use think\Db;
use think\facade\Request;
use think\facade\Env;
use app\admin\model\Order as OrderModel;
use app\admin\model\Pay as PayModel;
use app\admin\model\Cash as CashModel;
class Bill extends Common
{
	//网贷订单
	public function loan()
	{
        $product = db('loan')->field('id,title')->order('status desc')->select();
        //var_dump($product);
        if(Request::isAjax()){
            $key=input('post.key');
            $phone = input('post.phone');
            $starttime = input('post.starttime');
            $endtime = input('post.endtime');
            if($key){
                $where[] = ['o.pid','=',$key];
                $map[] = ['pid','=',$key];
            }
            if($phone){
                $where[] = ['u.tel','like','%'.$phone.'%'];
            }
            if($starttime && $endtime){
                $start = strtotime($starttime);
                $end = strtotime($endtime);
                $where[] = ['o.ctime','between',[$start,$end]];
                $map[] = ['ctime','between',[$start,$end]];

            }
            $all = db('order')->where($map)->count();
            $weishen = db('order')->where('status = 1')->where($map)->count();
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('order')->alias('o')
                ->join(config('database.prefix').'loan l','o.pid = l.id','left')
                ->join(config('database.prefix').'users u','o.fan_id1 = u.vid','left')
                ->join(config('database.prefix').'userinfo i','o.fan_id1 = i.uvid','left')
                ->field('o.*,l.title,u.nickname,u.tel as phone,i.lev_id as lev')
                ->where($where)
                ->where('o.cid',2)
                ->order('o.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            //var_dump(Db::table('order')->getLastSql());
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['ctime'] = date('Y-m-d H:s',$v['ctime']);
                $list['data'][$k]['sn'] = date('YmdHs',$v['sn']);
                if($v['lev']==2){
                    $list['data'][$k]['per']=db('loan')->where('id',$v['pid'])->value('vip_commisson')."%";
                }else{
                    $list['data'][$k]['per']=db('loan')->where('id',$v['pid'])->value('commisson')."%";
                }
            }
            //var_dump($list);
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1,'all'=>$all,'weishen'=>$weishen];
        }
        $this->assign('product',$product);
        return view('order/loanindex');
	}
    public  function edit(){
	    if(Request::isPost()){
            $data=$this->request->post();
            //dump($data);die();
            if(db('order')->where('id',$data['id'])->update($data)){
                return json(['code'=>1,'msg'=>'修改成功','url'=>url('loan')]);
            }else{
                return json(['code'=>0,'msg'=>'修改失败']);
            }
        }
        $id=input('id');
        $this->assign('id',$id);
        return $this->fetch();
    }


	//银行订单
	public function bank()
	{
        if(Request::isAjax()){
           $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('order')->alias('o')
                ->join(config('database.prefix').'bank b','o.pid = b.id','left')
                ->join(config('database.prefix').'users u','o.fan_id1 = u.vid','left')
                ->join(config('database.prefix').'userinfo i','o.fan_id1 = i.uvid','left')
                ->field('o.*,b.title,u.tel as phone,i.lev_id as lev')
                ->where('o.name|o.tel|o.card','like',"%".$key."%")
                ->where('o.cid',1)
                ->order('o.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['ctime'] = date('Y-m-d H:s',$v['ctime']);
//                if($v['lev_id']==2){
//                    $list['data'][$k]['per']=db('bank')->where('id',$v['pid'])->value('mout')."¥";
//                }else{
//                    $list['data'][$k]['per']=db('bank')->where('id',$v['pid'])->value('vipmout')."¥";
//                }
//                $list['data'][$k]['per']=db('bank')->where('id',$v['pid'])->value('mout')."¥";

            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view('order/bankindex');
	}

	//保险订单
    public function safe()
    {

        if(Request::isAjax()){
            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('order')->alias('o')
                ->join(config('database.prefix').'safe b','o.pid = b.id','left')
                ->join(config('database.prefix').'users u','o.fan_id1 = u.vid','left')
                ->join(config('database.prefix').'userinfo i','o.fan_id1 = i.uvid','left')
                ->field('o.*,b.title,u.tel as phone,i.lev_id as lev')
                ->where('o.name|o.tel|o.card','like',"%".$key."%")
                ->where('o.cid',3)
                ->order('o.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['ctime'] = date('Y-m-d H:s',$v['ctime']);
                if($v['lev']==2){
                    $list['data'][$k]['per']=db('safe')->where('id',$v['pid'])->value('mout')."¥";
                }else{
                    $list['data'][$k]['per']=db('safe')->where('id',$v['pid'])->value('vipmout')."¥";
                }
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view('order/safeindex');
    }
	//支付订单
	public function pay()
	{
        $allmoney = db('pay')->where(array('status'=>0))->sum('money');
        if(Request::isAjax()){
           $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('pay')->alias('p')
                ->where('p.name|p.tel','like',"%".$key."%")
                ->order('p.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['ctime'] = date('Y-m-d H:s',$v['ctime']);
                $list['data'][$k]['ptime'] = date('Y-m-d H:s',$v['ptime']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        $this->assign('all',$allmoney);
        return view('order/payindex');
	}
	//提现订单
	public function cash()
	{
        $yida = db('cash')->where(array('status'=>0))->sum('money');
        $weida = db('cash')->where(array('status'=>1))->sum('money');
        $this->assign('yida',$yida);
        $this->assign('weida',$weida);
        if(Request::isAjax()){
            $key=input('post.key');
            $status = input('post.status');
            if($key){
                $where[] = array('c.name|c.tel','like',"%".$key."%");
            }
            //var_dump($status);die;
            if(isset($status)){
                $where[] = array('c.status','=',$status);
            }
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('cash')->alias('c')
                ->join(config('database.prefix') . 'userinfo u', 'c.vid = u.uvid', 'left')
                ->where($where)
                ->order('c.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
//var_dump(Db::table('cash')->getLastSql());
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['ctime'] = date('Y-m-d H:s',$v['ctime']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view('order/cashindex');
	}

    public function fanyong($id)
    {
        //dump($id);die();
        $order = Db('order')->where('id',$id)->find();




//       if ($order['cid'] ==2) {

//           if ($order['total']==''){
//               return view('fanyong');
//           }
//
//
//
//
//           $list = Db::name('clt_recard')->alias('re')
//               ->join('clt_userinfo u', 're.vid = u.uvid', 'left')
//               ->where('re.order_id',$id)
//               ->field('re.money, u.name')
//               ->select();
//
//
//
//
//
//           $list = db('order')->alias('o')
//                ->join(config('database.prefix') . 'loan l', 'o.pid = l.id', 'left')
//                ->where('o.id',$id)
//                ->find();
//        }
//        if ($order['cid'] ==3) {
//            $list = db('order')->alias('o')
//                ->join(config('database.prefix') . 'safe s', 'o.pid = s.id', 'left')
//                ->where('o.id',$id)
//                ->find();
//        }


        if ($order['cid'] ==1) {
            $list = $order;
        }



        $list1 = Db::name('recard')->alias('re')
            ->join('userinfo u', 're.vid = u.uvid', 'left')
            ->where('re.order_id',$id)
            ->where('re.return_cash_lev',1)
            ->field('re.money, u.name')
            ->find();

           $list2 = Db::name('recard')->alias('re')
               ->join('userinfo u', 're.vid = u.uvid', 'left')
               ->where('re.order_id',$id)
               ->where('re.return_cash_lev',2)
               ->field('re.money, u.name')
               ->find();

           $list3 = Db::name('recard')->alias('re')
               ->join('userinfo u', 're.vid = u.uvid', 'left')
               ->where('re.order_id',$id)
               ->where('re.return_cash_lev',3)
               ->field('re.money, u.name')
               ->find();

           $this->assign('list1',$list1);
           $this->assign('list2',$list2);
           $this->assign('list3',$list3);

//           $this->assign('order',$list);
//        $user1 = Db('userinfo')->where('uvid',$order['fan_id1'])->find();
//        $this->assign('user1',$user1);
//        $user2 = Db('userinfo')->where('uvid',$order['fan_id2'])->find();
//        $this->assign('user2',$user2);
        /*$user3 = Db('userinfo')->where('uvid',$order['fan_id3'])->find();
        $this->assign('user3',$user3);*/
        return view('fanyong');
    }



    //支付订单
    public function index()
    {
        $allmoney = db('order')->where(array('status'=>0))->sum('total_income');
        if(Request::isAjax()){

            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('order')
                ->where('status',0)
                ->where('name|tel','like',"%".$key."%")
                ->order('ctime desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['ctime'] = date('Y-m-d H:s',$v['ctime']);
                $list['data'][$k]['sn'] = date('YmdHs',$v['sn']);

                if ( $list['data'][$k]['cid']==1){
                    $list['data'][$k]['cid']='信用卡';
                }
                if ( $list['data'][$k]['cid']==2){
                    $list['data'][$k]['cid']='贷款';
                }
                if ( $list['data'][$k]['cid']==3){
                    $list['data'][$k]['cid']='保险';
                }
                if ( $list['data'][$k]['cid']==4){
                    $list['data'][$k]['cid']='推荐';
                }
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        $this->assign('all',$allmoney);
        return view('order/billindex');
    }


}
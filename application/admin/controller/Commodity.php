<?php
namespace app\home\controller;

use think\Db;
use think\facade\Request;
use think\facade\Session;
use app\admin\model\Users as UsersModel;
class Commodity extends Common
{
    public function initialize(){
        parent::initialize();
    }
   public function login()
    {
        //判断管理员是否登录
        if (!session('vid')) {
            $this->redirect('/login');
        }
    }
    //展示全部商品
    public function index()
    {
        $this->login();
        //贷款列表
        $loanList = db('loan')
            ->where('status',1)
            ->order('sort')
            ->select();

        //保险列表
        $safeList = db('safe')
            ->where('status',1)
            ->order('sort')
            ->select();

        //信用卡列表
        $bankList = db('bank')
            ->where('status',1)
            ->order('sort')
            ->select();

        $this->assign('loanL',$loanList);
        $this->assign('bankL',$bankList);
        $this->assign('safeL',$safeList);
        return view('/commodity/index');
    }
    //快捷工具
    public function tools(){

        return $this->fetch();
    }


}
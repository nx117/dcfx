<?php
namespace app\admin\controller;
use think\Db;
use think\facade\Request;

class Article extends Common
{
	//资讯列表
	public function index()
	{
        if(Request::isAjax()){
           $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('article')->alias('a')
            	->join(config('database.prefix').'category c','a.a_id = c.c_id','left')
                ->where('a.title','like',"%".$key."%")
                ->order('a.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['add_time'] = date('Y-m-d H:s',$v['add_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view('article/index');
	}

	//资讯添加
	public function add()
	{
		$cate = Db::name('category')->select();
		return view('article/add',['cate'=>$cate]);
	}
	//资讯添加保存数据
	public function articleAdd()
	{
        if (Request::isAjax()) {
            $data = Request::except('file');
            $data['add_time'] = time();
            $data['content'] = htmlspecialchars($data['content']);
            if (Db::name('article')->insert($data)) {
                return json(['code'=>1, 'msg'=>'添加成功','url'=> url('index')]);
            }else{
                return json(['code'=>0, 'msg'=>'添加失败']);
            }
        }
	}
	//资讯编辑
	public function edit()
	{
		$id =	input('param.id');
		$cate = Db::name('category')->select();
		$list = Db::name('article')->where('id',$id)->find();
		return view('article/edit',['cate'=>$cate,'article'=>$list]);
	}

	//编辑保存数据
	public function articleUpdate()
	{
		if (Request::isAjax()){
	        $data = Request::except('file');
	        $id = $data['id'];
	        unset($data['id']);
	        $data['content'] = htmlspecialchars($data['content']);
	        if (Db::name('article')->where('id',$id)->update($data)) {
	              return json(['code'=>1, 'msg'=>'更新成功','url'=>('index')]);
	        }else{
	            return json(['code'=>0, 'msg'=>'更新失败']);
	        }
    	}
	}

	//资讯内容删除
	public function del()
	{
    	db('article')->where(array('id'=>input('id')))->delete();
        cache('index', NULL);
        return ['code'=>1,'msg'=>'删除成功！'];
	}

	//修改排序
	public function state()
	{
		$id=input('post.id');
        $status=input('post.is_tui');
        db('article')->where('id='.$id)->update(['is_tui'=>$status]);
        $result['status'] = 1;
        $result['info'] = '推荐成功!';
        $result['url'] = url('index');
        return $result;

	}

}
<?php
namespace app\admin\controller;
use think\Db;
use think\facade\Request;
class System extends Common
{
    /********************************站点管理*******************************/
    //站点设置
    public function system($sys_id=1){
        $table = db('system');
        if(Request::isAjax()) {
            $data = Request::except('file');
            if($table->where('id',1)->update($data)!==false) {
                savecache('System');
                return json(['code' => 1, 'msg' => '站点设置保存成功!', 'url' => url('system/system')]);
            } else {
                return json(array('code' => 0, 'msg' =>'站点设置保存失败！'));
            }
        }else{
            $system = $table->find($sys_id);
            $this->assign('system', json_encode($system,true));
            return $this->fetch();
        }
    }

    public function setapp($sys_id=1)
    {
       $table = db('system');
        if(Request::isAjax()) {
            $data = Request::except('file');
            if($table->where('id',1)->update($data)!==false) {
                savecache('System');
                return json(['code' => 1, 'msg' => '海报设置成功!', 'url' => url('system/setapp')]);
            } else {
                return json(array('code' => 0, 'msg' =>'海报设置失败！'));
            }
        }else{
            $system = $table->find($sys_id);
            $this->assign('system', json_encode($system,true));
            return $this->fetch();
        } 
    }
    public function alipay($sys_id=1)
    {
       $table = db('system');
        if(Request::isAjax()) {
            $data = Request::except('file');
            if($table->where('id',1)->update($data)!==false) {
                savecache('System');
                return json(['code' => 1, 'msg' => '支付宝设置成功!', 'url' => url('system/alipay')]);
            } else {
                return json(array('code' => 0, 'msg' =>'支付宝设置失败'));
            }
        }else{
            $system = $table->find($sys_id);
            $this->assign('system', json_encode($system,true));
            return $this->fetch();
        } 
    }    
}

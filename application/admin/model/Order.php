<?php
namespace app\admin\model;

use think\Model;

class Order extends Model
{
	protected $name = 'order';
    protected $type       = [
        // 设置addtime为时间戳类型（整型）
        'ctime' => 'timestamp:Y-m-d H:i:s',
    ];
}
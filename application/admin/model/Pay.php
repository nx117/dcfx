<?php
namespace app\admin\model;

use think\Model;

class Pay extends Model
{
	protected $name = 'pay';
    protected $type       = [
        // 设置addtime为时间戳类型（整型）
        'ctime' => 'timestamp:Y-m-d H:i:s',
        'ptime' => 'timestamp:Y-m-d H:i:s',
    ];
}
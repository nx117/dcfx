<?php
namespace app\admin\model;

use think\Model;

class Article extends Model
{
	protected $name = 'article';
    protected $type       = [
        // 设置addtime为时间戳类型（整型）
        'add_time' => 'timestamp:Y-m-d H:i:s',
    ];
}
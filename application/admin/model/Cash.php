<?php
namespace app\admin\model;

use think\Model;

class Cash extends Model
{
	protected $name = 'cash';
    protected $type       = [
        // 设置addtime为时间戳类型（整型）
        'reg_time' => 'timestamp:Y-m-d H:i:s',
    ];

}
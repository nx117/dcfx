<?php /*a:3:{s:66:"/www/wwwroot/39.97.170.249/application/admin/view/users/index.html";i:1571456062;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend><?php echo lang('user'); ?><?php echo lang('list'); ?></legend>
    </fieldset>
    <div class="demoTable">
        <div class="layui-inline">
            <input class="layui-input" name="key" id="key" placeholder="<?php echo lang('pleaseEnter'); ?>关键字">
        </div>
        <button class="layui-btn" id="search" data-type="reload">搜索</button>

    <a class="layui-btn layui-btn-danger" id="wei">未提现：<b></b>元</a>
    <a class="layui-btn layui-btn-normal" id="yi">已提现：<b></b>元</a>
    <a class="layui-btn layui-btn-primary">注册用户:<?php echo htmlentities($members); ?>&nbsp;|&nbsp;VIP:<?php echo htmlentities($agents); ?></a>
    </div>
    <table class="layui-table" id="list" lay-filter="list"></table>
</div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script type="text/html" id="lev1">
    {{# if(d.lev1 >0){ }}
    {{d.lev1_name}}({{d.lev1}})
    {{# }else{  }}
    无
    {{# } }}
</script>
<script type="text/html" id="lev2">
    {{# if(d.lev2 >0){ }}
    {{d.lev2_name}}({{d.lev2}})
    {{# }else{  }}
    无
    {{# } }}
</script>
<script type="text/html" id="lev3">
    {{# if(d.lev3 >0){ }}
    {{d.lev3_name}}({{d.lev3}})
    {{# }else{  }}
    无
    {{# } }}
</script>
<script type="text/html" id="action">
    <a href="<?php echo url('edit'); ?>?vid={{d.vid}}" class="layui-btn layui-btn-xs">编辑</a>
</script>
<script type="text/html" id="open">
    <input type="checkbox" name="open" value="{{d.vid}}" lay-skin="switch" lay-text="禁用|正常" lay-filter="open" {{ d.open == 1 ? 'checked' : '' }}>
</script>
<script>
    layui.use(['table','form'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery;
        var tableIn = table.render({
            id: 'user',
            elem: '#list',
            url: '<?php echo url("index"); ?>',
            method: 'post',
            page: true,
            cols: [[
                {field: 'vid', title: '用户编号', width: 100, fixed: true},
                {field: 'nickname', title: '<?php echo lang("nickname"); ?>', width: 120},
                {field: 'tel', title: '电话号码', width: 120},
                {field: 'name', title: '姓名', width: 100},
                {field: 'title', title: '用户级别', width: 100},
                {field: 'lev1_name', title: '上级(编号)', width: 150,templet: '#lev1'},
                // {field: 'lev2_name', title: '上上级(编号)', width: 150,templet: '#lev2'},
                // {field: 'lev3_name', title: '第三级(编号)', width: 150,templet: '#lev3'},
                {field: 'money', title: '账户余额', width: 100},
                {field: 'reg_time', title: '注册时间', width: 150},
                {field: 'open', align: 'center', title: '<?php echo lang("status"); ?>', width: 100, toolbar: '#open'},
                {width: 160, align: 'center', toolbar: '#action'}
            ]],
            parseData: function(res){
                $('#wei b').html(res.w);
                $('#yi b').html(res.y);
            },
            limit: 10 //每页默认显示的数量
        });
        form.on('switch(is_lock)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var id = this.value;
            var is_lock = obj.elem.checked===true?0:1;
            $.post('<?php echo url("usersState"); ?>',{'id':id,'is_lock':is_lock},function (res) {
                layer.close(loading);
                if (res.status==1) {
                    tableIn.reload();
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                    return false;
                }
            })
        });
        //搜索
        $('#search').on('click', function() {
            var key = $('#key').val();
            if($.trim(key)==='') {
                layer.msg('<?php echo lang("pleaseEnter"); ?>关键字！',{icon:0});
                return;
            }
            tableIn.reload({ page: {page: 1},where: {key: key}});
        });
        form.on('switch(open)', function(obj){
            loading =layer.load(1, {shade: [0.1,'#fff']});
            var vid = this.value;
            var open = obj.elem.checked===true?1:0;
            $.post('<?php echo url("status"); ?>',{'vid':vid,'open':open},function (res) {
                layer.close(loading);
                if (res.status==1) {
                    tableIn.reload();
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                    return false;
                }
            })
        });
        table.on('tool(list)', function(obj) {
            var data = obj.data;
            if (obj.event === 'del') {
                layer.confirm('您确定要删除该会员吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo url('usersDel'); ?>",{id:data.id},function(res){
                        layer.close(loading);
                        if(res.code===1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            tableIn.reload();
                        }else{
                            layer.msg('操作失败！',{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
            }
        });
    });
</script>
</body>
</html>
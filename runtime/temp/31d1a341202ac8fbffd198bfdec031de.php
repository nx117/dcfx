<?php /*a:3:{s:70:"/www/wwwroot/39.97.170.249/application/admin/view/users/groupForm.html";i:1569419341;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit" ng-app="hd" ng-controller="ctrl">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>用户组别编辑</legend>
    </fieldset>
    <blockquote class="layui-elem-quote" style="font-size:16px">
        银行返佣比例为固定金额 格式为 实习会员|初级代理|团队经理 ->  100|90|80  直推、第一级、第二级分别对应实习会员一级返佣 初级代理二级返佣 团队经理三级返佣
    </blockquote>
    <form class="layui-form layui-form-pane">
        <div class="layui-form-item">
            <label class="layui-form-label">用户组名称</label>
            <div class="layui-input-4">
                <input type="text" name="title" value="<?php echo htmlentities($group['title']); ?>" lay-verify="required" placeholder="输入用户组名称" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">升级费用</label>
            <div class="layui-input-4">
                <input type="number" name="price" value="<?php echo htmlentities($group['price']); ?>" lay-verify="required" placeholder="输入升级费用" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">权重</label>
            <div class="layui-input-4">
                <input type="number" name="weight" value="<?php echo htmlentities($group['weight']); ?>" lay-verify="required" placeholder="输入权重" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">
                此权重为用户对应的阅读文章的权重
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">分销返佣</label>
            <div class="layui-input-4">
                <input type="text" name="mout" value="<?php echo htmlentities($group['mout']); ?>" lay-verify="required" placeholder="输入分销返佣" class="layui-input">
            </div>
        </div>
    
        <input type="hidden" name="id" value="<?php echo htmlentities($group['id']); ?>">
          <div class="layui-form-item">
              <button  class="layui-btn" lay-filter="save" lay-submit="">
                  修改
              </button>
          </div>
    </form>
</div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script>
    layui.use(['form','layer','upload'], function(){
        $ = layui.jquery;var form = layui.form,layer = layui.layer;
          //监听提交
          form.on('submit(save)', function(data){
                loading =layer.load(1, {shade: [0.1,'#fff']});
                $.post("<?php echo url('update'); ?>",data.field,function(res){
                layer.close(loading);
                    if(res.code > 0){
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                         location.href = res.url;
                    });
                    }else{
                        layer.msg(res.msg,{icon: 2, time: 1000});
                    }
                });
                return false;
              });
        });
</script>
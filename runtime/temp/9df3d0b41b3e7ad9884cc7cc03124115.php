<?php /*a:3:{s:65:"/www/wwwroot/39.97.170.249/application/admin/view/users/edit.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit" ng-app="hd" ng-controller="ctrl">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>用户编辑</legend>
    </fieldset>
    <form class="layui-form layui-form-pane">
        <div class="layui-form-item">
            <label class="layui-form-label">所属用户组</label>
            <div class="layui-input-4">
                <select name="lev_id">
                    <option value="">请选择会员组</option>
                    <?php foreach($info as $v): ?>
                    <option <?php if(($v['id'] == $user['lev_id'])): ?>selected<?php endif; ?> value="<?php echo htmlentities($v['id']); ?>"><?php echo htmlentities($v['title']); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <input type="hidden" name="uvid" value="<?php echo htmlentities($user['uvid']); ?>">
          <div class="layui-form-item">
              <button  class="layui-btn" lay-filter="save" lay-submit="">
                  修改
              </button>
          </div>
    </form>
</div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script>
layui.use(['form','layer','upload'], function(){
        $ = layui.jquery;var form = layui.form,layer = layui.layer;
          //监听提交
          form.on('submit(save)', function(data){
                loading =layer.load(1, {shade: [0.1,'#fff']});
                $.post("<?php echo url('userupdate'); ?>",data.field,function(res){
                layer.close(loading);
                    if(res.code > 0){
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                         location.href = res.url;
                    });
                    }else{
                        layer.msg(res.msg,{icon: 2, time: 1000});
                    }
                });
                return false;
              });
        });
</script>
</body>
</html>
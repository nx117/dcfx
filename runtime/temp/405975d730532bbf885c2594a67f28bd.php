<?php /*a:1:{s:69:"/www/wwwroot/39.97.170.249/application/home/view/user/updatename.html";i:1569590646;}*/ ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="/static/home/css/style.css" />
		<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1; user-scalable=no">
		<title>昵称修改</title>
		<link rel="stylesheet" href="/static/home/css/ydui.css" />
	</head>
	<body style="background-color: #F1F4F8">
		<div class="form-cz-box">
			<span class="head-tl">请填写您的昵称</span>
			<form class="form-write-box">
				<div class="ipt-box-cl">
					<input type="text" id="name" class="nickname" value="<?php echo htmlentities($user['nickname']); ?>" placeholder='请输入您的昵称'></input>
				</div>
				<button type="button" class="btn-form" id="btn_gm">保 存</button>
			</form>
		</div>
	</body>
	<script src="/static/home/js/jquery-1.12.4.min.js"></script>
	<script src="/static/home/js/ydui.flexible.js"></script>
	<script src="/static/home/js/ydui.js"></script>
	<script>
		$("#btn_gm").click(function() {
			var name = $("#name").val();
            $.post("<?php echo url('user/savename'); ?>", {name:name}, function (data) {
            	if (data.code === 0) {
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'success', 1000 ,function(){
							location.replace("/user/myinfo");
							});
						}, 1000);
            	} else {
					YDUI.dialog.toast(data.msg, 'none', function() {});
            	}
            }, 'json');
			return false;
		});
	</script>
</html>
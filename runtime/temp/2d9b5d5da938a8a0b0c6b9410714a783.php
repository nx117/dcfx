<?php /*a:3:{s:64:"/www/wwwroot/39.97.170.249/application/admin/view/loan/edit.html";i:1569419325;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>修改网贷产品</legend>
    </fieldset>
<!--    <blockquote class="layui-elem-quote" style="font-size:18px">-->
<!--        银行返佣比例为固定金额 格式为 实习会员|初级代理|团队经理 ->  100|90|80  直推、第一级、第二级分别对应实习会员一级返佣 初级代理二级返佣 团队经理三级返佣-->
<!--    </blockquote>-->
    <form class="layui-form" lay-filter="form">
          <div class="layui-form-item">
              <label for="title" class="layui-form-label">
                  <span class="x-red">*</span>网贷名称
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="title" name="title" value="<?php echo htmlentities($loan['title']); ?>" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>


              <label for="title" class="layui-form-label">
                  <span class="x-red">*</span>贷款类型
              </label>
              <div class="layui-input-inline">
              <select class="select" name="loan_type">

                  <?php foreach($cate as $v): ?>

                  <option value="<?php echo htmlentities($v['pid']); ?>" <?php if($loan['loan_id'] == $v['pid']): ?> selected="selected" <?php endif; ?>  ><?php echo htmlentities($v['title']); ?></option>
                  <?php endforeach; ?>

              </select>
              </div>


<!--              <label for="title" class="layui-form-label">-->
<!--                  <span class="x-red">*</span>是否网贷-->
<!--              </label>-->
<!--              <div class="layui-input-inline">-->
<!--                  <input type="text"  name="title" value="<?php echo htmlentities($loan['loan_id']); ?>" required="" lay-verify="required"-->
<!--                         autocomplete="off" class="layui-input">-->
<!--              </div>-->


<!--              <label for="title" class="layui-form-label">-->
<!--                  <span class="x-red">*</span>申请人数-->
<!--              </label>-->
<!--              <div class="layui-input-inline">-->
<!--                  <input type="number" id="title" name="num" value="<?php echo htmlentities($loan['num']); ?>"  required="" lay-verify="required"-->
<!--                  autocomplete="off" class="layui-input">-->
<!--              </div>-->

              <label for="title" class="layui-form-label">
                  <span class="x-red">*</span>活动标语
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="title" name="jiangjin" value="<?php echo htmlentities($loan['jiangjin']); ?>"  required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>

              <label for="sort" class="layui-form-label">
                  <span class="x-red">*</span>排序
              </label>
              <div class="layui-input-inline">
                  <input type="number" id="L_repass" name="sort" value="<?php echo htmlentities($loan['sort']); ?>" lay-verify="sort"
                         autocomplete="off" class="layui-input">
              </div>
          </div>

          <div class="layui-form-item">


              <label for="sort" class="layui-form-label">
                  <span class="x-red">*</span>利息
              </label>
              <div class="layui-input-inline">
                  <input type="text" name="annual_interest_rate" lay-verify="title" value="<?php echo htmlentities($loan['annual_interest_rate']); ?>" autocomplete="off" placeholder="请输入利率" class="layui-input">
              </div>

<!--              <label for="sort" class="layui-form-label">-->
<!--                  <span class="x-red">*</span>利息-->
<!--              </label>-->
<!--              <div class="layui-input-inline">-->
<!--                   <input type="number" name="annual_interest_rate"  value="<?php echo htmlentities($loan['annual_interest_rate']); ?>" class="layui-input">-->
<!--              </div>-->

            
              <label for="auth" class="layui-form-label">
                  <span class="x-red">*</span>结算日期
              </label>
              <div class="layui-input-inline">
                  <input type="text" value="<?php echo htmlentities($loan['jiesuan']); ?>" name="jiesuan" required="" lay-verify="required"
                  autocomplete="off" class="layui-input">
              </div>

              <label for="auth" class="layui-form-label">
                  <span class="x-red">*</span>产品特点
              </label>
              <div class="layui-input-inline">
                  <input type="text" value="<?php echo htmlentities($loan['cptd']); ?>" name="cptd" required="" lay-verify="required"
                         autocomplete="off" class="layui-input">
              </div>
              <label for="title" class="layui-form-label">
                  <span class="x-red">*</span>额度
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="title" name="max_money" value="<?php echo htmlentities($loan['max_money']); ?>" lay-verify="required"
                         autocomplete="off" class="layui-input">
              </div>
          </div>
          
          <div class="layui-form-item">
<!--          <label for="sort" class="layui-form-label">-->
<!--                  <span class="x-red">*</span>分销返佣-->
<!--              </label>-->
<!--              <div class="layui-input-inline">-->
<!--                <select  name="lilv_price" id="lilv_price" lay-filter="aihao">-->
<!--                  <option value="1" <?php if(($loan['mout'] !=0)): ?> selected<?php endif; ?> >固定金额</option>-->
<!--                  <option value="2" <?php if(($loan['percent_price'] !=0)): ?> selected<?php endif; ?> >百分比</option>-->
<!--                </select>-->
<!--              </div>-->
<!--              <div class="layui-input-inline">-->
<!--                  <input type="text" name="mout" <?php if(($loan['mout'] !=0)): ?> value="<?php echo htmlentities($loan['mout']); ?>" <?php else: ?> value="<?php echo htmlentities($loan['percent_price']); ?>" <?php endif; ?> class="layui-input">-->
<!--              </div>-->

              <label for="title" class="layui-form-label">
                  <span class="x-red">*</span>普通返佣
              </label>
              <div class="layui-input-inline">
                  <input type="text" value="<?php echo htmlentities($loan['commisson']); ?>"  name="commisson" required="" lay-verify="required"
                         autocomplete="off" class="layui-input">
              </div>
              <label for="title" class="layui-form-label">
                  <span class="x-red">*</span>vip返佣
              </label>
              <div class="layui-input-inline">
                  <input type="text"  value="<?php echo htmlentities($loan['vip_commisson']); ?>" name="vip_commisson" required="" lay-verify="required"
                         autocomplete="off" class="layui-input">
              </div>
              <label for="auth" class="layui-form-label">
                  <span class="x-red">*</span>标签1
              </label>
              <div class="layui-input-inline">
                  <input type="text" value="<?php echo htmlentities($loan['biaoqian1']); ?>" placeholder="请输入标签" name="biaoqian1" required="" lay-verify="required"
                         autocomplete="off" class="layui-input">
              </div>

              <label for="auth" class="layui-form-label">
                  <span class="x-red">*</span>标签2
              </label>
              <div class="layui-input-inline">
                  <input type="text" value="<?php echo htmlentities($loan['biaoqian2']); ?>" placeholder="请输入标签" name="biaoqian2" required="" lay-verify="required"
                         autocomplete="off" class="layui-input">
              </div>

          </div>
        <div class="layui-form-item">
            <label for="sort" class="layui-form-label">
                <span class="x-red">*</span>总佣金
            </label>
            <div class="layui-input-inline">
                <input type="number" value="<?php echo htmlentities($loan['total_mout']); ?>" id="L_repass" name="total" required="" lay-verify="sort"
                       autocomplete="off" class="layui-input">
            </div>

        </div>
          <div class="layui-form-item">
            <label class="layui-form-label">网贷LOGO</label>
           <input type="hidden" name="logo" id="image" value="<?php echo htmlentities($loan['logo']); ?>">
            <div class="layui-input-block">
                <div class="layui-upload">
                    <button type="button" class="layui-btn layui-btn-primary" id="cateBtn"><i class="icon icon-upload3"></i>点击上传</button>
                    <div class="layui-upload-list">
                        <img class="layui-upload-img" id="cateImage" src="<?php echo htmlentities($loan['logo']); ?>" width="80px">
                        <p id="demoText"></p>
                    </div>
                </div>
            </div>
        </div>



        <div class="layui-form-item">
            <label class="layui-form-label">详情页图片</label>
            <input type="hidden" name="img" id="image4" value="<?php echo htmlentities($loan['img']); ?>">
            <div class="layui-input-block">
                <div class="layui-upload">
                    <button type="button" class="layui-btn layui-btn-primary" id="cateBtn4"><i class="icon icon-upload3"></i>点击上传</button>
                    <div class="layui-upload-list">
                        <img class="layui-upload-img" id="cateImage4" src="<?php echo htmlentities($loan['img']); ?>" width="100px">
                        <p id="demoText4"></p>
                    </div>
                </div>
            </div>
        </div>



        <div class="layui-form-item">
            <label class="layui-form-label">二维码背景</label>
            <input type="hidden" name="bgpic" id="image1" value="<?php echo htmlentities($loan['bgpic']); ?>">
            <div class="layui-input-block">
                <div class="layui-upload">
                    <button type="button" class="layui-btn layui-btn-primary" id="cateBtn1"><i class="icon icon-upload3"></i>点击上传</button>
                    <div class="layui-upload-list">
                        <img class="layui-upload-img" id="cateImage1" src="<?php echo htmlentities($loan['bgpic']); ?>" width="80px">
                        <p id="demoText1"></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">甲方图片</label>
            <input type="hidden" name="jfimg" id="imagejf" value="<?php echo htmlentities($loan['jfimg']); ?>">
            <div class="layui-input-block">
                <div class="layui-upload">
                    <button type="button" class="layui-btn layui-btn-primary" id="cateBtnJf"><i class="icon icon-upload3"></i>点击上传</button>
                    <div class="layui-upload-list">
                        <img class="layui-upload-img" id="cateImageJf" src="<?php echo htmlentities($loan['jfimg']); ?>" width="80px">
                        <p id="demoTextjf"></p>
                    </div>
                </div>
            </div>
        </div>

    <div class="layui-form-item">
        <label class="layui-form-label">
            二维码坐标
        </label>
        <div class="layui-input-block" style="width:600px">
            <input type="text" value="<?php echo htmlentities($loan['w']); ?>" name="w" autocomplete="off" class="layui-input">
            <div class="layui-form-mid layui-word-aux" style="color:red">设置格式为：88,230,0,0,140,150</div>
        </div>
    </div>
        <div class="layui-form-item">

            <label for="auth" class="layui-form-label">
                <span class="x-red">*</span>甲方姓名
            </label>
            <div class="layui-input-inline">
                <input type="text" value="<?php echo htmlentities($loan['jfname']); ?>" placeholder="请输入甲方姓名" name="jfname" required="" lay-verify="required"
                       autocomplete="off" class="layui-input">
            </div>


            <label for="auth" class="layui-form-label">
                <span class="x-red">*</span>甲方手机号 </label>
            <div class="layui-input-inline">
                <input type="text"  value="<?php echo htmlentities($loan['jftel']); ?>" name="jftel" placeholder="请输入甲方手机号" required="" lay-verify="required"
                       autocomplete="off" class="layui-input">
            </div>


        </div>
          <div class="layui-form-item">
              <label for="auth" class="layui-form-label">
                  <span class="x-red">*</span>申请网址
              </label>
              <div class="layui-input-inline">
                  <input type="text" id="L_repass" name="href" value="<?php echo htmlentities($loan['href']); ?>" lay-verify="auth"
                  autocomplete="off" class="layui-input"  style="width:890px">
              </div>

          </div>
<!--          <div class="layui-form-item">-->
<!--            <label class="layui-form-label">申请攻略</label>-->
<!--            <input type="hidden" name="content" id="image">-->
<!--            <div class="layui-input-block">-->
<!--                <script name="content" id="editor" type="text/plain" style="width:1200px;height:500px;"><?php echo htmlspecialchars_decode($loan['content']);?></script>-->
<!--            </div>-->
<!--        </div>-->

        <div class="layui-form-item">
            <label class="layui-form-label">相关费用</label>
            <input type="hidden" name="expenses" id="image">
            <div class="layui-input-block">
                <script name="expenses" id="editor1" type="text/plain" style="width:1024px;height:200px;"><?php echo htmlspecialchars_decode($loan['expenses']);?></script>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">相关权益</label>
            <input type="hidden" name="equity" id="image">
            <div class="layui-input-block">
                <script name="equity" id="editor2" type="text/plain" style="width:1024px;height:200px;"><?php echo htmlspecialchars_decode($loan['equity']);?></script>
            </div>
        </div>

          <div class="layui-form-item">
              <label for="auth" class="layui-form-label">
                  <span class="x-red">*</span>是否显示
              </label>
              <div class="layui-input-block">
                <input type="radio" name="status" value="1" title="是" <?php if($loan['status']==1): ?>checked<?php endif; ?>>
                <input type="radio" name="status" value="0" title="否" <?php if($loan['status']==0): ?>checked<?php endif; ?>>
              </div>

          </div>       
        <input type="hidden" name="id" value="<?php echo htmlentities($loan['id']); ?>">
          <div class="layui-form-item">
              <label for="L_repass" class="layui-form-label">
              </label>
              <button  class="layui-btn" lay-filter="save" lay-submit="">
                  修改
              </button>
          </div>
      </form>
    </div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


    <script>
    var ue = UE.getEditor('editor');
    var ue = UE.getEditor('editor1');
    var ue = UE.getEditor('editor2');
    layui.use(['form', 'layer','upload'], function () {
        var form = layui.form, layer = layui.layer,$= layui.jquery,upload = layui.upload; 
        var uploadInst = upload.render({
              elem: '#cateBtn',
              url: '<?php echo url("UpFiles/upload"); ?>',
              done: function(res){
                  if(res.code>0){
                      $('#image').val(res.url);
                      $('#cateImage').attr('src', res.url);
                  }else{
                      //如果上传失败
                      return layer.msg('上传失败');
                  }
              },
              error: function(){
                  //演示失败状态，并实现重传
                  var demoText = $('#demoText');
                  demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                  demoText.find('.demo-reload').on('click', function(){
                      uploadInst.upload();
                  });
              }
       });



        var uploadInst = upload.render({
            elem: '#cateBtn4',
            url: '<?php echo url("UpFiles/upload"); ?>',
            done: function(res){
                if(res.code>0){
                    $('#image4').val(res.url);
                    $('#cateImage4').attr('src', res.url);
                }else{
                    //如果上传失败
                    return layer.msg('上传失败');
                }
            },
            error: function(){
                //演示失败状态，并实现重传
                var demoText = $('#demoText4');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function(){
                    uploadInst.upload();
                });
            }
        });




        var uploadInst = upload.render({
              elem: '#cateBtn1',
              url: '<?php echo url("UpFiles/upload"); ?>',
              done: function(res){
                  if(res.code>0){
                      $('#image1').val(res.url);
                      $('#cateImage1').attr('src', res.url);
                  }else{
                      //如果上传失败
                      return layer.msg('上传失败');
                  }
              },
              error: function(){
                  //演示失败状态，并实现重传
                  var demoText = $('#demoText1');
                  demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                  demoText.find('.demo-reload').on('click', function(){
                      uploadInst.upload();
                  });
              }
       });
        //jf二维码
        var uploadInst = upload.render({
            elem: '#cateBtnJf',
            url: '<?php echo url("UpFiles/upload"); ?>',
            done: function(res){
                if(res.code>0){
                    $('#imagejf').val(res.url);
                    $('#cateImageJf').attr('src', res.url);
                }else{
                    //如果上传失败
                    return layer.msg('上传失败');
                }
            },
            error: function(){
                //演示失败状态，并实现重传
                var demoText = $('#demoTextjf');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function(){
                    uploadInst.upload();
                });
            }
        });

          //监听提交
          form.on('submit(save)', function(data){
              debugger;
                loading =layer.load(1, {shade: [0.1,'#fff']});
                $.post("<?php echo url('product/loanUpdate'); ?>",data.field,function(res){
                layer.close(loading);
                if (res.code > 0) {
                    layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                        location.href = res.url;
                    });
                    }else{
                        layer.msg(res.msg,{icon: 2, time: 1000});
                    }
                });
                return false;
              });
          
          
        });
    </script>
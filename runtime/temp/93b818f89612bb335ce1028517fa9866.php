<?php /*a:1:{s:64:"/www/wwwroot/39.97.170.249/application/home/view/user/uppwd.html";i:1569590649;}*/ ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="/static/home/css/ydui.css">
		<link rel="stylesheet" href="/static/home/css/style.css" />
		<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1; user-scalable=no">
		<title>修改密码</title>
		<script src="/static/home/js/ydui.flexible.js"></script>
	</head>

	<body style="background-color: #F1F4F8">
		<!--pages/register/register.wxml-->
		<div class="form-cz-box">
			<span class="head-tl">请设置您的密码</span>
			<div class="form-write-box">
				<div class="ipt-box-cl">
					<span>输入密码</span>
					<input class="passone" type="password" placeholder='请输入您的密码'></input>
				</div>
				<div class="ipt-box-cl">
					<span>再次输入密码</span>
					<input class="passtwo" type="password" placeholder='请再次输入您的密码'></input>
				</div>
				<button class="btn-form">提交</button>
			</div>
		</div>
		<script src="/static/home/js/jquery-1.12.4.min.js"></script>
		<script src="/static/home/js/ydui.js"></script>
		<script>
			var $passOne = $(".passone");
			var $passTwo = $(".passtwo");
			$(".btn-form").click(function(){
				if($passOne.val()==""){
					YDUI.dialog.toast('请输入您的密码！', 'none', 1000);
					return;
				}
				if($passTwo.val()==""){
					YDUI.dialog.toast('请输入您的密码！', 'none', 1000);
					return;
				}
				if($passOne.val()!=$passTwo.val()){
					YDUI.dialog.toast('请保持两次输入的密码一致!', 'none', 1000);
					return;
				}
				YDUI.dialog.loading.open('修改中...');
				$.post("<?php echo url('user/updatepwd'); ?>",{password:$passTwo.val()},function(data){
					if(data.code ==0){
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'success', 800,function(){
								location.replace("/login");
							});
						}, 800);
					}else{
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'error', 800,function(){});
						}, 800);
					}
				});
			})
		</script>
	</body>
</html>
<?php /*a:1:{s:71:"/www/wwwroot/39.97.170.249/application/home/view/user/updatewxname.html";i:1569590639;}*/ ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="/static/home/css/style.css" />
		<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1; user-scalable=no">
		<title>完善资料</title>
		<link rel="stylesheet" href="/static/home/css/ydui.css" />
	</head>
	<body style="background-color: #F1F4F8">
		<div class="form-cz-box">
			<form class="form-write-box">
				<span class="head-tl">请填写您的姓名</span>
				<div class="ipt-box-cl">
					<input type="text" id="name" class="wxname" value="<?php echo htmlentities($user['name']); ?>" placeholder='请输入您的姓名'></input>
				</div>
				<span class="head-tl">请填写您的身份证号</span>
				<div class="ipt-box-cl">
					<input type="text" id="card" class="wxname" value="<?php echo htmlentities($user['card']); ?>" placeholder='请输入您的身份证号'></input>
				</div>
				<span class="head-tl">请填写您的微信号</span>
				<div class="ipt-box-cl">
					<input type="text" id="wxname" class="wxname" value="<?php echo htmlentities($user['wxname']); ?>" placeholder='请输入您的微信号'></input>
				</div>
				<button type="button" class="btn-form" id="btn_gm">保 存</button>
			</form>
		</div>
	</body>
	<script src="/static/home/js/jquery-1.12.4.min.js"></script>
	<script src="/static/home/js/ydui.flexible.js"></script>
	<script src="/static/home/js/ydui.js"></script>
	<script>
		$("#btn_gm").click(function() {
			var wxname = $("#wxname").val();
            $.post("<?php echo url('user/savewxname'); ?>", {wxname:wxname}, function (data) {
            	if (data.code === 0) {
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'success', 1000 ,function(){
							location.replace("/user/myinfo");
							});
						}, 1000);
            	} else {
					YDUI.dialog.toast(data.msg, 'none', function() {});
            	}
            }, 'json');
			return false;
		});
	</script>
</html>
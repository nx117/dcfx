<?php /*a:3:{s:65:"/www/wwwroot/39.97.170.249/application/admin/view/bank/index.html";i:1569419331;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend><?php echo htmlentities($title); ?></legend>
    </fieldset>
    <blockquote class="layui-elem-quote">
        <?php if(($type ==1)): ?>   <a href="<?php echo url('recommendAdd'); ?>" class="layui-btn layui-btn-sm">添加更多产品</a> <?php elseif(($type ==0)): ?>
        <a href="<?php echo url('bankAdd'); ?>" class="layui-btn layui-btn-sm">添加信用卡</a><?php else: endif; ?>

    </blockquote>
    <table class="layui-table" id="list" lay-filter="list"></table>
</div>

<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script type="text/html" id="barDemo">
    <a href="<?php echo url('bankEdit'); ?>?id={{d.id}}" class="layui-btn layui-btn-xs">编辑</a>
    <?php if(($type ==0)): ?>
    <a href="<?php echo url('addCreditCardType'); ?>?id={{d.id}}" class="layui-btn layui-btn-xs">添加卡种</a>
    <?php endif; ?>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><?php echo lang('del'); ?></a>
</script>
<script type="text/html" id="order">
    <input name="{{d.id}}" data-id="{{d.id}}" class="list_order layui-input" value=" {{d.sort}}" size="10"/>
</script>
<script type="text/html" id="logo">
    <img src="{{d.logo}}" width="20px">
</script>
<script>
    layui.use(['table','form'], function() {
        url = '<?php echo url("bankList"); ?>';

        var type = '<?php echo htmlentities($type); ?>';
        if (type==1){
        url  = '<?php echo url("recommendList"); ?>'
        }


        // alert(type);
        var table = layui.table,form = layui.form,$ = layui.jquery;
        var tableIn = table.render({
            elem: '#list',
            url: url,
            method:'post',
            cols: [[
                {field:'title', title: '商品名称', width:120,fixed: true}
                // ,{field:'cname', title: '所属分类', width:120}
                ,{field:'logo', title: '商品Logo', width:100,templet: '#logo'}
                ,{field:'jfname', title: '甲方姓名', width:120}
                ,{field:'jftel', title: '甲方手机号', width:200}
                ,{field: 'sort',align: 'center',title: '<?php echo lang("order"); ?>', width: 120, templet: '#order', sort: true}
                ,{width:200,title: '操作',align:'center', toolbar: '#barDemo'}
            ]]
        });
        $('body').on('blur','.list_order',function() {
            var id = $(this).attr('data-id');
            var sort = $(this).val();
            $.post('<?php echo url("bankOrder"); ?>',{id:id,sort:sort},function(res){
                if(res.code === 1){
                    layer.msg(res.msg, {time: 1000, icon: 1});
                    table.reload('loan');
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                }
            })
        });
        table.on('tool(list)', function(obj){
            var data = obj.data;
            if(obj.event === 'del'){
                layer.confirm('<?php echo lang("确定要删除吗？"); ?>', function(index){
                    $.post("<?php echo url('bankDel'); ?>",{id:data.id},function(res){
                        if(res.code==1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            obj.del();
                        }else{
                            layer.msg(res.msg,{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
            }
        });

    });
</script>
</body>
</html>
<?php /*a:1:{s:70:"/www/wwwroot/39.97.170.249/application/home/view/user/appregister.html";i:1569590635;}*/ ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1; user-scalable=no">
		<link rel="stylesheet" href="/static/home/css/style.css" />
		<link rel="stylesheet" href="/static/home/css/ydui.css" />
		<title>注册提花科技</title>
		<style>
			body{margin: 0;padding: 0}
			/*.app-link-box{background: url(/static/home/images/main.png) no-repeat center center;background-size: 100% 100%;}*/
			.title-imgNew img{ width: 50%;margin: 0 auto;display: block;padding: 10px 0 20px 0}
			.checkress-ms-box{width: 90%;margin: 0 auto;background-color:#fff;border-radius: 5px;}
			.tzyq-img img{display: block;width: 90%;margin: 0 auto;margin-top: 50px}
			.footer-img img{display: block;width: 90%;margin: 0 auto;margin-top: 50px}
			.xieyi-p{font-size: 12px;}
			.xieyi-p a{color:#4E90E6}
			.jsp{color:#74aee7;width: 90%;padding: 20px 0 50px 0;display: block;font-size: 12px;margin: 0 auto}
			.btn-yzm{border-style: none;background-color: #4E90E6;color: #fff;margin: 6px 0;width: 100px;border-radius: 5px;outline: none;padding: 0 10px;font-size: 13px;}
			.down-app-box{display: flex;display: -webkit-flex;background-color: rgba(0,0,0,0.5);padding: 10px 0;color: #fff;position: fixed;left: 0;top: 0;width: 100%;}
			.applogo-box{padding-left: 20px;}
			.applogo-box img{width: 50px;height: 50px;display: inline-block;vertical-align: middle;}
			.span-logo{display: inline-block;vertical-align: middle;padding-left: 5px;}
			.span-logo span{display: block;}
			.span-logo span:nth-of-type(1){font-size: 15px;}
			.span-logo span:nth-of-type(2){font-size: 12px;}
			.btn-downapp{flex: 1;-ms-flex: 1;padding-right: 20px;}
			.btn-downapp button{height: 40px;border-style: none;float: right;background-color: #fe6561;font-size: 15px;color: #fff;border-radius: 5px;margin: 5px 0;padding: 0 15px;}
			.btn-disabled{background-color: #C0C0C0}
			.checkress-ipt-box{padding: 10px 0;border-bottom: 1px solid #f1f4f8}
			.checkress-form-box span.ssc-rh{font-size: 12px;color: #fe6561;padding: 10px 0 0 0}
			@media only screen and (max-width: 376px) {
				.checkress-ipt-box input{width: 100px;}
				.btn-yzm{width: 85px;}
			}
			#captcha-verifycode-image{width: 80px;height: 50px;margin:0;}
		</style>
	</head>
	<body>
		<div class="app-link-box">
			<div class="down-app-box" style="display: none;">
				<div class="applogo-box">
					<img  src="/static/home/images/logo.png" alt="" />
					<div class="span-logo">
						<span><?php echo htmlentities($sys['name']); ?></span>
						<span>为更好生活而来</span>
					</div>
				</div>
			</div>
			<div class="title-imgNew">
				<img  src="/static/home/images/logoNew.png" alt="" />
			</div>
			<div class="checkress-ms-box">
				<div class="checkress-form-box">
					<form id="w0" action="" method="post">					
					<span class="ssc-rh" style="display: none;">*微信打开的用户请到浏览器下载APP</span>
					<div class="checkress-ipt-box">
						<img src="/static/home/images/sjhm_icon.png"/>
						<input placeholder='请输入您的手机号' type="number" class="sjhao"/>
						<input type="hidden" name="vid" value="<?php echo htmlentities($user['vid']); ?>">
					</div>
					<div class="checkress-ipt-box yzm-k">
						<img src="/static/home/images/yzm_icon.png"></img>
						<input placeholder='请输入图形验证码' type="text" name="code" class="tuxingym"></input>
						<div class='verification'>
		                <div class="form-group field-captcha-verifycode">
							<label class="control-label" for="captcha-verifycode"></label>
							<img id="captcha-verifycode-image" src="<?php echo url('login/verify'); ?>"  onclick="this.src='<?php echo url("login/verify"); ?>?'+'id='+Math.random()"/>
						<div class="help-block"></div>
						</div>
					</div>
					</div>
					<div class="checkress-ipt-box">
						<img src="/static/home/images/yzm_icon.png"/>
						<input placeholder='请输入手机验证码' type="number" class="yzma"/>
						<button class="yzmimg btn-yzm" type="button" id="J_GetCode">验证码</button>
					</div>
					<div class="checkress-ipt-box">
						<img src="/static/home/images/sfz_icon.png"/>
						<input placeholder='请输入您的密码' type="password" class="yzma mima"/>
					</div>
						<div class="checkress-ipt-box">
							<img src="/static/home/images/rz6.png"/>
							<input placeholder='请输入您的邀请码' id="key" value="<?php echo htmlentities($user['cdkey']); ?>" type="text" class="yzma mima"/>
						</div>
					<button class="btn-checkress" type="button" id="_chaxun">立即注册</button>
					<p class="xieyi-p">注册即为同意<a href="<?php echo url('login/fuwuxieyi'); ?>">《服务协议》</a></p>
				</div>
			</div>
			
<!--			<p class="jsp">*本活动最终解释权归<?php echo htmlentities($sys['name']); ?>所有</p>-->
		</div>
		<script type="text/javascript" src="/static/home/js/jquery-2.1.4.min.js"></script>
		<script src="/static/home/js/ydui.js"></script>
		<script>
			var reg = /^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/;
			var mima = $(".mima");
			var sjhao = $(".sjhao");
			var yzma = $(".yzma");
			var $getCode = $('#J_GetCode');
			var $down = $("#down");

			var key = $("#key");
		    /* 定义参数 */
		    $getCode.sendCode({
		        disClass: 'btn-disabled',
		        secs: 60,
		        run: false,
		        runStr: '{%s}秒',
		        resetStr: '获取验证码'
		    });
		    $getCode.on('click', function () {
		    	if($('input[name=code]').val()==""){
					YDUI.dialog.toast('请先输入图形验证码！', 'none', 1000);
					return;
				}
		    	if(sjhao.val()==""||!reg.test(sjhao.val())){
		    		YDUI.dialog.toast('请输入正确的手机号码！', 'none', 1000);
					return;
		    	}
		        YDUI.dialog.loading.open('发送中...');
		        $.post("<?php echo url('login/sendCode'); ?>",{tel:sjhao.val(),yzm:$('input[name=code]').val()},function(data){
					if(data.code ==1){
						setTimeout(function() {
							$('.yzm-k').hide();
							YDUI.dialog.loading.close();
							$getCode.sendCode('start');
							YDUI.dialog.toast(data.msg, 'success', 1000);
						}, 800);
					}else if(data.code ==2){
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'error', 1000);
						}, 800);
					}else{
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'error', 1000);
						}, 800);
					}
				});
		    });
		    $("#_chaxun").on("click",function(){
		    	if(sjhao.val()==""||!reg.test(sjhao.val())){
		    		YDUI.dialog.toast('请输入正确的手机号码！', 'none', 1000);
					return;
		    	}
		    	if(yzma.val()==""){
		    		YDUI.dialog.toast('验证码不能为空！', 'none', 1000);
					return;
		    	}
		    	if(mima.val()==""){
		    		YDUI.dialog.toast('密码不能为空！', 'none', 1000);
					return;
		    	}
				if(key.val()==""){
					YDUI.dialog.toast('邀请码不能为空！', 'none', 1000);
					return;
				}
		    	if(mima.val().length<6){
		    		YDUI.dialog.toast('密码长度不能小于六位数！', 'none', 1000);
					return;
		    	}
		        YDUI.dialog.loading.open('注册中...');
		        $.post("<?php echo url('login/appregister'); ?>",{tel:sjhao.val(),yzma:yzma.val(),cdkey:key.val(),mima:mima.val(),vid:$('input[name=vid]').val()},function(data){
		        	YDUI.dialog.loading.close();
		        	if(data.code ==0){
			            YDUI.dialog.toast(data.msg, 'success', 1000);
			            setTimeout(function() {
			            	location.href = "http://tihuakeji.com/OfficialAccount";
			            }, 2000);
					}else if(data.code ==1){
			            YDUI.dialog.toast(data.msg, 'error', 1000);
					}else{
						YDUI.dialog.toast(data.msg, 'error', 1000);
					}
			    });
		    })
		</script>
	</body>
</html>
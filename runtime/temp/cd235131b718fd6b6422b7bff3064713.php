<?php /*a:3:{s:69:"/www/wwwroot/39.97.170.249/application/admin/view/category/index.html";i:1569419330;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>资讯分类列表</legend>
    </fieldset>
    <table class="layui-table" id="list" lay-filter="list"></table>
</div>

<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script type="text/html" id="barDemo">
    <a href="<?php echo url('Category/edit'); ?>?id={{d.c_id}}" class="layui-btn layui-btn-xs">编辑</a>
</script>
<script type="text/html" id="order">
    <input name="{{d.c_id}}" data-id="{{d.c_id}}" class="list_order layui-input" value=" {{d.sort}}" size="10"/>
</script>
<script>
    layui.use(['table','form'], function() {
        var table = layui.table,form = layui.form,$ = layui.jquery;
        var tableIn = table.render({
            elem: '#list',
            url: '<?php echo url("Category/index"); ?>',
            method:'post',
            cols: [[
                {field: 'c_id', title: 'ID编号', fixed: true,width:100, sort: true},
                {field:'c_title', title: '资讯分类名称',fixed: true}
                // ,{field: 'sort',align: 'center',width:150, title: '<?php echo lang("order"); ?>', templet: '#order', sort: true}
                ,{title: '操作',align:'center', toolbar: '#barDemo'}
            ]]
        });
        $('body').on('blur','.list_order',function() {
            var id = $(this).attr('data-id');
            var sort = $(this).val();
            $.post('<?php echo url("Category/order"); ?>',{c_id:id,sort:sort},function(res){
                if(res.code === 1){
                    layer.msg(res.msg, {time: 1000, icon: 1});
                    table.reload('loan');
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                }
            })
        });

    });
</script>
</body>
</html>
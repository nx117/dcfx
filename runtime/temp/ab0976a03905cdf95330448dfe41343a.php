<?php /*a:1:{s:67:"/www/wwwroot/39.97.170.249/application/home/view/data/withdraw.html";i:1571458531;}*/ ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport" />
		<meta content="yes" name="apple-mobile-web-app-capable" />
		<meta content="black" name="apple-mobile-web-app-status-bar-style" />
		<meta content="telephone=no" name="format-detection" />
		<title>提现</title>
		<link rel="stylesheet" href="/static/home/css/ydui.css">
		<link rel="stylesheet" href="/static/home/css/style.css" />
		<script src="/static/home/js/ydui.flexible.js"></script>
		<style>
			.m-button{background-color: #fff;margin-bottom: 15px}
			.btn-block{margin-top: 0;padding: 10px 0;display: flex;display: -webkit-flex;position: relative;}
			.btn-block .logo{width: 30px;height: 30px;}
			.btn-block span{line-height: 30px;color: #000;font-size: 14px;padding-left: 10px;}
			.btn-warning{height: auto;}
			.jt-logo{width: 8px;height: 18px;position: absolute;right: 0;top: 50%;transform: translateY(-50%);}
			.cell-right input[type="radio"] + .cell-radio-icon:after, .cell-right input[type="checkbox"]:not(.m-switch) + .cell-radio-icon:after{color: #4E90E6}
			.cell-item img{width: 30px;height: 30px;margin: 10px 0;margin-right: 10px;}
			.wrd-monyipt-box input{min-width: 100px;}
			#btn_wrdts{min-width: 70px;}
		</style>
	</head>

	<body>
		<div class="wrd-mony-box">
			<div class="m-button">
    			<button type="button" class="btn-block btn-warning" id="J_ShowActionSheet">
    				<img src="/static/home/images/zfblogo.png" alt="" class="logo"/>
    				<span>支付宝</span>
    				<img src="/static/home/images/jtgr_icon.png" alt="" class="jt-logo"/>
    			</button>
			</div>
			<!--提现金额，输入  -->
			<div class="wrd-monys-box">
				<span class="wrd-title-head">提现金额(元)</span>

				<!--输入区  -->
				<div class="wrd-monyipt-box">
					<span class="mony-icon">￥</span>
					<input type="number" placeholder='请输入提现金额' id="ipmeny"></input>
					<span id="btn_wrdts">全部提现</span>
				</div>

				<!--可提现金额  -->
				<span class="ys-mony-xt">可提现金额: <span id="moneys"><?php echo htmlentities($user['money']); ?></span> 元</span>
			</div>

			<button class="btn-sure-ts" type="button">确 认 提 现</button>
			<span class="tsxt">提现到支付宝账号</span>
		</div>
		<div class="m-actionsheet" id="J_ActionSheet">
<!-- 		    <label class="cell-item actionsheet-item J_Cancel">
		    	<img src="/images/wxlg_icon.png" alt="" />
			    <span class="cell-left">微信</span>
			    <label class="cell-right">
			        <input type="radio" name="radio" checked/>
			        <i class="cell-radio-icon"></i>
			    </label>
			</label> -->
		    <label class="cell-item actionsheet-item J_Cancel">
		    	<img src="/static/home/images/zfblogo.png" alt="" />
			    <span class="cell-left">支付宝</span>
			    <label class="cell-right">
			        <input type="radio" name="radio" checked/>
			        <i class="cell-radio-icon"></i>
			    </label>
			</label>
		    <a href="javascript:;" class="actionsheet-action J_Cancel">取消</a>
		</div>
		
		<script src="/static/home/js/jquery-1.12.4.min.js"></script>
		<script src="/static/home/js/ydui.js"></script>
		<script>
			$("#btn_wrdts").click(function() {
				var mys = $("#moneys").text();
				console.log(mys);
				$("#ipmeny").val(mys);
			})

			$(".btn-sure-ts").click(function() {
				var mys = $("#moneys").text();
				// location.replace('/member/aliacc');
				// return false;
				if($("#ipmeny").val() == '') {
					YDUI.dialog.toast('提现金额不能为空', 'none', function() {});
				}else if($("#ipmeny").val() < 1){
					YDUI.dialog.toast('最少提现1元', 'none', function() {});
				}else if($("#ipmeny").val()>$("#moneys").text()){
					YDUI.dialog.toast('可提现额度不足', 'none', function() {});
				}else {
		            $.post("<?php echo url('data/txsave'); ?>", {type:1,money:$("#ipmeny").val()}, function (data) {
		            	if (data.code == 1) {
							setTimeout(function() {
								YDUI.dialog.loading.close();
								YDUI.dialog.toast(data.msg, 'error', 1000);
							}, 800);

		            	}else if (data.code == 2){
							setTimeout(function() {
								YDUI.dialog.loading.close();
								YDUI.dialog.toast(data.msg, 'error', 1000);
							}, 800);

							setTimeout(function() {
								YDUI.dialog.loading.close();
								 window.location.href="http://tihuakeji.com/user/aliacc";
							}, 1800);


						}else if(data.code == 0){
							// YDUI.dialog.toast(data.msg, 'success', function() {
							// 	// location.replace("/data/index");
							// 	YDUI.dialog.toast(data.msg, 'error', 1000);
							// }, 1000);

							YDUI.dialog.loading.close();
							alert(data.msg);
							location.reload();
		            	}else{
							setTimeout(function() {
								YDUI.dialog.loading.close();
								YDUI.dialog.toast(data.msg, 'error', 1000);
							}, 800);
		            	}
		            });
				}

			})
			var $as = $('#J_ActionSheet');
		    $('#J_ShowActionSheet').on('click', function () {
		        $as.actionSheet('open');
		    });
		    $('.J_Cancel').on('click', function () {
		        $as.actionSheet('close');
		    });
		    /* 自定义事件 */
		    $as.on('open.ydui.actionsheet', function () {
		        console.log('打开了');
		    }).on('close.ydui.actionsheet', function () {
		        console.log('关闭了');
		    });
		    $("[name='radio']").click(function(){
		    	var val = $(this).parent(".cell-right").siblings(".cell-left").text()
		    	var src = $(this).parent(".cell-right").siblings("img").attr("src")
		    	$(".logo").attr("src",src);
		    	$(".btn-block span").text(val);
		    })
		</script>
	</body>
</html>
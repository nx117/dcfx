<?php /*a:2:{s:69:"/www/wwwroot/39.97.170.249/application/home/view/commodity/index.html";i:1572923422;s:71:"/www/wwwroot/39.97.170.249/application/home/view/common/new_footer.html";i:1569590650;}*/ ?>
<!doctype html>
<html>

<head>
    <title>推广</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="renderer" content="webkit">
    <link rel="stylesheet" type="text/css" href="/static/home/tb/css/public.css">
    <link rel="stylesheet" type="text/css" href="/static/home/tb/css/style.css">
    <title></title>
</head>

<body class="yemianbg">
    <div class="tab">
        <ul class="tab-hd">

            <li class="active">信用卡</li>
            <li>贷款</li>
            <li>保险</li>
            <li>更多</li>
        </ul>
        <ul class="tab-bd mb75">
            <!--		商城预留-->


            <!--		信用卡-->
            <li class="thisclass">
                <?php if(is_array($bankL) || $bankL instanceof \think\Collection || $bankL instanceof \think\Paginator): $i = 0; $__LIST__ = $bankL;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <a href="<?php echo url('loan/about',array('id'=>$v['id'])); ?>">
                    <div class="xyk_list">
                        <div class="xyk_cx fr fs12"><?php echo htmlentities($v['jiangjin']); ?></div>
                        <div class="clear"></div>
                        <div class="mt4">
                            <div class="width7 fl"><img src="<?php echo htmlentities($v['logo']); ?>" /></div>
                            <div class="fl ml4 fs14 fwb"><?php echo htmlentities($v['title']); ?></div>
                            <div class="fl ml4 xyk_js fs12"><?php echo htmlentities($v['jiesuan']); ?></div>
                            <div class="clear"></div>
                        </div>
                        <div class="mt6">
                            <div class="fl width60">
                                <div class="xyk_ys">
                                    <div class="fl ml7  xyk_ys01 mt5"><?php echo htmlentities($v['biaoqian1']); ?></div>
                                    <div class="fl ml7  xyk_ys02 mt5"><?php echo htmlentities($v['biaoqian2']); ?></div>
                                    <!--							<div class="fl ml7  xyk_ys02 mt5">额度大</div>-->
                                    <div class="clear"></div>
                                </div>
                                <div class="mt15 huise"><?php echo htmlentities($v['subscript']); ?></div>
                            </div>
                            <div class="fr width37">
                                <div class="xyk_ys">
                                    <div class="width22 fl mt5"><img src="/static/home/tb/images/xyk_11.png" /></div>
                                    <div class="fl hongse ml3"><span class="fs12">¥</span><span class="fs20 ml3"><?php echo htmlentities($v['vipmout']); ?></span></div>
                                    <!--							<div class="fl xyk_sm ml6 fs9 mt5">说明</div>-->
                                    <div class="clear"></div>
                                </div>
                                <div class="mt15 huise">普通用户 ¥ <?php echo htmlentities($v['mout']); ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </a>
                <?php endforeach; endif; else: echo "$empty" ;endif; ?>


            </li>

            <!--		贷款-->
            <li>
                <?php if(is_array($loanL) || $loanL instanceof \think\Collection || $loanL instanceof \think\Paginator): $i = 0; $__LIST__ = $loanL;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <a href="<?php echo url('loan/aboutLoan',array('id'=>$v['id'])); ?>">
                    <div class="xyk_list">
                        <div class="xyk_cx fr fs12"><?php echo htmlentities($v['jiangjin']); ?></div>
                        <div class="clear"></div>
                        <div class="mt4">
                            <div class="width7 fl"><img src="<?php echo htmlentities($v['logo']); ?>" /></div>
                            <div class="fl ml4 fs14 fwb"><?php echo htmlentities($v['title']); ?></div>
                            <div class="fl ml4 xyk_js fs12"><?php echo htmlentities($v['jiesuan']); ?></div>
                            <div class="clear"></div>
                        </div>
                        <div class="mt6">
                            <div class="fl width55">
                                <div class="dk_ys">
                                    <div class="fwb fs20"><?php echo htmlentities($v['max_money']); ?></div>
                                </div>
                                <div class="mt7 huise">贷款额度</div>
                            </div>

                            <div class="fr width45">
                                <div class="dk_ys">
                                    <div class="fr hongse ml3"><span class="fs12">返</span><span class="fs20 ml3"><?php echo htmlentities($v['vip_commisson']); ?>%</span></div>
                                    <div class="width22 fr mt5"><img src="/static/home/tb/images/xyk_11.png" /></div>
                                    <div class="clear"></div>
                                </div>
                                <div class="mt15 huise" style="float:right">普通用户返 <?php echo htmlentities($v['commisson']); ?>%</div>
                                <!--						<div class="mt7 huise tar">普通用户返<span class="black fwb"><?php echo htmlentities($v['commisson']); ?></span></div>-->
                            </div>
                            <!--					<div class="xyk_ys" style="float:left ">-->
                            <div style="margin-left: .01rem" class="fl ml7  xyk_ys01 mt5"><?php echo htmlentities($v['biaoqian1']); ?></div>
                            <div style="float:left " class="fl ml7  xyk_ys02 mt5"><?php echo htmlentities($v['biaoqian2']); ?></div>
                            <!--							<div class="fl ml7  xyk_ys02 mt5">额度大</div>-->
                            <!--						<div class="clear"></div>-->
                            <!--					</div>-->
                            <div class="clear"></div>
                        </div>
                        <div class="dk_line"></div>
                        <div class="fl width50 huise mb5">利息<?php echo htmlentities($v['annual_interest_rate']); ?></div>
                        <div class="fr width50 huise mb tar"><?php echo htmlentities($v['cptd']); ?></div>
                        <div class="clear"></div>
                    </div>
                </a>
                <?php endforeach; endif; else: echo "$empty" ;endif; ?>


            </li>

            <!--		保险-->
            <li>
                <?php if(is_array($safeL) || $safeL instanceof \think\Collection || $safeL instanceof \think\Paginator): $i = 0; $__LIST__ = $safeL;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <a href="<?php echo url('loan/aboutSafe',array('id'=>$v['id'])); ?>">
                    <div class="xyk_list">
                        <div class="xyk_cx fr fs12"><?php echo htmlentities($v['jiangjin']); ?></div>
                        <div class="clear"></div>
                        <div class="mt4">
                            <div class="width7 fl"><img src="<?php echo htmlentities($v['logo']); ?>" /></div>
                            <div class="fl ml4 fs14 fwb"><?php echo htmlentities($v['title']); ?></div>
                            <div class="fl ml4 xyk_js fs12"><?php echo htmlentities($v['jiesuan']); ?></div>
                            <div class="clear"></div>
                        </div>
                        <div class="mt6">
                            <div class="fl width60">
                                <div class="xyk_ys">
                                    <div class="fl ml7  xyk_ys01 mt5"><?php echo htmlentities($v['biaoqian1']); ?></div>
                                    <div class="fl ml7  xyk_ys02 mt5"><?php echo htmlentities($v['biaoqian2']); ?></div>
                                    <!--							<div class="fl ml7  xyk_ys02 mt5">额度大</div>-->
                                    <div class="clear"></div>
                                </div>
                                <div class="mt15 huise"><?php echo htmlentities($v['subscript']); ?></div>
                            </div>
                            <div class="fr width37">
                                <div class="xyk_ys">
                                    <div class="width22 fl mt5"><img src="/static/home/tb/images/xyk_11.png" /></div>
                                    <div class="fl hongse ml3"><span class="fs12"></span><span class="fs20 ml3"><?php echo htmlentities($v['vipmout']); ?>%</span></div>
                                    <!--							<div class="fl xyk_sm ml6 fs9 mt5">说明</div>-->
                                    <div class="clear"></div>
                                </div>
                                <div class="mt15 huise">普通用户 <?php echo htmlentities($v['mout']); ?>%</div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </a>
                <?php endforeach; endif; else: echo "$empty" ;endif; ?>
            </li>

            <li>

                <?php if(is_array($reL) || $reL instanceof \think\Collection || $reL instanceof \think\Paginator): $i = 0; $__LIST__ = $reL;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <a href="<?php echo url('loan/about',array('id'=>$v['id'])); ?>">
                    <div class="xyk_list">
                        <div class="xyk_cx fr fs12"><?php echo htmlentities($v['jiangjin']); ?></div>
                        <div class="clear"></div>
                        <div class="mt4">
                            <div class="width7 fl"><img src="<?php echo htmlentities($v['logo']); ?>" /></div>
                            <div class="fl ml4 fs14 fwb"><?php echo htmlentities($v['title']); ?></div>
                            <div class="fl ml4 xyk_js fs12"><?php echo htmlentities($v['jiesuan']); ?></div>
                            <div class="clear"></div>
                        </div>
                        <div class="mt6">
                            <div class="fl width60">
                                <div class="xyk_ys">
                                    <div class="fl ml7  xyk_ys01 mt5"><?php echo htmlentities($v['biaoqian1']); ?></div>
                                    <div class="fl ml7  xyk_ys02 mt5"><?php echo htmlentities($v['biaoqian2']); ?></div>
                                    <!--							<div class="fl ml7  xyk_ys02 mt5">额度大</div>-->
                                    <div class="clear"></div>
                                </div>
                                <div class="mt15 huise"><?php echo htmlentities($v['subscript']); ?></div>
                            </div>
                            <div class="fr width37">
                                <div class="xyk_ys">
                                    <div class="width22 fl mt5"><img src="/static/home/tb/images/xyk_11.png" /></div>
                                    <div class="fl hongse ml3"><span class="fs12">¥</span><span class="fs20 ml3"><?php echo htmlentities($v['vipmout']); ?></span></div>
                                    <!--							<div class="fl xyk_sm ml6 fs9 mt5">说明</div>-->
                                    <div class="clear"></div>
                                </div>
                                <div class="mt15 huise">普通用户 ¥ <?php echo htmlentities($v['mout']); ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </a>
                <?php endforeach; endif; else: echo "$empty" ;endif; ?>
            </li>



        </ul>
    </div>
    <script src="/static/home/tb/js/jquery.min.js"></script>
    <script>
        $(function() {
            function tabs(tabTit, on, tabCon) {
                $(tabTit).children().hover(function() {
                    $(this).addClass(on).siblings().removeClass(on);
                    var index = $(tabTit).children().index(this);
                    $(tabCon).children().eq(index).show().siblings().hide();
                });
            };
            tabs(".tab-hd", "active", ".tab-bd");
        });
    </script>







     <!--<div class="footer-box">
	<ul>
		<li>
			<a href="/">
				<img src="/static/home/images/WX20181029-110205@2x_meitu_3.png" alt="" />
				<span>鑫云</span>
			</a>
		</li>
		<li>
			<a href="<?php echo url('user/level'); ?>">
				<img src="/static/home/images/vip.png" alt="" />
				<span>代理</span>
			</a>
		</li>
		<li>
			<a href="/data/index">
				<img src="/static/home/images/data_icon.png" alt="" />
				<span>钱包</span>
			</a>
		</li>
		<li>
			<a href="/article/index">
				<div class="bq">
					<img src="/static/home/images/yj_icon.png" alt="" />
				</div>
				<span>消息</span>
			</a>
		</li>
		<li>
			<a href="/user/index">
				<img src="/static/home/images/grzx_icon.png" alt="" />
				<span>个人中心</span>
			</a>
		</li>
	</ul>


</div>-->

 <link rel="stylesheet" type="text/css" href="/static/home/tb/css/public.css">
 <link rel="stylesheet" type="text/css" href="/static/home/tb/css/style.css">

 <!--bottom-->
 <div class="botkong"></div>
 <div class="bot">
	 <li class="li02">
		 <a href="/">
			 <div class="tac fs12 pt30">推广</div>
		 </a>
	 </li>
	 <li class="li01">
		 <a href="/commodity">
			 <div class="tac fs12 pt30">资讯</div>
		 </a>
	 </li>
	 <li class="li03">
		 <a href="/user/order">
			 <div class="tac fs12 pt30">订单</div>
		 </a>
	 </li>
	 <li class="li04">
		 <a href="/user/index">
			 <div class="tac fs12 pt30">我的</div>
		 </a>
	 </li>
	 <div class="clear"></div>
 </div>




















</body>

</html>
<?php /*a:3:{s:66:"/www/wwwroot/39.97.170.249/application/admin/view/article/add.html";i:1569419344;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
<div class="admin-main layui-anim layui-anim-upbit" ng-app="hd" ng-controller="ctrl">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>资讯信息添加</legend>
    </fieldset>
     <form class="layui-form">
          <div class="layui-form-item">
              <label for="title" class="layui-form-label">
                  <span class="x-red">*</span>标题
              </label>
              <div class="layui-input-block">
                  <input type="text" id="title" name="title" required="" lay-verify="required"
                  autocomplete="off" class="layui-input"  style="width:1024px">
              </div>
          </div>
          <div class="layui-form-item">
              <label class="layui-form-label"><span class="x-red">*</span>分类</label>
              <div class="layui-input-inline">
                <select name="a_id" lay-filter="aihao">
			        <option value="">--请选择--</option>
			        <?php foreach($cate as $v): ?>
			        <option value="<?php echo htmlentities($v['c_id']); ?>"><?php echo htmlentities($v['c_title']); ?></option>
			        <?php endforeach; ?>
		      	</select>
              </div>
          </div>

		<div class="layui-form-item">
            <label class="layui-form-label">封面</label>
            <input type="hidden" name="img" id="image">
            <div class="layui-input-block">
                <div class="layui-upload">
                    <button type="button" class="layui-btn layui-btn-primary" id="cateBtn"><i class="icon icon-upload3"></i>点击上传</button>
                    <div class="layui-upload-list">
                        <img class="layui-upload-img" id="cateImage">
                        <p id="demoText"></p>
                    </div>
                </div>
            </div>
        </div>
        
<!--          <div class="layui-form-item">-->
<!--              <label for="sort" class="layui-form-label">-->
<!--                  <span class="x-red">*</span>排序-->
<!--              </label>-->
<!--              <div class="layui-input-inline">-->
<!--                  <input type="number" id="L_repass" name="order" required="" lay-verify="sort"-->
<!--                  autocomplete="off" class="layui-input">-->
<!--              </div>-->
<!--              <div class="layui-form-mid layui-word-aux">填数字,数字越小越靠前</div>-->
<!--          </div>-->

          <div class="layui-form-item">
              <label for="auth" class="layui-form-label">
                  <span class="x-red">*</span>权限
              </label>
              <div class="layui-input-inline">
                  <input type="number" id="L_repass" name="auth" required="" lay-verify="auth"
                  autocomplete="off" class="layui-input">
              </div>
              <div class="layui-form-mid layui-word-aux">0为普通会员权限，所填数字跟用户组别的权重相同</div>
          </div>
          <div class="layui-form-item">
              <label class="layui-form-label"><span class="x-red">*</span>是否推荐</label>
              <div class="layui-input-inline">
                <input type="radio" name="is_tui" value="0" title="不推荐">
                <input type="radio" name="is_tui" value="1" title="推荐">
              </div>
              <div class="layui-form-mid layui-word-aux">系统公告可以不推荐</div>
          </div>

          <div class="layui-form-item">
              <label for="auth" class="layui-form-label">
                  <span class="x-red">*</span>点击次数
              </label>
              <div class="layui-input-inline">
                  <input type="number" id="L_repass" name="hits" class="layui-input">
              </div>
          </div>

        <div class="layui-form-item">
            <label class="layui-form-label">内容</label>
            <input type="hidden" name="content" id="image">
            <div class="layui-input-block">
                <script name="content" id="editor" type="text/plain" style="width:1024px;height:500px;"></script>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="button" class="layui-btn" lay-submit="" lay-filter="submit"><?php echo lang('submit'); ?></button>
                <a href="<?php echo url('index'); ?>" class="layui-btn layui-btn-primary"><?php echo lang('back'); ?></a>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script src="/static/common/js/angular.min.js"></script>
<script>
    var ue = UE.getEditor('editor');
    layui.use(['form','layer','upload'], function(){
        $ = layui.jquery;var form = layui.form,upload = layui.upload,layer = layui.layer;
			var uploadInst = upload.render({
	            elem: '#cateBtn',
	            url: '<?php echo url("UpFiles/upload"); ?>',
	            done: function(res){
	                if(res.code>0){
	                    $('#image').val(res.url);
	                    $('#cateImage').attr('src', res.url);
	                }else{
	                    //如果上传失败
	                    return layer.msg('上传失败');
	                }
	            },
	            error: function(){
	                //演示失败状态，并实现重传
	                var demoText = $('#demoText');
	                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
	                demoText.find('.demo-reload').on('click', function(){
	                    uploadInst.upload();
	                });
	            }
			 });

          //监听提交
	         form.on('submit(submit)', function (data) {
	            loading =layer.load(1, {shade: [0.1,'#fff']});
	            $.post("<?php echo url('article/articleAdd'); ?>", data.field, function (res) {
	                layer.close(loading);
	                if (res.code > 0) {
	                    layer.msg(res.msg, {time: 1800, icon: 1}, function () {
	                        location.href = res.url;
	                    });
	                } else {
	                    layer.msg(res.msg, {time: 1800, icon: 2});
	                }
	            });
	        });

        });
</script>
<?php /*a:1:{s:65:"/www/wwwroot/39.97.170.249/application/home/view/user/myinfo.html";i:1570545458;}*/ ?>

<!DOCTYPE html>
<html style="height: 100%;">

	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="/static/home/css/ydui.css" />
		<link rel="stylesheet" href="/static/home/css/style.css" />
		<meta id="viewport" name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1,user-scalable=no">
		<title>个人信息</title>
		<style type="text/css">
			.user-photo{
				position: absolute;
    			left: 0;
    			right: 0;
    			top: -18px;
    			opacity: 0;
    			z-index: 999;
    			width: 100%;
    			height: 93px;
			}
		</style>
	</head>
	<body style="background-color: #F1F4F8">
		<div class="editprofile-box">
			<form id="w0" action="/user/myinfo" method="post">			
			<div class="edit-ple-box">
				<div class="edit-list-li">
					<input type="file" name="shop_avatar" class="user-photo" id="filed"/>
					<span class="wx-tl-xt">头像</span>
					<img src="<?php echo htmlentities($user['headpic']); ?>" class="wx-headig" id="imgshow"></img>
				</div>
				<a href="/user/updatename">
				<div class="edit-list-li">
					<span>昵称</span>
					<span class="cl-rg" selectable='true'><?php echo htmlentities($user['nickname']); ?></span>
					<img src="/static/home/images/jtgr_icon.png" class="wx-jt"></img>
				</div>
				</a>
<!--				<a href="/user/updatewxname">-->
<!--				<div class="edit-list-li">-->
<!--					<span>完善资料</span>-->
<!--					<span class="cl-rg" selectable='true'></span>-->
<!--					<img src="/static/home/images/jtgr_icon.png" class="wx-jt"></img>-->
<!--				</div>-->
				</a>
				<div class="edit-list-li">
					<span>手机号码</span>
					<span class="cl-rg dqhm"><?php echo htmlentities($user['tel']); ?></span>
					<img src="/static/home/images/jtgr_icon.png" class="wx-jt"></img>
				</div>
			</div>
			<div class="edit-ple-box">
				<a href="/user/aliacc">
					<div class="edit-list-li" style="margin: 15px 0;">
						<span>提现账号</span>
						<img src="/static/home/images/jtgr_icon.png" class="wx-jt"></img>
					</div>
				</a>
			</div>
			<div class="edit-ple-box">
				<a href="<?php echo url('user/uppwd'); ?>">
					<div class="edit-list-li" style="margin: 15px 0;">
						<span>修改密码</span>
						<img src="/static/home/images/jtgr_icon.png" class="wx-jt"></img>
					</div>
				</a>
			</div>
			<div class="edit-ple-box">
				<div class="edit-list-li">
					<span>ID号</span>
					<span class="cl-rg"><?php echo htmlentities($user['vid']); ?></span>
				</div>
				<a href="/user/vipcpl">
					<div class="edit-list-li">
						<span>我的职务</span>
						<span class="cl-rg"><?php echo htmlentities($user['title']); ?></span>
						<img src="/static/home/images/jtgr_icon.png" class="wx-jt"></img>
					</div>
				</a>

				<div class="edit-list-li">
					<span>我的邀请码</span>
					<span class="cl-rg"><?php echo htmlentities($user['cdkey']); ?></span>
					<img src="/static/home/images/jtgr_icon.png" class="wx-jt"></img>
				</div>
				<a href="javascript:;">
					<div class="edit-list-li">
						<span>推荐人</span>
						<span class="cl-rg"><?php if(($user['lev1'] >0)): ?><a style="float:right" href="/user/id/<?php echo htmlentities($user['lev1']); ?>"><?php echo htmlentities($user['lev1_name']); ?></a><?php else: ?>无<?php endif; ?></span>
						<!-- <span class="cl-rg"><?php if(($user['lev1'] >0)): ?><?php echo htmlentities($user['lev1_name']); else: ?>无<?php endif; ?></span> -->
						<img src="/static/home/images/jtgr_icon.png" class="wx-jt"></img>
					</div>
				</a>
				<div class="edit-list-li">
					<span>注册时间</span>
					<span class="cl-rg dqhm"><?php echo htmlentities(date(('Y-m-d H:i:s'),!is_numeric($user['reg_time'])? strtotime($user['reg_time']) : $user['reg_time'])); ?></span>
				</div>
			</div>
			<a href="/logout"><button type="button" class="btn-form" id="btn_gm" style="margin-top: 20px;">退 出</button></a>
			<!--修改手机号码弹窗-->
			<div class="model-mask" onmousewheel="false;">
			</div>
			<div class="model">
				<div class="model-top-box">修改手机号码</div>
				<div class="model-phone-box">
					<input type="number" placeholder="请输入您的新手机号码" class="melph" />
					<button type="button" class="btn btn-warning" id="J_GetCode" style="font-size: 13px;">获取验证码</button>
				</div>
				<div class="model-phone-box yzm-k" style="margin: 0 10px 10px 10px;">
					<input type="text" name="code" placeholder="请输入您的图形验证码" class="tuxinym" />
					<div class='verification'>
		                <div class="form-group field-captcha-verifycode">
							<label class="control-label" for="captcha-verifycode"></label>
							<img id="captcha-verifycode-image" src="<?php echo url('login/verify'); ?>"/>
						<div class="help-block"></div>
						</div>
					</div>
				</div>
				<input type="number" placeholder="请输入验证码" class="melym" />
				<div class="model-btn-box">
					<button class="remove">取消</button>
					<button class="submin" type="button">提交</button>
				</div>
			</div>
		</div>
		<script src="/static/home/js/jquery-1.12.4.min.js"></script>
		<script src="/static/home/js/ydui.js"></script>
		<script>
			$('#filed').change(function() {
				var file = $('#filed').get(0).files[0];
				var reader = new FileReader();
				reader.readAsDataURL(file);
				reader.onload = function(e) {
					//读取成功后返回的一个参数e，整个的一个进度事件
					if(file.type == "image/png" || file.type == "image/jpg" || file.type == "image/bmp" || file.type == "image/jpeg"){
						//选择所要显示图片的img，要赋值给img的src就是e中target下result里面
						//的base64编码格式的地址
						// $("input[name='shop_avatar']").val(e.target.result);
						$('#imgshow').get(0).src = e.target.result;
					}else{
						YDUI.dialog.toast('请选择正确的图片格式！', 'none', 1000);
						return;
					}
					var size = 1048576*5;
					if(file.size > size){
						YDUI.dialog.toast('请选择图片大小为5M以内！', 'none', 1000);
						return;
					}
					var user_avatar = $("#filed").val();
					YDUI.dialog.loading.open('头像更新中...');
					if(user_avatar != ''){
						$.post("<?php echo url('updateavatar'); ?>",{user_avatar: $("#imgshow").attr('src'),},function(data){
							YDUI.dialog.loading.close();
							if(data.code ==0){
								YDUI.dialog.toast('更新成功', 'success', 1000, function() { /* 关闭后调用 */
									location.reload();
								});
							}else{
								YDUI.dialog.toast(data.msg, 'error', 1000);
							}
						});
					}
				}
			})
			$("#captcha-verifycode-image").click(function(){
		        var img = $(this);
		        $.get('/home/login/verify?refresh=1',function(data){
		               img.attr('src',data.url);
		        },'json');
		    });

			var $getCode = $('#J_GetCode');
			var regedg = /^1[0-9]{10}$/;
			//存储当前的手机号码
			var dqhm = $(".dqhm").text();
			/* 定义参数 */
			$getCode.sendCode({
				disClass: 'btn-disabled',
				secs: 60,
				run: false,
				runStr: '{%s}秒后重新获取',
				resetStr: '重新获取验证码'
			});
			$getCode.on('click', function() {
				console.log("sd");
				if($('input[name=code]').val()==""){
					YDUI.dialog.toast('请先输入图形验证码！', 'none', 1000);
					return;
				}
				/* ajax 成功发送验证码后调用【start】 */
				let phoneip = $(".melph").val();
				if(!regedg.test(phoneip)) {
					YDUI.dialog.toast('请输入正确的手机号码！', 'none', 1000)
					return;
				} else {
					YDUI.dialog.loading.open('发送中...');
					$.post("<?php echo url('login/sendCode'); ?>",{tel:phoneip,yzm:$('input[name=code]').val()},function(data){
						if(data.code ==1){
							setTimeout(function() {
								$('.yzm-k').hide();
								YDUI.dialog.loading.close();
								$getCode.sendCode('start');
								YDUI.dialog.toast(data.msg, 'success', 1000);
							}, 800);
						}else if(data.code ==2){
							setTimeout(function() {
								YDUI.dialog.loading.close();
								YDUI.dialog.toast(data.msg, 'error', 1000);
							}, 800);
						}else{
							setTimeout(function() {
								YDUI.dialog.loading.close();
								YDUI.dialog.toast(data.msg, 'error', 1000);
							}, 800);
						}
					});
				}
			});
			function showUpdateNum() {
				$(".model-mask").show();
				$(".model").animate({top:"50%"},"show")
			}
			function hideUpdateNum() {
				$(".model-mask").fadeOut()
				$(".model").animate({top:"-50%"},"show")
			}
			$(".remove").click(function() {
				$(this).bind(hideUpdateNum());
			})
			$(".submin").click(function() {
				let phoneip = $(".melph").val();
				let ymip = $(".melym").val();
				if(phoneip == '') {
					YDUI.dialog.toast('手机号不能为空！', 'none', 1000)
					return;
				}
				if(ymip == '') {
					YDUI.dialog.toast('验证码不能为空！', 'none', 1000)
					return;
				}
				if(!regedg.test(phoneip)) {
					YDUI.dialog.toast('您输入的手机号码有误！', 'none', 1000)
					return;
				}
				YDUI.dialog.loading.open('正在修改...');
				$.post("<?php echo url('user/editphone'); ?>",{tel:phoneip,yzm:ymip},function(data){
					if(data.code ==0){
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'success', 1000 ,function(){
							location.replace("/user/myinfo");
							});
						}, 600);
					}else if(data.code ==2){
							setTimeout(function() {
								YDUI.dialog.loading.close();
								YDUI.dialog.toast(data.msg, 'error', 1000);
							}, 800);
					}else{
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'error', 400 ,function(){});
						}, 800);
					}
				})
			})
		</script>
	</body>
</html>
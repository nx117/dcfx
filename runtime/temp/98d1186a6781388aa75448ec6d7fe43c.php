<?php /*a:2:{s:64:"/www/wwwroot/39.97.170.249/application/home/view/user/index.html";i:1569590636;s:67:"/www/wwwroot/39.97.170.249/application/home/view/common/footer.html";i:1569590651;}*/ ?>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/static/home/css/czym_dbk.css?e">
    <link rel="stylesheet" href="/static/home/css/newhome.css?a">
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1; user-scalable=no">

    <title>个人中心</title>
</head>

<body style="background-color: #F1F4F8">
<div class="grzx-seventbox personalct-box">
    <div class="sventbox-header">
                <span class="person_s">
					<a href="/user/myinfo" class="set-sevent">设置</a>
                    <!--<a href="/index/systemnotice" class="news-sevent">消息</a>-->
				</span>
<!--        <h4 class="person_t">个人中心</h4>-->
        <div class="usersevent-mess">
            <img src="<?php echo htmlentities($user['headpic']); ?>" class="head" alt="">
            <div class="seventname-box">
                <strong><?php echo htmlentities($user['nickname']); ?></strong>
                <span><?php echo htmlentities($user['title']); ?></span>
            </div>
            <em class="idsevent">ID:<?php echo htmlentities($user['vid']); ?></em>
            <div class="jscjhy">
                <a href="<?php echo url('user/level'); ?>"></a>
                <img src="/static/home/images/huiy_icon.png" alt="">
            </div>
        </div>
        <div class="seventye-box">
            <div class="dvye">
                <div class="yue-dvye-box">
                    <span>我的余额(元)</span>
                    <strong><?php echo htmlentities($user['money']); ?></strong>
                </div>
                <a href="/data/withdraw" class="ts-dvye">提现</a>
            </div>
            <div class="dvye">
                <ul class="dvye-ul">
                    <li>
                        <strong><?php echo htmlentities($user['dayprice']); ?></strong>
                        <em>今日收益(元)</em>
                    </li>
                    <li>
                        <strong><?php echo htmlentities($user['myteam']); ?></strong>
                        <em>团队收益(元)</em>
                    </li>
                    <li>
                        <strong class="isst"><?php echo htmlentities($user['myteam']); ?></strong>
                        <em>累计收入(元)</em>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="seventlist-box">
        <div class="listgn-seventbox">
            <ul>
                <li>
                    <a href="/user/createpro">
                        <img src="/static/home/images/packet_icon.png" alt="">
                        <span>推广</span>
                    </a>
                </li>
                <li>
                    <a href="/home/data/yaoqing">
                        <img src="/static/home/images/dindse_icon.png" alt="">
                        <span>收入</span>
                    </a>
                </li>

                <li>
                    <a href="/data/myteam/1">
                        <img src="/static/home/images/time_icon.png" alt="">
                        <span>团队</span>
                    </a>
                </li>
                <li>
                    <a href="#" onclick="attentionEwm()">
                        <img src="/static/home/images/kefuse_icon.png" alt="">
                        <span>客服</span>
                    </a>
                </li>
                <!--<li>
                    <a href="/member/helpcenter">
                        <img src="images/helpse_icons.png" alt="">
                        <span>问题</span>
                    </a>
                </li>-->
<!--                <li>-->
<!--                    <a href="<?php echo htmlentities($user['appurl']); ?>">-->
<!--                        <img src="/static/home/images/appdown.png" alt="">-->
<!--                        <span>App下载</span>-->
<!--                    </a>-->
<!--                </li>-->
            </ul>
        </div>
    </div>
    <div class="houm-seventbox">
        <h3>新手指南</h3>
        <div class="banner-sevent">
            <a href="">
                <img src="/static/home/images/xinshoubg-bg.png" alt="">
            </a>
        </div>
        <h3>推广海报</h3>
        <div class="tghb-seventbox">
            <ul>
                <li>
                    <a href="/user/createpro">
                        <img src="/static/home/images/money_icon.png" alt="">
                        <div class="tghb-listmess">
                            <span>招募合伙人</span>
                            <em>邀请好友，马上有钱</em>
                        </div>
                    </a>
                </li>
                <!--<li>
                    <a href="/shop/myposter?type=loan">
                        <img src="/static/home/images/phonse_icons.png" alt="">
                        <div class="tghb-listmess">
                            <span>贷款推广</span>
                            <em>贷款额度高，审批流程快</em>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="/shop/myposter?type=bank">
                        <img src="/static/home/images/xinykse_icons.png" alt="">
                        <div class="tghb-listmess">
                            <span>信用卡推广</span>
                            <em>推广信用卡，单卡返现超百元</em>
                        </div>
                    </a>
                </li>-->
            </ul>
        </div>
    </div>
     <!--<div class="footer-box">
	<ul>
		<li>
			<a href="/">
				<img src="/static/home/images/WX20181029-110205@2x_meitu_3.png" alt="" />
				<span>鑫云</span>
			</a>
		</li>
		<li>
			<a href="<?php echo url('user/level'); ?>">
				<img src="/static/home/images/vip.png" alt="" />
				<span>代理</span>
			</a>
		</li>
		<li>
			<a href="/data/index">
				<img src="/static/home/images/data_icon.png" alt="" />
				<span>钱包</span>
			</a>
		</li>
		<li>
			<a href="/article/index">
				<div class="bq">
					<img src="/static/home/images/yj_icon.png" alt="" />
				</div>
				<span>消息</span>
			</a>
		</li>
		<li>
			<a href="/user/index">
				<img src="/static/home/images/grzx_icon.png" alt="" />
				<span>个人中心</span>
			</a>
		</li>
	</ul>
</div>-->
 <div class="footer-box">
	 <ul>

		 <li>
			 <a href="/">
				 <img src="/static/home/images/jk.png" alt="">
				 <span>贷款</span>
			 </a>
		 </li>
		 <li>
			 <a href="/home/loan/index">
				 <img src="/static/home/images/loans_icon_n.png" alt="">
				 <span>信用卡</span>
			 </a>
		 </li>
		 <li>
			 <a href="/home/loan/bath">
				 <div class="bq">
					 <img src="/static/home/images/home_icon.png" alt="">
					 <span>首页</span>
				 </div>

			 </a>
		 </li>
		 <li>

			 <a href="/home/loan/lou">
				 <div class="bq">
					 <img src="/static/home/images/discover_n_icon.png" alt="">

				 </div>
				 <span>保险</span>
			 </a>
		 </li>

		 <li>
			 <a href="/user/index">
				 <img src="/static/home/images/grzx_icon.png" alt="">
				 <span >我的</span>
			 </a>
		 </li>

	 </ul>
 </div>
    <div class="gon-box" onclick="attentionEwm()">
        <img src="/static/home/images/gzh_icon.png" alt="关注公众号">
    </div>
    <!--通用遮罩-->
    <div class="gzh-modelmask-box" style="display: none;"></div>
    <!--关注公众号二维码弹窗-->
    <div class="gzh-model-box">
        <div class="head-model-box">
            <img src="/static/home/images/sliderss_icon.png" alt="">
            <img src="/static/home/images/clear.png" alt="" class="close-btn">
        </div>
        <div class="ewm-model-box">
            <img src="http://kashenimg.oss-cn-shenzhen.aliyuncs.com/merchants/2018/04/15248233769844.jpg" alt="">

        </div>
        <div class="content-mess-box">
            <p>如何添加客服</p>
            <div class="luce-box">
                <span>长按保存二维码</span>&gt;<span>微信识别二维码</span>&gt;<span>添加好友</span>
            </div>
            <span>客服微信:dai1633</span>
        </div>
    </div>
</div>
<script src="/static/home/js/jquery-1.12.4.min.js"></script>
<script src="/static/home/js/czym_dbk.js"></script>


</body>
</html>
<?php /*a:3:{s:70:"/www/wwwroot/39.97.170.249/application/admin/view/order/bankindex.html";i:1569419323;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend><?php echo htmlentities($title); ?></legend>
    </fieldset>
    <div class="demoTable">
        <div class="layui-inline">
            <input class="layui-input" name="key" id="key" placeholder="<?php echo lang('pleaseEnter'); ?>关键字">
        </div>
        <button class="layui-btn" id="search" data-type="reload">搜索</button>
        <button type="button" class="layui-btn layui-btn-danger" id="bohuiAll">批量驳回</button>
    </div>
    <table class="layui-table" id="list" lay-filter="list"></table>
</div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script type="text/html" id="status">
 {{# if(d.status==1){ }}
    <button class="layui-btn layui-btn-xs">待审核</button>
 {{# }else if(d.status==2){  }}
    <button class="layui-btn layui-btn-xs layui-btn-danger">已驳回</button>
    {{# }else{  }}
    <button class="layui-btn layui-btn-xs layui-btn-warm">通过</button>
    {{# } }}
</script>
<script type="text/html" id="action">

{{# if(d.status==0){ }}
    已通过
{{# }else if(d.status==2){  }}
    已驳回
{{# }else{  }}
<a class="layui-btn layui-btn-sm" lay-event="tongguo"><i class="layui-icon">&#xe605;</i>通过</a>
<a class="layui-btn layui-btn-sm layui-btn-danger"  lay-event="bohui"><i class="layui-icon">&#x1006;</i>驳回</a>
{{# } }}
</script>
<script type="text/html" id="cha">
<a class="layui-btn layui-btn-sm layui-btn-danger"  lay-event="cha"><i class="layui-icon">&#xe615;</i>查看</a>
</script>
<script>
   layui.use(['table','form'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery;
       url = '<?php echo url("Order/bank"); ?>';

       var type = '<?php echo htmlentities($type); ?>';
       if (type==1){
           url  = '<?php echo url("Order/recommend"); ?>'
       }
       var tableIn = table.render({
            id: 'bank',
            elem: '#list',
            url: url,
            method: 'post',
            page: true,
            cols: [[
                {checkbox:true,fixed: true},
                {field: 'fan_id1', title: '推广者', width: 80},
                {field: 'sn', title: '订单号', width: 80},
                {field: 'name', title: '姓名', width: 80},
                {field: 'tel', title: '手机号', width: 150},
                {field: 'card', title: '身份证号', width: 150},
                {field: 'title', title: '商品名称', width: 150},
                {field: 'status', title: '状态', width: 150,toolbar: '#status'},
                // {field: 'per', title: '返佣', width: 80},
                {field: 'ctime', title: '申请时间', width: 150},
                {title: '查看返佣',width: 200, align: 'center', toolbar: '#cha'},
                {title: '操作',width: 200, align: 'center', toolbar: '#action'}
            ]],
            limit: 10 //每页默认显示的数量
        });
        //搜索
        $('#search').on('click', function() {
            var key = $('#key').val();
            if($.trim(key)==='') {
                layer.msg('<?php echo lang("pleaseEnter"); ?>关键字！',{icon:0});
                return;
            }
            tableIn.reload({ page: {page: 1},where: {key: key}});
        });
        //数据操作
     table.on('tool(list)', function(obj){
        var data = obj.data;
        if(obj.event === 'tongguo'){
                layer.confirm('您确定要通过这条数据吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo url('data/banktg'); ?>",{id:data.id},function(res){
                        layer.close(loading);
                        if(res.code===1){
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            tableIn.reload();
                        });
                        }else{
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            tableIn.reload();
                        });
                        }
                    });
                    layer.close(index);
                });
        } else if(obj.event === 'bohui'){
                layer.confirm('您确定要驳回这条数据吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo url('data/bohui'); ?>",{id:data.id},function(res){
                        layer.close(loading);
                        if(res.code===1){
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            tableIn.reload();
                        });
                        }else{
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            tableIn.reload();
                        });
                        }
                    });
                    layer.close(index);
                });
        }else if(obj.event === 'cha'){
            var id = data.id;
                layer.open({
                    type: 2,
                    title: '返佣详情',
                    shadeClose: true,
                    shade: false,
                    maxmin: true, //开启最大化最小化按钮
                    area: ['893px', '200px'],
                    content: '/order/fanyong/'+id,
                });
        }
    });
        $('#bohuiAll').click(function(){
            layer.confirm('确认要批量驳回吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('bank'); //test即为参数id设定的值
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo url('data/bohuiAll'); ?>", {ids: ids}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        })
});
</script>
</body>
</html>
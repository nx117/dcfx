<?php /*a:1:{s:69:"/www/wwwroot/39.97.170.249/application/home/view/login/forgetpwd.html";i:1569590666;}*/ ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="/static/home/css/ydui.css">
		<link rel="stylesheet" href="/static/home/css/style.css" />
		<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1; user-scalable=no">
		<title>修改密码</title>
		<script src="/static/home/js/ydui.flexible.js"></script>
	</head>
	<body style="background-color: #F1F4F8">
		<!--pages/register/register.wxml-->
		<div class="form-cz-box">
			<form id="w0" action="/member/forgetpwd" method="post">			<span class="head-tl">请输入您的个人信息</span>
			<div class="form-write-box">
				<div class="ipt-box-cl">
					<span>手机号</span>
					<input class="phonenum" type="number" placeholder='请输入您的手机号码'></input>
					<span class="send-yzm-btn" id="J_GetCode">发送验证码</span>
				</div>
				<div class="ipt-box-cl yzm-k">
					<span>图形验证码</span>
					<input class="tuxingym" type="text" name="code" placeholder='请输入您的图形验证码'></input>
					<div class='verification'>
		                <div class="form-group field-captcha-verifycode">
<label class="control-label" for="captcha-verifycode"></label>
<img id="captcha-verifycode-image" src="<?php echo url('login/verify'); ?>"  onclick="this.src='<?php echo url("verify"); ?>?'+'id='+Math.random()"/>
<div class="help-block"></div>
</div>		            </div>
				</div>
				<div class="ipt-box-cl">
					<span>手机验证码</span>
					<input class="phoneem" type="text" placeholder='请输入手机验证码'></input>
				</div>
				<button class="btn-form" type="button">下 一 步</button>
			</div>
		</div>
		<script src="/static/home/js/jquery-2.1.4.min.js"></script>
		<script src="/static/home/js/ydui.js"></script>
		<script>
			var $getCode = $('#J_GetCode');
			var $phoneNum = $(".phonenum");
			var $phoneEm = $(".phoneem");
			var regedg = /^1[0-9]{10}$/;
			/* 定义参数 */
			$getCode.sendCode({
				disClass: 'btn-disabled',
				secs: 60,
				run: false,
				runStr: '{%s}s重新获取',
				resetStr: '重新获取'
			});
			$getCode.on('click', function() {
				/* ajax 成功发送验证码后调用【start】 */
				if($('input[name=code]').val()==""){
					YDUI.dialog.toast('请先输入图形验证码！', 'none', 1000);
					return;
				}
				if($phoneNum.val()==""){
					YDUI.dialog.toast('请输入您的手机号码！', 'none', 1000);
					return;
				} 
				if(!regedg.test($phoneNum.val())){
					YDUI.dialog.toast('请输入正确的手机号码！', 'none', 1000);
					return;
				}
				YDUI.dialog.loading.open('发送中...');
				$.post("<?php echo url('login/sendCode'); ?>",{tel:$phoneNum.val(),yzm:$('input[name=code]').val()},function(data){
					if(data.code ==1){
						setTimeout(function() {
							$('.yzm-k').hide();
							YDUI.dialog.loading.close();
							$getCode.sendCode('start');
							YDUI.dialog.toast(data.msg, 'success', 1000);
						}, 800);
					}else if(data.code ==2){
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'error', 1000);
						}, 800);
					}else{
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'error', 1000);
						}, 800);
					}
				});
			});
			$(".btn-form").click(function(){
				if($phoneNum.val()==""){
					YDUI.dialog.toast('请输入您的手机号码！', 'none', 1000);
					return;
				} 
				if(!regedg.test($phoneNum.val())){
					YDUI.dialog.toast('请输入正确的手机号码！', 'none', 1000);
					return;
				}
				if($phoneEm.val()==""){
					YDUI.dialog.toast('请输入验证码！', 'none', 1000);
					return;
				}
				YDUI.dialog.loading.open('审核中...');
				$.post("<?php echo url('login/dopwd'); ?>",{tel:$phoneNum.val(),yzm:$phoneEm.val()},function(data){
					if(data.code==0){
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'success', 400 ,function(){
								location.replace(data.url);
							});
						}, 800);
					}else{
						setTimeout(function() {
							YDUI.dialog.loading.close();
							YDUI.dialog.toast(data.msg, 'error', 400 ,function(){});
						}, 800);
					}
				});
			})
		</script>
	</body>
</html>
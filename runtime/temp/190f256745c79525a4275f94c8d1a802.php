<?php /*a:3:{s:70:"/www/wwwroot/39.97.170.249/application/admin/view/order/cashindex.html";i:1569419323;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>提现订单列表</legend>
    </fieldset>
    <div class="demoTable">
        <div class="layui-inline">
            <input class="layui-input" name="key" id="key" placeholder="<?php echo lang('pleaseEnter'); ?>关键字">
        </div>
        <div class="layui-inline layui-form">
            <select name="status" id="status">
                <option value="">选择产品</option>
                <option value="0">已打款</option>
                <option value="1">未打款</option>
                <option value="2">已拒绝</option>
            </select>
        </div>        
        <button class="layui-btn" id="search" data-type="reload">搜索</button>
        <button class="layui-btn layui-btn-danger">未打款金额:<?php echo htmlentities($weida); ?>元</button>
        <button class="layui-btn layui-btn-normal">已打款金额:<?php echo htmlentities($yida); ?>元</button>
        <div class="layui-inline" style="float: right;margin-right: 10px;">
            <button type="button" class="layui-btn layui-btn-danger" id="payAll">批量打款</button>
        </div>
    </div>
    <table class="layui-table" id="list" lay-filter="list"></table>
</div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script type="text/html" id="status">
 {{# if(d.status==1){ }}
    <button class="layui-btn layui-btn-xs">待打款</button>
 {{# }else if(d.status==2){  }}
    <button class="layui-btn layui-btn-xs layui-btn-danger">已驳回</button>
    {{# }else{  }}
    <button class="layui-btn layui-btn-xs layui-btn-warm">已打款</button>
    {{# } }}
</script>
<script type="text/html" id="action">

{{# if(d.status==0){ }}
    已打款
{{# }else if(d.status==2){  }}
    已驳回
{{# }else{  }}
<a class="layui-btn layui-btn-sm" lay-event="tongguo"><i class="layui-icon">&#xe605;</i>打款</a>
<a class="layui-btn layui-btn-sm layui-btn-danger"  lay-event="bohui"><i class="layui-icon">&#x1006;</i>驳回</a>
{{# } }}
</script>
<script>
   layui.use(['table','form'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery;
        var tableIn = table.render({
            id: 'cash',
            elem: '#list',
            url: '<?php echo url("Order/cash"); ?>',
            method: 'post',
            page: true,
            cols: [[
                {checkbox:true,fixed: true},
                {field: 'vid', title: '会员ID', width: 80, fixed: true},
                {field: 'name', title: '姓名', width: 80},
                {field: 'sn', title: '订单号', width: 150},
                {field: 'tel', title: '手机号', width: 130},
                {field: 'nickname', title: '昵称', width: 100},
                {field: 'money', title: '金额', width: 80},
                {field: 'alipay', title: '支付宝账号',width: 150},
                // {field: 'status', title: '状态', width: 150,toolbar: '#status'},
                {field: 'ctime', title: '申请时间', width: 150},
                {field: 'remark', edit: 'text',title: '打款备注',width:267},
                {width: 200, title: '操作',align: 'center', toolbar: '#action'}
            ]],
            limit: 20 //每页默认显示的数量
        });
        //搜索
        $('#search').on('click', function() {
            var key = $('#key').val();
            var status = $('#status').val();
            /*if($.trim(key)==='') {
                layer.msg('<?php echo lang("pleaseEnter"); ?>关键字！',{icon:0});
                return;
            }*/
            tableIn.reload({ page: {page: 1},where: {key: key,status:status}});
        });
        
        //数据操作
     table.on('tool(list)', function(obj){
        var data = obj.data;
        debugger;
        if(obj.event === 'tongguo'){
          layer.confirm('您确定要打款吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    // alert(data.remark);
                    $.post("<?php echo url('data/dakuan'); ?>",{id:data.id,'remark':data.remark},function(res){
                        layer.close(loading);
                        if(res.code===1){
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            tableIn.reload();
                        });
                        }else{
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            tableIn.reload();
                        });
                        }
                    });
                    layer.close(index);
                });
        } else if(obj.event === 'bohui'){
          layer.confirm('您确定要驳回吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo url('data/cashbohui'); ?>",{id:data.id},function(res){
                        layer.close(loading);
                        if(res.code===1){
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            tableIn.reload();
                        });
                        }else{
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            tableIn.reload();
                        });
                        }
                    });
                    layer.close(index);
                });
        }
      });
        $('#payAll').click(function(){
            layer.confirm('确认要批量打款吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('cash'); //test即为参数id设定的值
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo url('data/payAll'); ?>", {ids: ids}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        })     
    });



   layui.use('table', function(){
       var table = layui.table;

       //监听单元格编辑
       table.on('edit(cash)', function(obj){
           debugger;
           var value = obj.value; //得到修改后的值
               // ,data = obj.data //得到所在行所有键值
               // ,field = obj.field; //得到字段
           layer.msg('[ID: '+ data.id +'] ' + field + ' 字段更改为：'+ value);
       });
   });

</script>
</body>
</html>
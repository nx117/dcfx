<?php /*a:1:{s:68:"/www/wwwroot/39.97.170.249/application/home/view/article/detail.html";i:1569590657;}*/ ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no">
    <meta name="format-detection" content="telephone=no" >
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title><?php echo htmlentities($art['title']); ?></title>
    <link rel="stylesheet" href="/static/home/css/art.css">
    <script src="/static/home/js/jquery.js"></script>


    <link rel="stylesheet" href="/static/home/css/style.css" />
    <link rel="stylesheet" href="/static/home/css/pay.css" />
    <link rel="stylesheet" type="text/css" href="/static/home/pay/css/index.css">
    <script type="text/javascript" src="/static/home/js/jquery-2.1.4.min.js"></script>


    <style type="text/css">
        section{width: auto !important;}
        .infosactoe{margin: 16px 0;font-size: 0px;color: #a5b3c6}
        .infosactoe span{font-size: 15px;display: inline-block;vertical-align: middle;line-height: 15px;}
        .infosactoe .usm{padding-right: 6px;border-right: 1px solid #eee;max-width: 100px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
        .infosactoe .time{padding-left: 6px;}
        .article p{margin-bottom:20px;word-wrap:break-word;}
    </style>
</head>
<body>
    <div class="article" > 
        <h2><?php echo htmlentities($art['title']); ?></h2>
        <div class="infosactoe">
            <span class="usm"><?php echo htmlentities($sys['name']); ?></span>
            <span class="time"><?php echo htmlentities(date(('Y-m-d H:i'),!is_numeric($art['add_time'])? strtotime($art['add_time']) : $art['add_time'])); ?> &nbsp;点击数：<?php echo htmlentities($art['hits']); ?></span>
        </div>
        <?php echo htmlspecialchars_decode($art['content']);?>
    </div>

   
</body>




</html>
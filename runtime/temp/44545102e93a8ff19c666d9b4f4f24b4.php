<?php /*a:3:{s:68:"/www/wwwroot/39.97.170.249/application/admin/view/system/alipay.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>支付配置</legend>
    </fieldset>
    <div class="layui-tab">
        <ul class="layui-tab-title">
            <li class="layui-this">支付宝配置</li>
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <form class="layui-form layui-form-pane" lay-filter="form-system">
                    <div class="layui-form-item">
                        <label class="layui-form-label">APPID</label>
                        <div class="layui-input-4">
                            <input type="text" name="appid" lay-verify="required" placeholder="<?php echo lang('pleaseEnter'); ?>支付宝APPID" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">RSA密钥</label>
                        <div class="layui-input-4">
                            <input type="text" name="rsakey" lay-verify="rsakey" placeholder="<?php echo lang('pleaseEnter'); ?>RSA密钥" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">支付宝密钥</label>
                        <div class="layui-input-4">
                            <input type="text" name="alipaykey" lay-verify="required" placeholder="<?php echo lang('pleaseEnter'); ?>支付宝密钥" class="layui-input">
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">回调地址</label>
                        <div class="layui-input-4">
                            <input type="text" placeholder="<?php echo htmlentities($pay['url']); ?>/index/pay/hddz" class="layui-input" disabled="">
                        </div>
                        <div class="layui-form-mid layui-word-aux">将网址填入企业支付宝对应地址</div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">支付网关</label>
                        <div class="layui-input-4">
                            <input type="text" placeholder="<?php echo htmlentities($pay['url']); ?>/index/pay/yywg" class="layui-input" disabled="">
                        </div>
                        <div class="layui-form-mid layui-word-aux">将网址填入企业支付宝对应地址</div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button type="button" class="layui-btn" lay-submit="" lay-filter="sys"><?php echo lang('submit'); ?></button>
                            <button type="reset" class="layui-btn layui-btn-primary"><?php echo lang('reset'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script>
    layui.use(['form', 'layer','upload','element'], function () {
        var form = layui.form,layer = layui.layer,upload = layui.upload,$ = layui.jquery,element = layui.element;
        var seytem = <?php echo $system; ?>;
        form.val("form-system", seytem);
        //提交监听
        form.on('submit(sys)', function (data) {
            loading =layer.load(1, {shade: [0.1,'#fff']});
            $.post("<?php echo url('system/alipay'); ?>",data.field,function(res){
                layer.close(loading);
                if(res.code > 0){
                    layer.msg(res.msg,{icon: 1, time: 1000},function(){
                        location.href = res.url;
                    });
                }else{
                    layer.msg(res.msg,{icon: 2, time: 1000});
                }
            });
        })
    })
</script>
</body>
</html>
<?php /*a:2:{s:66:"/www/wwwroot/39.97.170.249/application/home/view/article/usth.html";i:1571753249;s:71:"/www/wwwroot/39.97.170.249/application/home/view/common/new_footer.html";i:1569590650;}*/ ?>
<html>
<head>
    <meta charset="UTF-8" />
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1; user-scalable=no" />
    <title>活动公告</title>
    <link rel="stylesheet" href="/static/home/css/czym_dbk.css" />
    <script type="text/javascript" src="/static/home/js/vue.js"></script>
    <style>
        [v-cloak] {
            display: none;
        }
        .activity-li-box{margin-bottom: 0;}
    </style>

    <link rel="stylesheet" type="text/css" href="/static/home/tb/css/public.css">
    <link rel="stylesheet" type="text/css" href="/static/home/tb/css/style.css">
    <link rel="stylesheet" href="/static/home/css/czym_dbk.css?e">
    <script src="/static/home/js/jquery-1.12.4.min.js"></script>
    <script src="/static/home/js/czym_dbk.js"></script>

</head>
<body style="background-color: #F1F4F8">
<div id="app">
    <form method="post" action="<?php echo url('article/usth'); ?>">
        <div class="alliancefom-sosu-box">

            <input type="text" name="key" placeholder="请输入搜索文章" />
            <button type="submit">搜索</button>

        </div>
    </form>
    <div class="record-head-box" style="margin-top: 0px;">
        <span class="title active" ><a>活动公告</a> <span></span></span>

    </div>
    <div class="activity-ul-box">
        <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
        <a href="javascript:;" onclick="checkUser(this,'<?php echo htmlentities($vo['id']); ?>')">
            <div class="activity-li-box">
                <div class="activity-tl-box">
                    <span><?php echo htmlentities($vo['title']); ?></span>
                    <span><?php echo htmlentities(date(('Y-m-d H:i'),!is_numeric($vo['add_time'])? strtotime($vo['add_time']) : $vo['add_time'])); ?> </span>
                </div>
                <div class="activity-img-box">
                    <img src="<?php echo htmlentities($vo['img']); ?>" />
                </div>
            </div>
        </a>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
    <div class="activity-ul-box" style="display: none;">

        <?php if(is_array($xblist) || $xblist instanceof \think\Collection || $xblist instanceof \think\Paginator): $i = 0; $__LIST__ = $xblist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
        <a href="javascript:;" onclick="checkUser(this,'<?php echo htmlentities($vo['id']); ?>')">
            <div class="activity-li-box">
                <div class="activity-tl-box">
                    <span><?php echo htmlentities($vo['title']); ?></span>
                    <span><?php echo htmlentities(date(('Y-m-d H:i'),!is_numeric($vo['add_time'])? strtotime($vo['add_time']) : $vo['add_time'])); ?> </span>
                </div>
                <div class="activity-img-box">
                    <img src="<?php echo htmlentities($vo['img']); ?>" />
                </div>
            </div>
        </a>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
    <div class="no-ms-box" style="background-color: rgb(241, 244, 248); display: none;">
        <img src="../images/nocp_icon.png" alt="" />
        <span>暂无记录</span>
    </div>
    <div onclick="clicktop();" class="return-top-box" style="display: none;">
        <img src="/static/home/images/retunTop.png" alt="" />
    </div>
</div>
<script type="text/javascript" src="/static/home/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="/static/home/js/czym_dbk.js"></script>
<script src="/static/home/js/ydui.js"></script>
<link rel="stylesheet" href="/static/home/css/ydui.css" />
<script>
    function checkUser(obj,id){
        $.post("<?php echo url('article/checkUser'); ?>",{id:id},function(data){
            if (data.code==1) {
                YDUI.dialog.toast(data.msg, 'error', 800,function() {});
            }else{
                location.replace("/article/id/"+id);
            }
        })
    }
</script>
<script type="text/javascript">
    $(function(){
        $(".title").click(function(){
            $(this).addClass("active").siblings().removeClass("active");
            $(".activity-ul-box").eq($(".title").index(this)).show().siblings(".activity-ul-box").hide();
        });
    });
    function clicktop(){
        $("html,body").animate({
            scrollTop: 0
        }, 500);
    }

    $(window).scroll(function(){
        var t = $(this).scrollTop(), h = $(this).height(), dh = $(document).height();
        //滚动分页
        if(((t + h) > (dh - 1))) {
            //     vm.getInfo('page');
            if(vm.page <= vm.totalPage) {
                vm.page++;
                vm.get_info();
            }

        }
    });
</script>

 <!--<div class="footer-box">
	<ul>
		<li>
			<a href="/">
				<img src="/static/home/images/WX20181029-110205@2x_meitu_3.png" alt="" />
				<span>鑫云</span>
			</a>
		</li>
		<li>
			<a href="<?php echo url('user/level'); ?>">
				<img src="/static/home/images/vip.png" alt="" />
				<span>代理</span>
			</a>
		</li>
		<li>
			<a href="/data/index">
				<img src="/static/home/images/data_icon.png" alt="" />
				<span>钱包</span>
			</a>
		</li>
		<li>
			<a href="/article/index">
				<div class="bq">
					<img src="/static/home/images/yj_icon.png" alt="" />
				</div>
				<span>消息</span>
			</a>
		</li>
		<li>
			<a href="/user/index">
				<img src="/static/home/images/grzx_icon.png" alt="" />
				<span>个人中心</span>
			</a>
		</li>
	</ul>


</div>-->

 <link rel="stylesheet" type="text/css" href="/static/home/tb/css/public.css">
 <link rel="stylesheet" type="text/css" href="/static/home/tb/css/style.css">

 <!--bottom-->
 <div class="botkong"></div>
 <div class="bot">
	 <li class="li02">
		 <a href="/">
			 <div class="tac fs12 pt30">推广</div>
		 </a>
	 </li>
	 <li class="li01">
		 <a href="/commodity">
			 <div class="tac fs12 pt30">资讯</div>
		 </a>
	 </li>
	 <li class="li03">
		 <a href="/user/order">
			 <div class="tac fs12 pt30">订单</div>
		 </a>
	 </li>
	 <li class="li04">
		 <a href="/user/index">
			 <div class="tac fs12 pt30">我的</div>
		 </a>
	 </li>
	 <div class="clear"></div>
 </div>

</body>
</html>
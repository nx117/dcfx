<?php /*a:3:{s:70:"/www/wwwroot/39.97.170.249/application/admin/view/order/loanindex.html";i:1571753806;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>网贷订单列表</legend>
    </fieldset>
    <div class="demoTable">
        <div class="layui-inline layui-form">
            <!--<input class="layui-input" name="key" id="key" placeholder="<?php echo lang('pleaseEnter'); ?>产品ID">-->
            <select name="key" id="key">
                <option value="">选择产品</option>
                <?php if(is_array($product) || $product instanceof \think\Collection || $product instanceof \think\Paginator): $i = 0; $__LIST__ = $product;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <option value="<?php echo htmlentities($vo['id']); ?>"><?php echo htmlentities($vo['title']); ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>            
            </select>
        </div>
        <div class="layui-inline">
            <input class="layui-input" name="phone" id="phone" placeholder="<?php echo lang('pleaseEnter'); ?>推广者手机号">
        </div>
        <div class="layui-inline">
            <input class="layui-input" type="text" name="starttime" id="starttime" placeholder="开始日期">
        </div>
        <div class="layui-inline">
            <input class="layui-input" type="text" name="endtime" id="endtime" placeholder="结束日期">
        </div>
        <button class="layui-btn" id="search" data-type="reload">搜索</button>
        <div class="layui-inline" style="float: right;margin-right: 10px;">
            <button type="button" class="layui-btn layui-btn-normal" id="shenheAll">批量审核</button>
            <button type="button" class="layui-btn layui-btn-danger" id="bohuiAll">批量驳回</button>
        </div>
    </div>
    <div class="demoTable" style="margin-top:10px;">
        <button class="layui-btn layui-btn-primary">未审核订单:<span id="weishen"></span></button>
        <button class="layui-btn layui-btn-primary">订单总数:<span id="all"></span></button>
    </div>
    <table class="layui-table" id="list" lay-filter="list"></table>
</div>
<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script type="text/html" id="status">
    {{# if(d.status==1){ }}
    <button class="layui-btn layui-btn-xs">待审核</button> {{# }else if(d.status==2){ }}
    <button class="layui-btn layui-btn-xs layui-btn-danger">已驳回</button> {{# }else{ }}
    <button class="layui-btn layui-btn-xs layui-btn-warm">通过</button> {{# } }}
</script>


<script type="text/html" id="snDate">

</script>



<script type="text/html" id="cha">
    <a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="cha"><i class="layui-icon">&#xe615;</i>查看</a>
</script>
<script type="text/html" id="tuiguang">
    <span>d.nickname(d.phone)</span>
</script>
<script type="text/html" id="action">

    {{# if(d.status==0){ }} 已通过 {{# }else if(d.status==2){ }} 已驳回 {{# }else{ }}

    <!--    <a class="layui-btn layui-btn-xs" href="<?php echo url('order/edit'); ?>?id={{d.id}}">贷款总额</a>-->
    <a class="layui-btn layui-btn-sm" lay-event="tongguo"><i class="layui-icon">&#xe605;</i>通过</a>
    <a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="bohui"><i class="layui-icon">&#x1006;</i>驳回</a> {{# } }} &nbsp;&nbsp;
    <!--    <a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="delete">删除</a>-->
</script>

<script>
    layui.use(['table', 'form', 'laydate'], function() {
        var table = layui.table,
            form = layui.form,
            $ = layui.jquery,
            laydate = layui.laydate;
        laydate.render({
            elem: '#starttime' //指定元素
                ,
            type: 'datetime',
            theme: 'grid',
            format: 'yyyy-MM-dd HH:mm:ss'
        });
        laydate.render({
            elem: '#endtime',
            type: 'datetime',
            theme: 'grid',
            format: 'yyyy-MM-dd HH:mm:ss'
        });
        var tableIn = table.render({
            id: 'order',
            elem: '#list',
            url: '<?php echo url("Order/loan"); ?>',
            method: 'post',
            page: true,
            cols: [
                [{
                    checkbox: true,
                    fixed: true
                }, {
                    field: 'fan_id1',
                    title: '推广者',
                    width: 80
                }, {
                    field: 'sn',
                    title: '订单号',
                    width: 80
                }, {
                    field: 'name',
                    title: '姓名',
                    width: 80
                }, {
                    field: 'card',
                    title: '身份证',
                    width: 150
                }, {
                    field: 'tel',
                    title: '手机号',
                    width: 120
                }, {
                    field: 'title',
                    title: '网贷名称',
                    width: 150
                }, {
                    field: 'status',
                    title: '状态',
                    width: 80,
                    toolbar: '#status'
                }, {
                    field: 'per',
                    title: '返佣',
                    width: 80
                }, {
                    field: 'ctime',
                    title: '申请时间',
                    width: 150
                }, {
                    title: '查看返佣',
                    width: 100,
                    align: 'center',
                    toolbar: '#cha'
                }, {
                    field: 'total',
                    title: '贷款总额',
                    edit: 'text',
                    width: 100
                }, {
                    width: 190,
                    title: '操作',
                    align: 'center',
                    toolbar: '#action'
                }]
            ],
            parseData: function(res) { //res 即为原始返回的数据
                $('#all').html(res.all);
                $('#weishen').html(res.weishen);
            },
            limit: 30 //每页默认显示的数量
        });
        //搜索
        $('#search').on('click', function() {
            var key = $('#key').val();
            var phone = $('#phone').val();
            var starttime = $('#starttime').val();
            var endtime = $('#endtime').val();
            /*if($.trim(key)==='') {
                layer.msg('<?php echo lang("pleaseEnter"); ?>关键字！',{icon:0});
                return;
            }*/
            tableIn.reload({
                page: {
                    page: 1
                },
                where: {
                    key: key,
                    phone: phone,
                    starttime: starttime,
                    endtime: endtime
                }
            });
        });
        //审核通过
        //数据操作
        table.on('tool(list)', function(obj) {
            var data = obj.data;
            if (obj.event === 'tongguo') {
                if (data.total == '') {
                    layer.msg(贷款总金额不能为空, {
                        time: 1800,
                        icon: 1
                    }, function() {});
                }
                layer.confirm('您确定要通过这条数据吗？', function(index) {
                    var loading = layer.load(1, {
                        shade: [0.1, '#fff']
                    });
                    $.post("<?php echo url('data/loantg'); ?>", {
                        id: data.id,
                        'total': data.total
                    }, function(res) {
                        layer.close(loading);
                        if (res.code === 1) {
                            layer.msg(res.msg, {
                                time: 1800,
                                icon: 1
                            }, function() {
                                tableIn.reload();
                            });
                        } else {
                            layer.msg(res.msg, {
                                time: 1800,
                                icon: 1
                            }, function() {
                                tableIn.reload();
                            });
                        }
                    });
                    layer.close(index);
                });
            } else if (obj.event === 'bohui') {
                layer.confirm('您确定要驳回这条数据吗？', function(index) {
                    var loading = layer.load(1, {
                        shade: [0.1, '#fff']
                    });
                    $.post("<?php echo url('data/bohui'); ?>", {
                        id: data.id
                    }, function(res) {
                        layer.close(loading);
                        if (res.code === 1) {
                            layer.msg(res.msg, {
                                time: 1800,
                                icon: 1
                            }, function() {
                                tableIn.reload();
                            });
                        } else {
                            layer.msg(res.msg, {
                                time: 1800,
                                icon: 1
                            }, function() {
                                tableIn.reload();
                            });
                        }
                    });
                    layer.close(index);
                });
            } else if (obj.event === 'cha') {
                var id = data.id;
                layer.open({
                    type: 2,
                    title: '返佣详情',
                    shadeClose: true,
                    shade: false,
                    maxmin: true, //开启最大化最小化按钮
                    area: ['893px', '200px'],
                    content: '/order/fanyong/' + id,
                });
            } else if (obj.event === 'delete') {
                layer.confirm('您确定要删除这条数据吗？', function(index) {
                    var loading = layer.load(1, {
                        shade: [0.1, '#fff']
                    });
                    $.post("<?php echo url('data/delete'); ?>", {
                        id: data.id
                    }, function(res) {
                        layer.close(loading);
                        if (res.code === 1) {
                            layer.msg(res.msg, {
                                time: 1800,
                                icon: 1
                            }, function() {
                                tableIn.reload();
                            });
                        } else {
                            layer.msg(res.msg, {
                                time: 1800,
                                icon: 1
                            }, function() {
                                tableIn.reload();
                            });
                        }
                    });
                    layer.close(index);
                });
            }
        });
        $('#bohuiAll').click(function() {
            layer.confirm('确认要批量驳回吗？', {
                icon: 3
            }, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('order'); //test即为参数id设定的值
                var ids = [];
                $(checkStatus.data).each(function(i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {
                    shade: [0.1, '#fff']
                });
                $.post("<?php echo url('data/bohuiAll'); ?>", {
                    ids: ids
                }, function(data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {
                            time: 1000,
                            icon: 1
                        });
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {
                            time: 1000,
                            icon: 2
                        });
                    }
                });
            });
        });
        $('#shenheAll').click(function() {
            layer.confirm('确认要批量审核吗？', {
                icon: 3
            }, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('order'); //test即为参数id设定的值
                var ids = [];
                $(checkStatus.data).each(function(i, o) {
                    ids.push(o.id);
                });
                console.log(ids);
                var loading = layer.load(1, {
                    shade: [0.1, '#fff']
                });
                $.post("<?php echo url('data/shenheAll'); ?>", {
                    ids: ids
                }, function(data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {
                            time: 1000,
                            icon: 1
                        });
                        tableIn.reload();
                    } else {
                        layer.msg(data.msg, {
                            time: 1000,
                            icon: 2
                        });
                    }
                });
            });
        })
    });
</script>
</body>

</html>
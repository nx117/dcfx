<?php /*a:3:{s:63:"/www/wwwroot/39.97.170.249/application/admin/view/bank/add.html";i:1569419332;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="/ueditor/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="/ueditor/lang/zh-cn/zh-cn.js"></script>
<div class="admin-main layui-anim layui-anim-upbit" ng-app="hd" ng-controller="ctrl">
    <fieldset class="layui-elem-field layui-field-title">
        <legend><?php echo htmlentities($title); ?></legend>
    </fieldset>
    <form class="layui-form" method="post">
          <div class="layui-form-item">
              <input type="hidden" name="type" value="<?php echo htmlentities($type); ?>"/>
                          <label for="title" class="layui-form-label">
                              <span class="x-red">*</span>商品名称
                          </label>
                          <div class="layui-input-inline">
                                <input type="text" name="title" required="" lay-verify="required"
                                autocomplete="off" class="layui-input"  placeholder="<?php echo lang('pleaseEnter'); ?>商品名称" >
                          </div>
<!--                          <div class="layui-form-item" pane="">-->
<!--                              <label class="layui-form-label">分类</label>-->
<!--                              <div class="layui-input-block">-->
<!--                                  <?php foreach($cate as $v): ?>-->
<!--                                  <input type="checkbox" name="loan_id[]" lay-skin="primary" title="<?php echo htmlentities($v['title']); ?>" value="<?php echo htmlentities($v['id']); ?>">-->
<!--                                  <?php endforeach; ?>-->
<!--                              </div>-->
<!--                          </div>-->
<!--                          <label for="title" class="layui-form-label">-->
<!--                                <span class="x-red">*</span>申请人数-->
<!--                          </label>-->
<!--                            <div class="layui-input-inline">-->
<!--                                <input type="number" id="title" name="num" required="" lay-verify="required"-->
<!--                                autocomplete="off" class="layui-input">-->
<!--                            </div>-->

                        <label for="sort" class="layui-form-label">
                                <span class="x-red">*</span>排序
                            </label>
                            <div class="layui-input-inline">
                                <input type="number" id="L_repass" name="sort" required="" lay-verify="sort"
                                autocomplete="off" class="layui-input">
                            </div>

                            <label for="auth" class="layui-form-label">
                                <span class="x-red">*</span>活动标语 </label>
                            <div class="layui-input-inline">
                                <input type="text"   placeholder="例：限时加佣"  name="jiangjin" required="" lay-verify="required"
                                       autocomplete="off" class="layui-input">
                            </div>

                        </div>
                      <div class="layui-form-item">
                          <label for="auth" class="layui-form-label">
                                    <span class="x-red">*</span>普通返佣
                                </label>
                                <div class="layui-input-inline">
                                    <input type="text" placeholder="请输入返佣" name="mout" required="" lay-verify="required"
                                    autocomplete="off" class="layui-input">
                                </div>
                              <label for="auth" class="layui-form-label">
                                  <span class="x-red">*</span>vip返佣
                              </label>
                              <div class="layui-input-inline">
                                  <input type="text" placeholder="请输入返佣" name="vipmout" required="" lay-verify="required"
                                         autocomplete="off" class="layui-input">
                              </div>
                          <label for="auth" class="layui-form-label">
                              <span class="x-red">*</span>总佣金
                          </label>
                          <div class="layui-input-inline">
                              <input type="text" placeholder="请输入收入佣金" name="total" required="" lay-verify="required"
                                     autocomplete="off" class="layui-input">
                          </div>

                                <label for="auth" class="layui-form-label">
                                    <span class="x-red">*</span>结算日期
                                </label>
                                <div class="layui-input-inline">
                                    <input type="text" placeholder="例：每周五结" name="jiesuan" required="" lay-verify="required"
                                    autocomplete="off" class="layui-input">
                                </div>

                      </div>

                          <div class="layui-form-item">


                              <label for="auth" class="layui-form-label">
                                  <span class="x-red">*</span>标签1
                              </label>
                              <div class="layui-input-inline">
                                  <input type="text" placeholder="请输入标签" name="biaoqian1" required="" lay-verify="required"
                                         autocomplete="off" class="layui-input">
                              </div>

                              <label for="auth" class="layui-form-label">
                                  <span class="x-red">*</span>标签2
                              </label>
                              <div class="layui-input-inline">
                                  <input type="text" placeholder="请输入标签" name="biaoqian2" required="" lay-verify="required"
                                         autocomplete="off" class="layui-input">
                              </div>

                              <label for="auth" class="layui-form-label">
                                  <span class="x-red">*</span>简介
                              </label>
                              <div class="layui-input-inline">
                                  <input type="text" placeholder="请输入标签" name="subscript" required="" lay-verify="required"
                                         autocomplete="off" class="layui-input">
                              </div>

                          </div>


                          <div class="layui-form-item">
                              <label class="layui-form-label">LOGO</label>
                              <input type="hidden" name="logo" id="image2">
                              <div class="layui-input-block">
                                  <div class="layui-upload">
                                      <button type="button" class="layui-btn layui-btn-primary" id="cateBtn2"><i class="icon icon-upload3"></i>点击上传</button>
                                      <div class="layui-upload-list">
                                          <img class="layui-upload-img" id="cateImage2" width="80px">
                                          <p id="demoText2"></p>
                                      </div>
                                  </div>
                              </div>
                          </div>


                            <div class="layui-form-item">
                              <label class="layui-form-label">详情页图片</label>
                              <input type="hidden" name="img" id="image">
                              <div class="layui-input-block">
                                  <div class="layui-upload">
                                      <button type="button" class="layui-btn layui-btn-primary" id="cateBtn"><i class="icon icon-upload3"></i>点击上传</button>
                                      <div class="layui-upload-list">
                                          <img class="layui-upload-img" id="cateImage" width="80px">
                                          <p id="demoText"></p>
                                      </div>
                                  </div>
                              </div>
                          </div>


                            <div class="layui-form-item">
                              <label class="layui-form-label">二维码背景</label>
                              <input type="hidden" name="bgpic" id="image1">
                              <div class="layui-input-block">
                                  <div class="layui-upload">
                                      <button type="button" class="layui-btn layui-btn-primary" id="cateBtn1"><i class="icon icon-upload3"></i>点击上传</button>
                                      <div class="layui-upload-list">
                                          <img class="layui-upload-img" id="cateImage1" width="80px">
                                          <p id="demoText1"></p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      <div class="layui-form-item">
                          <label class="layui-form-label">
                              二维码坐标
                          </label>
                          <div class="layui-input-block" style="width:600px">
                              <input type="text" placeholder="请输入二维码显示坐标" name="w" autocomplete="off" class="layui-input">
                              <div class="layui-form-mid layui-word-aux" style="color:red">设置格式为：88,230,0,0,140,150</div>
                          </div>
                      </div>
<!--                      <div class="layui-form-item">-->
<!--                          <label class="layui-form-label">-->
<!--                              简介-->
<!--                          </label>-->
<!--                          <div class="layui-input-block" style="width:600px">-->
<!--                              <input type="text" placeholder="请输入简介" name="desc" autocomplete="off" class="layui-input">-->
<!--                              <div class="layui-form-mid layui-word-aux" style="color:red">一句话简介不能超过50字</div>-->
<!--                          </div>-->
<!--                      </div>-->
        <div class="layui-form-item">

            <label for="auth" class="layui-form-label">
                <span class="x-red">*</span>甲方姓名
            </label>
            <div class="layui-input-inline">
                <input type="text" placeholder="请输入甲方姓名" name="jfname" required="" lay-verify="required"
                       autocomplete="off" class="layui-input">
            </div>


            <label for="auth" class="layui-form-label">
                <span class="x-red">*</span>甲方手机号 </label>
            <div class="layui-input-inline">
                <input type="text"  name="jftel" placeholder="请输入甲方手机号" required="" lay-verify="required"
                       autocomplete="off" class="layui-input">
            </div>


        </div>

                            <div class="layui-form-item">
                                <label for="auth" class="layui-form-label">
                                    <span class="x-red">*</span>申请网址
                                </label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_repass" name="href" required="" lay-verify="auth"
                                    autocomplete="off" class="layui-input"  style="width:1024px">
                                </div>
                            </div>

<!--                            <div class="layui-form-item">-->
<!--                                <label for="auth" class="layui-form-label">-->
<!--                                    <span class="x-red">*</span>查询网址-->
<!--                                </label>-->
<!--                                <div class="layui-input-inline">-->
<!--                                    <input type="text" id="L_repass" name="about" required="" lay-verify="auth"-->
<!--                                    autocomplete="off" class="layui-input"  style="width:1024px">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="layui-form-item">-->
<!--                              <label class="layui-form-label">内容</label>-->
<!--                              <input type="hidden" name="content" id="image">-->
<!--                              <div class="layui-input-block">-->
<!--                                  <script name="content" id="editor" type="text/plain" style="width:1024px;height:500px;"></script>-->
<!--                              </div>-->
<!--                          </div>-->

        <div class="layui-form-item">
            <label class="layui-form-label">相关费用</label>
            <input type="hidden" name="expenses" id="image">
            <div class="layui-input-block">
                <script name="expenses" id="editor1" type="text/plain" style="width:1024px;height:200px;"></script>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">相关权益</label>
            <input type="hidden" name="equity" id="image">
            <div class="layui-input-block">
                <script name="equity" id="editor2" type="text/plain" style="width:1024px;height:200px;"></script>
            </div>
        </div>

                            <div class="layui-form-item">
                                <label for="auth" class="layui-form-label">
                                    <span class="x-red">*</span>是否显示
                                </label>
                                <div class="layui-input-block">
                                  <input type="radio" name="status" value="1" title="是" checked="">
                                  <input type="radio" name="status" value="0" title="否">
                                </div>

                            </div>
                          <div class="layui-form-item">
                              <div class="layui-input-block">
                                  <button type="button" class="layui-btn" lay-submit="" lay-filter="submit"><?php echo lang('submit'); ?></button>
                                  <?php if(($type ==1)): ?>  <a href="<?php echo url('Product/recommendList'); ?>" class="layui-btn layui-btn-primary"><?php echo lang('back'); ?></a> <?php elseif(($type==0)): ?>
                                  <a href="<?php echo url('Product/bankList'); ?>" class="layui-btn layui-btn-primary"><?php echo lang('back'); ?></a>
                                  <?php else: endif; ?>

                              </div>
                          </div>
                      </form>
                  </div>
                  <script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


                  <script src="/static/common/js/angular.min.js"></script>
                  <script>
                      var ue = UE.getEditor('editor');
                      var ue = UE.getEditor('editor1');
                      var ue = UE.getEditor('editor2');
                      layui.use(['form', 'layer','upload'], function () {
                          var form = layui.form, layer = layui.layer,$= layui.jquery,upload = layui.upload;
                          var info = <?php echo $info; ?>;
                          form.val("form", info);
                          if(info){
                              $('#adPic').attr('src',info.avatar);
                          }
                          form.render();
                          form.on('submit(submit)', function (data) {
                              loading =layer.load(1, {shade: [0.1,'#fff']});
                              $.post("", data.field, function (res) {
                                  layer.close(loading);
                                  if (res.code > 0) {
                                      layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                                          location.href = res.url;
                                      });
                                  } else {
                                      layer.msg(res.msg, {time: 1800, icon: 2});
                                  }
                              });
                          });

                            //普通图片上传
                          var uploadInst = upload.render({
                                elem: '#cateBtn',
                                url: '<?php echo url("UpFiles/upload"); ?>',
                                done: function(res){
                                    if(res.code>0){
                                        $('#image').val(res.url);
                                        $('#cateImage').attr('src', res.url);
                                    }else{
                                        //如果上传失败
                                        return layer.msg('上传失败');
                                    }
                                },
                                error: function(){
                                    //演示失败状态，并实现重传
                                    var demoText = $('#demoText');
                                    demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                                    demoText.find('.demo-reload').on('click', function(){
                                        uploadInst.upload();
                                    });
                                }
                         });




                          //背景二维码
                          var uploadInst = upload.render({
                                elem: '#cateBtn1',
                                url: '<?php echo url("UpFiles/upload"); ?>',
                                done: function(res){
                                    if(res.code>0){
                                        $('#image1').val(res.url);
                                        $('#cateImage1').attr('src', res.url);
                                    }else{
                                        //如果上传失败
                                        return layer.msg('上传失败');
                                    }
                                },
                                error: function(){
                                    //演示失败状态，并实现重传
                                    var demoText = $('#demoText1');
                                    demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                                    demoText.find('.demo-reload').on('click', function(){
                                        uploadInst.upload();
                                    });
                                }
                         });

                          //银行logo
                          var uploadInst = upload.render({
                                elem: '#cateBtn2',
                                url: '<?php echo url("UpFiles/upload"); ?>',
                                done: function(res){
                                    if(res.code>0){
                                        $('#image2').val(res.url);
                                        $('#cateImage2').attr('src', res.url);
                                    }else{
                                        //如果上传失败
                                        return layer.msg('上传失败');
                                    }
                                },
                                error: function(){
                                    //演示失败状态，并实现重传
                                    var demoText = $('#demoText2');
                                    demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                                    demoText.find('.demo-reload').on('click', function(){
                                        uploadInst.upload();
                                    });
                                }
                         });
                      });
                  </script>
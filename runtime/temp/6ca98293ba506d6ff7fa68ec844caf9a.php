<?php /*a:3:{s:69:"/www/wwwroot/39.97.170.249/application/admin/view/order/payindex.html";i:1570196264;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/head.html";i:1569419339;s:66:"/www/wwwroot/39.97.170.249/application/admin/view/common/foot.html";i:1569419339;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/static/common/css/font.css" media="all">
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>会员订单列表</legend>
    </fieldset>
    <div class="demoTable">
        <div class="layui-inline">
            <input class="layui-input" name="key" id="key" placeholder="<?php echo lang('pleaseEnter'); ?>关键字">
        </div>
        <button class="layui-btn" id="search" data-type="reload">搜索</button>
        <button type="button" class="layui-btn layui-btn-primary">收款总额:<?php echo htmlentities($all); ?></button>
    </div>
    <table class="layui-table" id="list" lay-filter="list"></table>

    <script type="text/html" id="status">
        {{# if(d.status==1){ }}
       未支付
        {{# }else{  }}
        已支付
        {{# } }}
    </script>

<script type="text/javascript" src="/static/plugins/layui/layui.js"></script>


<script>
   layui.use(['table','form'], function() {
        var table = layui.table,form = layui.form, $ = layui.jquery;
        var tableIn = table.render({
            id: 'pay',
            elem: '#list',
            url: '<?php echo url("Order/pay"); ?>',
            method: 'post',
            page: true,
            cols: [[
                {checkbox:true,fixed: true},
                {field: 'sn', title: '订单编号', width: 220, fixed: true},
                {field: 'nickname', title: '用户ID', width: 100},
                {field: 'name', title: '姓名', width: 120},
                {field: 'tel', title: '手机号', width: 150},
                {field: 'fan_id1', title: '上级用户ID', width: 100},
                {field: 'money', title: '支付金额', width: 100},
                {field: 'status', title: '支付状态', width: 100,toolbar:"#status"},
                {field: 'ctime', title: '提交时间', width: 150},
                {field: 'ctime', title: '支付时间', width: 150}
            ]],
            limit: 10 //每页默认显示的数量
        });
        //搜索
        $('#search').on('click', function() {
            var key = $('#key').val();
            if($.trim(key)==='') {
                layer.msg('<?php echo lang("pleaseEnter"); ?>关键字！',{icon:0});
                return;
            }
            tableIn.reload({ page: {page: 1},where: {key: key}});
        });
    });
</script>
</body>
</html>
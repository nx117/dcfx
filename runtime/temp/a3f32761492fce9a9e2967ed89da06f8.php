<?php /*a:1:{s:65:"/www/wwwroot/39.97.170.249/application/home/view/user/recard.html";i:1569590646;}*/ ?>
<!DOCTYPE html>
<html>
<head>

	<title>个人收支明细</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<meta name="renderer" content="webkit">
	<link rel="stylesheet" type="text/css" href="/static/home/recard/css/style.css">
	<link rel="stylesheet" type="text/css" href="/static/home/recard/css/public.css">
</head>
<body class="yemianbg">
	<div class="header w">
		<a href="/user/index"><img src="/static/home/recard/img/tixian2.png"></a> <span>收支明细</span>
	</div>
<div class="tab">
		<ul class="tab-hd">
			<li class="active"><span>收入记录</span></li>
			<li class="activc"><span>提现记录</span></li>
		</ul>
		<ul class="tab-bd">
			<li class="thisclass">
				<?php if(is_array($recard) || $recard instanceof \think\Collection || $recard instanceof \think\Paginator): $i = 0; $__LIST__ = $recard;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
				<div class="shourutp">
					<span class="time"><?php echo htmlentities(date(('Y-m-d H:i'),!is_numeric($v['update_time'])? strtotime($v['update_time']) : $v['update_time'])); ?></span><span class="money"><?php echo htmlentities($v['money']); ?></span>
					<img src="<?php echo htmlentities($v['img']); ?>">
					<span class="chanping"><?php echo htmlentities($v['title']); ?><br></span>
					<span class="bianhao">工资编号：762828</span>
				</div>
				<?php endforeach; endif; else: echo "$empty" ;endif; ?>
			</li>
			<li class="thisclass1">
				<?php if(is_array($cash) || $cash instanceof \think\Collection || $cash instanceof \think\Paginator): $i = 0; $__LIST__ = $cash;if( count($__LIST__)==0 ) : echo "$empty" ;else: foreach($__LIST__ as $key=>$c): $mod = ($i % 2 );++$i;?>
				<div class="shourutp">
					<span class="time"><?php echo htmlentities(date(('Y-m-d H:i'),!is_numeric($c['ctime'])? strtotime($c['ctime']) : $c['ctime'])); ?></span><span class="money">￥<?php echo htmlentities($c['money']); ?></span>
					<span class="yinlian">支付宝(<?php echo htmlentities($c['alipay']); ?>)</span>
					<span class="danhao">提现单号：80<?php echo htmlentities($c['id']); ?></span><span class="success"><?php if(($c['status'] ==0)): ?> 已通过 <?php endif; if(($c['status'] ==1)): ?> 已通过 <?php endif; if(($c['status'] ==2)): ?> 已驳回 <?php endif; ?></span>
				</div>
				<?php endforeach; endif; else: echo "$empty" ;endif; ?>

				
		</li>
		</ul>
	</div>
<script src="/static/home/recard/js/jquery.min.js"></script>
<script>
$(function(){
    function tabs(tabTit,on,tabCon){
        $(tabTit).children().hover(function(){
            $(this).addClass(on).siblings().removeClass(on);
            var index = $(tabTit).children().index(this);
           	$(tabCon).children().eq(index).show().siblings().hide();
    	});
	};
    tabs(".tab-hd","active",".tab-bd");
});
</script>
	
</body>
</html>
<!-- <div class="top">
		<ul class="tap_hd">
			<li class="active"><a href="">收入内容</a></li>
			<li class="tixian"><a href="">提现记录</a></li>
		</ul>
		<ul class="tab-bd">
			<li class="left">
				<div class="bigd">
					<div class="thisclass"><a href="">
						<span class="time">2019-07-05	10:23:22</span><span class="money">￥1.71</span>
						<img src="img/tixian1.jpg">
						<span class="chanping">出师团队工资提成(橙子信用(详<br>版)产品)</span>
						<span class="bianhao">工资编号：762828</span>
					</a></div>
					<div class="thisclass"><a href="">
						<span class="time">2019-07-05	10:23:22</span><span class="money">￥1.71</span>
						<img src="img/tixian1.jpg">
						<span class="chanping">出师团队工资提成(橙子信用(详<br>版)产品)</span>
						<span class="bianhao">工资编号：762828</span>
					</a></div>
					<div class="thisclass"><a href="">
						<span class="time">2019-07-05	10:23:22</span><span class="money">￥1.71</span>
						<img src="img/tixian1.jpg">
						<span class="chanping">出师团队工资提成(橙子信用(详<br>版)产品)</span>
						<span class="bianhao">工资编号：762828</span>
					</a></div>
				</div>
			</li>
			<li class="tab-bd">
				<div class="thisclass"><a href="">
					<span class="time">2019-07-05	10:23:22</span><span class="money">￥458</span>
					<span class="yinlian">银联(6217**** ****4084)</span>
					<span class="danhao">提现单号：338775</span><span class="success">提现成功</span>
				</a></div>
				<div class="thisclass"><a href="">
					<span class="time">2019-07-05	10:23:22</span><span class="money">￥1.81</span>
					<span class="yinlian">银联(6217**** ****4084)</span>
					<span class="danhao">提现单号：338775</span><span class="success">提现成功</span>
				</a></div>
				<div class="thisclass"><a href="">
					<span class="time">2019-07-05	10:23:22</span><span class="money">￥27</span>
					<span class="yinlian">银联(6217**** ****4084)</span>
					<span class="danhao">提现单号：338775</span><span class="success">提现成功</span>
				</a></div>
				<div class="thisclass"><a href="">
					<span class="time">2019-07-05	10:23:22</span><span class="money">￥17</span>
					<span class="yin -->